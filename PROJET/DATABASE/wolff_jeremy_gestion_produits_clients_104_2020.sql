-- Jérémy Wolff, 26.06.2020
-- Ma base de données - Module ICT-104
-- Requêtes SQL permettant de créer ma base de données aves ses tables et ses données
-- -------------------------------------------------------------------------------------

-- Database: wolff_jeremy_gestion_produits_clients_104_2020

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists wolff_jeremy_gestion_produits_clients_104_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS wolff_jeremy_gestion_produits_clients_104_2020;

-- Utilisation de cette base de donnée

USE wolff_jeremy_gestion_produits_clients_104_2020; 


-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 26 Juin 2020 à 12:14
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `wolff_jeremy_gestion_produits_clients_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_adresse`
--

CREATE TABLE `t_adresse` (
  `id_adresse` int(11) NOT NULL,
  `RueNom` varchar(80) NOT NULL,
  `NumeroRue` varchar(10) DEFAULT NULL,
  `CodePostal` varchar(10) NOT NULL,
  `VilleNom` varchar(70) NOT NULL,
  `Canton` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_adresse`
--

INSERT INTO `t_adresse` (`id_adresse`, `RueNom`, `NumeroRue`, `CodePostal`, `VilleNom`, `Canton`) VALUES
(1, 'Route de Magny', '32', '1880', 'Bex', 'VD'),
(2, 'Avenue de Cour', '105', '1000', 'Lausanne', 'VD'),
(3, 'Avenue de Chailly', '5', '1000', 'Lausanne', 'VD'),
(4, 'Avenue de Chailly', '6', '1000', 'Lausanne', 'VD'),
(5, 'Rue Haldimand', '14', '1002', 'Lausanne', 'VD'),
(6, 'Rue Pichard', '16', '1002', 'Lausanne', 'VD'),
(7, 'Place St-François', '6', '1002', 'Lausanne', 'VD'),
(8, 'Place de la Riponne', '10', '1005', 'Lausanne', 'VD'),
(9, 'Place de la Navigation', '6', '1006', 'Lausanne', 'VD'),
(10, 'Boulevard de Grancy', '4', '1006', 'Lausanne', 'VD'),
(11, 'Avenue de Montchoisi', '3', '1006', 'Lausanne', 'VD'),
(12, 'Montchoisi', '28', '1006', 'Lausanne', 'VD'),
(13, 'Avenue Juste-Olivier', '5', '1006', 'Lausanne', 'VD'),
(14, 'Boulevard du Grancy', '35', '1006', 'Lausanne', 'VD'),
(15, 'Chemin du Closelet', '1', '1006', 'Lausanne', 'VD'),
(16, 'Avenue de Cour', '11', '1007', 'Lausanne', 'VD'),
(17, 'Rue du Midi', '10', '1020', 'Renens VD', 'VD'),
(18, 'Place de la Gare', '4', '1020', 'Renens VD', 'VD'),
(19, 'Rue de la Mèbre', '6', '1020', 'Renens VD', 'VD'),
(20, 'Avenue de Florissant', '28', '1020', 'Renens VD', 'VD'),
(21, 'Rue des Charpentiers', '19', '1110', 'Morges', 'VD'),
(22, 'Rue de la Gare', '19', '1110', 'Morges', 'VD'),
(23, 'Grand-Rue', '69', '1110', 'Morges 1', 'VD'),
(24, 'Grand-Rue', '100', '1110', 'Morges 1', 'VD'),
(25, 'Grand-Rue', '11', '1110', 'Morges', 'VD'),
(26, 'Rue Adolphe Gandon', '4', '2950', 'Courgenay', 'JU'),
(27, 'Breitenrainplatz', '36', '3000', 'Bern 22', 'BE'),
(28, 'Thunstrasse', '15', '3000', 'Bern 6', 'BE'),
(29, 'Giacomettistrasse', '15', '3000', 'Bern 31', 'BE'),
(30, 'Kramgasse', '2', '3000', 'Bern 8', 'BE'),
(31, 'Eigerstrasse', '55', '3007', 'Bern', 'BE'),
(32, 'Eigerstrasse', '2', '3007', 'Bern', 'BE'),
(33, 'Pestalozzistrasse', '26', '3007', 'Bern', 'BE'),
(34, 'Freiburgstrasse', '2', '3008', 'Bern', 'BE'),
(35, 'Schanzenstrasse', '5', '3008', 'Bern', 'BE'),
(36, 'Bahnhofstrasse', '11', '3900', 'Brig', 'VS'),
(37, 'Saltina Platz', '2', '3902', 'Glis', 'VS'),
(38, 'Gliserallee', '1', '3902', 'Glis', 'VS'),
(39, 'Kantonsstrasse', '58', '3902', 'Glis', 'VS'),
(40, 'Haus Alpen-Apothe', 'ke', '3992', 'Bettmeralp', 'VS'),
(41, 'Viale Portone', '1', '6500', 'Bellinzona', 'TI'),
(42, 'Piazza Nosetto', '5', '6500', 'Bellinzona', 'TI'),
(43, 'Via Franco Zorzi', '36', '6500', 'Bellinzona', 'TI'),
(44, 'Via Ospeda', 'le', '6500', 'Bellinzona', 'TI'),
(45, 'Piazza Indipendenza', '4', '6500', 'Bellinzona', 'TI'),
(46, 'Viale Stazione', '5', '6500', 'Bellinzona', 'TI'),
(47, 'Via Tomaso Rodari', '3', '6500', 'Bellinzona', 'TI'),
(48, 'Via Besso 2', '3a', '6900', 'Lugano', 'TI'),
(49, 'Via Francesco Soave', '1', '6900', 'Lugano', 'TI'),
(50, 'Via Francesco Soave', '8', '6900', 'Lugano', 'TI');

-- --------------------------------------------------------

--
-- Structure de la table `t_date_visite_client`
--

CREATE TABLE `t_date_visite_client` (
  `id_date_visite_client` int(11) NOT NULL,
  `DateVisiteClient` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_date_visite_client`
--

INSERT INTO `t_date_visite_client` (`id_date_visite_client`, `DateVisiteClient`) VALUES
(1, '2020-05-15'),
(2, '2003-10-06'),
(3, '2015-10-24'),
(4, '2009-04-28'),
(5, '2012-07-17'),
(6, '2015-06-22'),
(7, '2007-05-04'),
(8, '2003-03-14'),
(9, '2005-03-18'),
(10, '2008-06-01'),
(11, '2013-04-23'),
(12, '2015-02-16'),
(13, '2010-05-21'),
(14, '2002-10-01'),
(15, '2009-02-07'),
(16, '2016-12-02'),
(17, '2010-12-24'),
(18, '2004-07-17'),
(19, '2016-04-07'),
(20, '2005-10-23'),
(21, '2001-09-05'),
(22, '2019-10-29'),
(23, '2008-03-26'),
(24, '2005-06-05'),
(25, '2019-12-29'),
(26, '2014-06-15'),
(27, '2005-03-21'),
(28, '2012-06-23'),
(29, '2019-01-12'),
(30, '2015-03-30'),
(31, '2004-03-07'),
(32, '2007-10-08'),
(33, '2016-12-02'),
(34, '2010-01-14'),
(35, '2002-05-11'),
(36, '2007-09-02'),
(37, '2010-07-17'),
(38, '2004-05-06'),
(39, '2014-09-13'),
(40, '2012-11-05'),
(41, '2010-11-28'),
(42, '2013-06-13'),
(43, '2015-07-19'),
(44, '2011-06-05'),
(45, '2005-05-20'),
(46, '2017-05-01'),
(47, '2010-05-28'),
(48, '2004-06-26'),
(49, '2011-05-27'),
(50, '2018-12-12');

-- --------------------------------------------------------

--
-- Structure de la table `t_groupage`
--

CREATE TABLE `t_groupage` (
  `id_groupage` int(11) NOT NULL,
  `NomGroupage` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_groupage`
--

INSERT INTO `t_groupage` (`id_groupage`, `NomGroupage`) VALUES
(1, 'Mac\'O\'Pharma'),
(2, 'Winconcept-Feelgoods'),
(3, 'Pharmacie Plus'),
(4, 'Amavita'),
(5, 'Amavita'),
(6, 'Amavita'),
(7, 'CPI (Membre de Salveo)'),
(8, 'Pharmacie Plus'),
(9, 'Sun Store SA'),
(10, 'Amavita'),
(11, 'Sun Store SA'),
(12, 'Amavita'),
(13, 'Populaires Lausanne'),
(14, 'Amavita'),
(15, 'Newoctopharm'),
(16, 'Pharmacie Plus'),
(17, 'PharmaRomandie'),
(18, 'CPI (Membre de Salveo)'),
(19, 'CPI (Membre de Salveo)'),
(20, 'Sun Store SA'),
(21, 'Sun Store SA'),
(22, 'Sun Store SA'),
(23, 'Pharmacie Plus'),
(24, 'BENU SA Pharmacies'),
(25, 'BENU SA Pharmacies'),
(26, 'Sun Store SA'),
(27, 'Pharmacie Plus'),
(28, 'Populaires Lausanne'),
(29, 'CPI (Membre de Salveo)'),
(30, 'CPI (Membre de Salveo)'),
(31, 'CPI (Membre de Salveo)'),
(32, 'Direct Care AG'),
(33, 'Direct Care AG'),
(34, 'Winconcept-Feelgoods'),
(35, 'BENU SA Pharmacies'),
(36, 'CPI (Membre de Salveo)'),
(37, 'Nux-Galenica'),
(38, 'Populaires Lausanne'),
(39, 'Winconcept-Feelgoods'),
(40, 'CPI (Membre de Salveo)'),
(41, 'CPI (Membre de Salveo)'),
(42, 'Newoctopharm'),
(43, 'BENU SA Pharmacies'),
(44, 'Amavita'),
(45, 'Winconcept-Feelgoods'),
(46, 'PharmaRomandie'),
(47, 'Newoctopharm'),
(48, 'Amavita'),
(49, 'BENU SA Pharmacies'),
(50, 'Winconcept-Feelgoods');

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE `t_mail` (
  `id_mail` int(11) NOT NULL,
  `NomMail` varchar(320) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_mail`
--

INSERT INTO `t_mail` (`id_mail`, `NomMail`) VALUES
(1, 'marina.maccaud@yahoo.fr'),
(2, 'lbour1@quantcast.com'),
(3, 'laingel2@ovh.net'),
(4, 'skidstoun3@goodreads.com'),
(5, 'nmacmeekan4@unicef.org'),
(6, 'sdeek5@unc.edu'),
(7, 'agarvan6@reference.com'),
(8, 'ecorben7@sfgate.com'),
(9, 'ktindall8@addthis.com'),
(10, 'gcroux9@webnode.com'),
(11, 'myacoba@1688.com'),
(12, 'tlochheadb@cafepress.com'),
(13, 'ggeorgescuc@wikia.com'),
(14, 'erousd@surveymonkey.com'),
(15, 'rpechae@apple.com'),
(16, 'gcharletf@phpbb.com'),
(17, 'sciccettig@unicef.org'),
(18, 'nbygroveh@google.com.hk'),
(19, 'vniccolsi@sina.com.cn'),
(20, 'hmaffyj@chicagotribune.com'),
(21, 'hchasteneyk@squarespace.com'),
(22, 'grobertzl@google.ru'),
(23, 'utreem@vimeo.com'),
(24, 'rmiddleditchn@forbes.com'),
(25, 'kwiddecombeo@epa.gov'),
(26, 'wabbisonp@hud.gov'),
(27, 'scayserq@webeden.co.uk'),
(28, 'btruser@stumbleupon.com'),
(29, 'ggoldes@slashdot.org'),
(30, 'flinfoott@github.io'),
(31, 'kmaccorkellu@topsy.com'),
(32, 'cvanhovev@discovery.com'),
(33, 'credfordw@blogspot.com'),
(34, 'ljakubiakx@nsw.gov.au'),
(35, 'bhruskay@t-online.de'),
(36, 'lmartinekz@stanford.edu'),
(37, 'djephcott10@naver.com'),
(38, 'lmcaless11@columbia.edu'),
(39, 'oheamus12@wordpress.org'),
(40, 'rsneezem13@reddit.com'),
(41, 'jflieger14@google.com.au'),
(42, 'rcollidge15@mapy.cz'),
(43, 'ddenizet16@smugmug.com'),
(44, 'jskates17@yandex.ru'),
(45, 'jsoares18@dell.com'),
(46, 'jmosten19@chronoengine.com'),
(47, 'wjursch1a@skype.com'),
(48, 'thoolaghan1b@tinypic.com'),
(49, 'romoylan1c@mapquest.com'),
(50, 'vincent.veillon@120secondes.ch');

-- --------------------------------------------------------

--
-- Structure de la table `t_num_tel_fax`
--

CREATE TABLE `t_num_tel_fax` (
  `id_num_tel` int(11) NOT NULL,
  `NumTel` varchar(20) DEFAULT NULL,
  `NumFax` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_num_tel_fax`
--

INSERT INTO `t_num_tel_fax` (`id_num_tel`, `NumTel`, `NumFax`) VALUES
(1, '021 646 05 22', '021 646 77 51'),
(2, '021 616 55 54', '021 616 61 81'),
(3, '021 652 37 22', '021 652 04 23'),
(4, '021 652 01 00', '021 652 17 20'),
(5, '058 851 30 03', '058 851 31 03'),
(6, '058 851 30 04', '058 851 31 04'),
(7, '021 310 20 71', '021 310 20 81'),
(8, '021 318 73 10', '021 318 73 19'),
(9, '021 312 26 26', '021 312 56 56'),
(10, '058 878 55 10', '058 878 55 19'),
(11, '058 851 30 49', '058 851 31 49'),
(12, '058 878 54 80', '058 878 54 81'),
(13, '058 851 30 38', '058 851 31 38'),
(14, '021 312 35 73', '021 312 76 91'),
(15, '058 851 30 12', '058 851 31 12'),
(16, '021 323 09 40', '021 323 09 41'),
(17, '021 323 84 84', '021 323 84 80'),
(18, '021 312 61 80', '021 312 61 89'),
(19, '021 624 75 22', '021 624 75 43'),
(20, '021 331 29 00', '021 331 29 01'),
(21, '058 878 55 50', '058 878 55 59'),
(22, '058 878 55 40', '058 878 55 49'),
(23, '058 878 55 00', '058 878 55 09'),
(24, '021 323 06 95', '021 323 89 02'),
(25, '021 323 52 10', '021 323 52 18'),
(26, '021 625 25 69', '021 624 11 33'),
(27, '058 878 54 90', '058 878 54 99'),
(28, '021 624 62 17', '021 624 62 80'),
(29, '021 624 08 70', '021 624 08 54'),
(30, '021 624 02 72', '021 626 30 63'),
(31, '021 646 43 94', '021 646 43 85'),
(32, '021 624 58 24', '021 624 00 27'),
(33, '021 312 02 14', '021 312 16 04'),
(34, '021 323 34 06', '021 323 34 04'),
(35, '021 616 20 44', '021 616 20 79'),
(36, '021 617 00 82', '021 616 88 02'),
(37, '058 851 30 27', '058 851 31 27'),
(38, '021 624 89 60', '021 626 28 61'),
(39, '021 624 38 50', '021 624 38 61'),
(40, '021 616 98 00', '021 616 98 01'),
(41, '058 878 10 30', '058 878 10 39'),
(42, '021 601 72 92', '021 601 72 93'),
(43, '021 624 44 60', '021 626 28 60'),
(44, '021 647 77 33', '021 647 76 11'),
(45, '021 624 81 40', '021 624 86 41'),
(46, '058 878 58 20', '058 878 58 29'),
(47, '021 624 04 18', '021 624 60 23'),
(48, '058 851 30 65', '058 851 31 65'),
(49, '021 728 21 45', '021 728 23 05'),
(50, '058 878 58 30', '058 878 58 39');

-- --------------------------------------------------------

--
-- Structure de la table `t_pdv_appart_groupage`
--

CREATE TABLE `t_pdv_appart_groupage` (
  `id_pdv_appart_groupage` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `FK_Groupage` int(11) NOT NULL,
  `DateCreationGroupage` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pdv_appart_groupage`
--

INSERT INTO `t_pdv_appart_groupage` (`id_pdv_appart_groupage`, `FK_PDV`, `FK_Groupage`, `DateCreationGroupage`) VALUES
(1, 1, 1, '2020-06-21 08:41:05'),
(2, 2, 2, '2020-06-21 08:41:05'),
(3, 3, 3, '2020-06-21 08:41:05'),
(4, 4, 4, '2020-06-21 08:41:05'),
(5, 5, 5, '2020-06-21 08:41:05'),
(6, 6, 6, '2020-06-21 08:41:05'),
(7, 7, 7, '2020-06-21 08:41:05'),
(8, 8, 8, '2020-06-21 08:41:05'),
(9, 9, 9, '2020-06-21 08:41:05'),
(10, 10, 10, '2020-06-21 08:41:05'),
(11, 11, 11, '2020-06-21 08:41:05'),
(12, 12, 12, '2020-06-21 08:41:05'),
(13, 13, 13, '2020-06-21 08:41:05'),
(14, 14, 14, '2020-06-21 08:41:05'),
(15, 15, 15, '2020-06-21 08:41:05'),
(16, 16, 16, '2020-06-21 08:41:05'),
(17, 17, 17, '2020-06-21 08:41:05'),
(18, 18, 18, '2020-06-21 08:41:05'),
(19, 19, 19, '2020-06-21 08:41:05'),
(20, 20, 20, '2020-06-21 08:41:05'),
(21, 21, 21, '2020-06-21 08:41:05'),
(22, 22, 22, '2020-06-21 08:41:05'),
(23, 23, 23, '2020-06-21 08:41:05'),
(24, 24, 24, '2020-06-21 08:41:05'),
(25, 25, 25, '2020-06-21 08:41:05'),
(26, 26, 26, '2020-06-21 08:41:05'),
(27, 27, 27, '2020-06-21 08:41:05'),
(28, 28, 28, '2020-06-21 08:41:05'),
(29, 29, 29, '2020-06-21 08:41:05'),
(30, 30, 30, '2020-06-21 08:41:05'),
(31, 31, 31, '2020-06-21 08:41:05'),
(32, 32, 32, '2020-06-21 08:41:05'),
(33, 33, 33, '2020-06-21 08:41:05'),
(34, 34, 34, '2020-06-21 08:41:05'),
(35, 35, 35, '2020-06-21 08:41:05'),
(36, 36, 36, '2020-06-21 08:41:05'),
(37, 37, 37, '2020-06-21 08:41:05'),
(38, 38, 38, '2020-06-21 08:41:05'),
(39, 39, 39, '2020-06-21 08:41:05'),
(40, 40, 40, '2020-06-21 08:41:05'),
(41, 41, 41, '2020-06-21 08:41:05'),
(42, 42, 42, '2020-06-21 08:41:05'),
(43, 43, 43, '2020-06-21 08:41:05'),
(44, 44, 44, '2020-06-21 08:41:05'),
(45, 45, 45, '2020-06-21 08:41:05'),
(46, 46, 46, '2020-06-21 08:41:05'),
(47, 47, 47, '2020-06-21 08:41:05'),
(48, 48, 48, '2020-06-21 08:41:05'),
(49, 49, 49, '2020-06-21 08:41:05'),
(50, 50, 50, '2020-06-21 08:41:05');

-- --------------------------------------------------------

--
-- Structure de la table `t_pdv_avoir_adresse`
--

CREATE TABLE `t_pdv_avoir_adresse` (
  `id_pdv_avoir_adresse` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `FK_Adresse` int(11) NOT NULL,
  `DateCreationAdresse` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pdv_avoir_adresse`
--

INSERT INTO `t_pdv_avoir_adresse` (`id_pdv_avoir_adresse`, `FK_PDV`, `FK_Adresse`, `DateCreationAdresse`) VALUES
(1, 1, 1, '2020-06-21 08:41:51'),
(2, 2, 2, '2020-06-21 08:41:51'),
(3, 3, 3, '2020-06-21 08:41:51'),
(4, 4, 4, '2020-06-21 08:41:51'),
(5, 5, 5, '2020-06-21 08:41:51'),
(6, 6, 6, '2020-06-21 08:41:51'),
(7, 7, 7, '2020-06-21 08:41:51'),
(8, 8, 8, '2020-06-21 08:41:51'),
(9, 9, 9, '2020-06-21 08:41:51'),
(10, 10, 10, '2020-06-21 08:41:51'),
(11, 11, 11, '2020-06-21 08:41:51'),
(12, 12, 12, '2020-06-21 08:41:51'),
(13, 13, 13, '2020-06-21 08:41:51'),
(14, 14, 14, '2020-06-21 08:41:51'),
(15, 15, 15, '2020-06-21 08:41:51'),
(16, 16, 16, '2020-06-21 08:41:51'),
(17, 17, 17, '2020-06-21 08:41:51'),
(18, 18, 18, '2020-06-21 08:41:51'),
(19, 19, 19, '2020-06-21 08:41:51'),
(20, 20, 20, '2020-06-21 08:41:51'),
(21, 21, 21, '2020-06-21 08:41:51'),
(22, 22, 22, '2020-06-21 08:41:51'),
(23, 23, 23, '2020-06-21 08:41:51'),
(24, 24, 24, '2020-06-21 08:41:51'),
(25, 25, 25, '2020-06-21 08:41:51'),
(26, 26, 26, '2020-06-21 08:41:51'),
(27, 27, 27, '2020-06-21 08:41:51'),
(28, 28, 28, '2020-06-21 08:41:51'),
(29, 29, 29, '2020-06-21 08:41:51'),
(30, 30, 30, '2020-06-21 08:41:52'),
(31, 31, 31, '2020-06-21 08:41:52'),
(32, 32, 32, '2020-06-21 08:41:52'),
(33, 33, 33, '2020-06-21 08:41:52'),
(34, 34, 34, '2020-06-21 08:41:52'),
(35, 35, 35, '2020-06-21 08:41:52'),
(36, 36, 36, '2020-06-21 08:41:52'),
(37, 37, 37, '2020-06-21 08:41:52'),
(38, 38, 38, '2020-06-21 08:41:52'),
(39, 39, 39, '2020-06-21 08:41:52'),
(40, 40, 40, '2020-06-21 08:41:52'),
(41, 41, 41, '2020-06-21 08:41:52'),
(42, 42, 42, '2020-06-21 08:41:52'),
(43, 43, 43, '2020-06-21 08:41:52'),
(44, 44, 44, '2020-06-21 08:41:52'),
(45, 45, 45, '2020-06-21 08:41:52'),
(46, 46, 46, '2020-06-21 08:41:52'),
(47, 47, 47, '2020-06-21 08:41:52'),
(48, 48, 48, '2020-06-21 08:41:52'),
(49, 49, 49, '2020-06-21 08:41:52'),
(50, 50, 50, '2020-06-21 08:41:52');

-- --------------------------------------------------------

--
-- Structure de la table `t_pdv_avoir_date_visite`
--

CREATE TABLE `t_pdv_avoir_date_visite` (
  `id_pdv_avoir_date_visite` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `FK_Date_Visite_Client` int(11) NOT NULL,
  `DateCreationDateVisite` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pdv_avoir_date_visite`
--

INSERT INTO `t_pdv_avoir_date_visite` (`id_pdv_avoir_date_visite`, `FK_PDV`, `FK_Date_Visite_Client`, `DateCreationDateVisite`) VALUES
(1, 1, 1, '2020-03-08 10:40:48'),
(2, 2, 2, '2020-03-08 10:40:48'),
(3, 3, 3, '2020-03-08 10:40:48'),
(4, 4, 4, '2020-03-08 10:40:48'),
(5, 5, 5, '2020-03-08 10:40:48'),
(6, 6, 6, '2020-03-08 10:40:48'),
(7, 7, 7, '2020-03-08 10:40:48'),
(8, 8, 8, '2020-03-08 10:40:48'),
(9, 9, 9, '2020-03-08 10:40:48'),
(10, 10, 10, '2020-03-08 10:40:48'),
(11, 11, 11, '2020-03-08 10:40:48'),
(12, 12, 12, '2020-03-08 10:40:48'),
(13, 13, 13, '2020-03-08 10:40:48'),
(14, 14, 14, '2020-03-08 10:40:48'),
(15, 15, 15, '2020-03-08 10:40:48'),
(16, 16, 16, '2020-03-08 10:40:48'),
(17, 17, 17, '2020-03-08 10:40:48'),
(18, 18, 18, '2020-03-08 10:40:48'),
(19, 19, 19, '2020-03-08 10:40:48'),
(20, 20, 20, '2020-03-08 10:40:48'),
(21, 21, 21, '2020-03-08 10:40:48'),
(22, 22, 22, '2020-03-08 10:40:48'),
(23, 23, 23, '2020-03-08 10:40:48'),
(24, 24, 24, '2020-03-08 10:40:48'),
(25, 25, 25, '2020-03-08 10:40:48'),
(26, 26, 26, '2020-03-08 10:40:48'),
(27, 27, 27, '2020-03-08 10:40:48'),
(28, 28, 28, '2020-03-08 10:40:48'),
(29, 29, 29, '2020-03-08 10:40:48'),
(30, 30, 30, '2020-03-08 10:40:48'),
(31, 31, 31, '2020-03-08 10:40:48'),
(32, 32, 32, '2020-03-08 10:40:48'),
(33, 33, 33, '2020-03-08 10:40:48'),
(34, 34, 34, '2020-03-08 10:40:48'),
(35, 35, 35, '2020-03-08 10:40:48'),
(36, 36, 36, '2020-03-08 10:40:48'),
(37, 37, 37, '2020-03-08 10:40:48'),
(38, 38, 38, '2020-03-08 10:40:48'),
(39, 39, 39, '2020-03-08 10:40:48'),
(40, 40, 40, '2020-03-08 10:40:48'),
(41, 41, 41, '2020-03-08 10:40:48'),
(42, 42, 42, '2020-03-08 10:40:48'),
(43, 43, 43, '2020-03-08 10:40:48'),
(44, 44, 44, '2020-03-08 10:40:49'),
(45, 45, 45, '2020-03-08 10:40:49'),
(46, 46, 46, '2020-03-08 10:40:49'),
(47, 47, 47, '2020-03-08 10:40:49'),
(48, 48, 48, '2020-03-08 10:40:49'),
(49, 49, 49, '2020-03-08 10:40:49'),
(50, 50, 50, '2020-03-08 10:40:49');

-- --------------------------------------------------------

--
-- Structure de la table `t_pdv_avoir_potentiel`
--

CREATE TABLE `t_pdv_avoir_potentiel` (
  `id_pdv_avoir_potentiel` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `FK_Potentiel_Client` int(11) NOT NULL,
  `DateCreationPotentielClient` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pdv_avoir_potentiel`
--

INSERT INTO `t_pdv_avoir_potentiel` (`id_pdv_avoir_potentiel`, `FK_PDV`, `FK_Potentiel_Client`, `DateCreationPotentielClient`) VALUES
(1, 1, 1, '2020-03-08 10:43:26'),
(2, 2, 2, '2020-03-08 10:43:26'),
(3, 3, 3, '2020-03-08 10:43:26'),
(4, 4, 4, '2020-03-08 10:43:26'),
(5, 5, 5, '2020-03-08 10:43:26'),
(6, 6, 6, '2020-03-08 10:43:26'),
(7, 7, 7, '2020-03-08 10:43:26'),
(8, 8, 8, '2020-03-08 10:43:26'),
(9, 9, 9, '2020-03-08 10:43:26'),
(10, 10, 10, '2020-03-08 10:43:26'),
(11, 11, 11, '2020-03-08 10:43:26'),
(12, 12, 12, '2020-03-08 10:43:26'),
(13, 13, 13, '2020-03-08 10:43:26'),
(14, 14, 14, '2020-03-08 10:43:26'),
(15, 15, 15, '2020-03-08 10:43:27'),
(16, 16, 16, '2020-03-08 10:43:27'),
(17, 17, 17, '2020-03-08 10:43:27'),
(18, 18, 18, '2020-03-08 10:43:27'),
(19, 19, 19, '2020-03-08 10:43:27'),
(20, 20, 20, '2020-03-08 10:43:27'),
(21, 21, 21, '2020-03-08 10:43:27'),
(22, 22, 22, '2020-03-08 10:43:27'),
(23, 23, 23, '2020-03-08 10:43:27'),
(24, 24, 24, '2020-03-08 10:43:27'),
(25, 25, 25, '2020-03-08 10:43:27'),
(26, 26, 26, '2020-03-08 10:43:27'),
(27, 27, 27, '2020-03-08 10:43:27'),
(28, 28, 28, '2020-03-08 10:43:27'),
(29, 29, 29, '2020-03-08 10:43:27'),
(30, 30, 30, '2020-03-08 10:43:27'),
(31, 31, 31, '2020-03-08 10:43:27'),
(32, 32, 32, '2020-03-08 10:43:27'),
(33, 33, 33, '2020-03-08 10:43:27'),
(34, 34, 34, '2020-03-08 10:43:27'),
(35, 35, 35, '2020-03-08 10:43:27'),
(36, 36, 36, '2020-03-08 10:43:27'),
(37, 37, 37, '2020-03-08 10:43:27'),
(38, 38, 38, '2020-03-08 10:43:27'),
(39, 39, 39, '2020-03-08 10:43:27'),
(40, 40, 40, '2020-03-08 10:43:27'),
(41, 41, 41, '2020-03-08 10:43:27'),
(42, 42, 42, '2020-03-08 10:43:27'),
(43, 43, 43, '2020-03-08 10:43:27'),
(44, 44, 44, '2020-03-08 10:43:27'),
(45, 45, 45, '2020-03-08 10:43:27'),
(46, 46, 46, '2020-03-08 10:43:27'),
(47, 47, 47, '2020-03-08 10:43:27'),
(48, 48, 48, '2020-03-08 10:43:27'),
(49, 49, 49, '2020-03-08 10:43:27'),
(50, 50, 50, '2020-03-08 10:43:27');

-- --------------------------------------------------------

--
-- Structure de la table `t_pdv_vendre_produits`
--

CREATE TABLE `t_pdv_vendre_produits` (
  `id_pdv_vendre_produits` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `FK_Produits` int(11) NOT NULL,
  `DateInsertionProduit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pdv_vendre_produits`
--

INSERT INTO `t_pdv_vendre_produits` (`id_pdv_vendre_produits`, `FK_PDV`, `FK_Produits`, `DateInsertionProduit`) VALUES
(1, 1, 1, '2020-06-21 08:46:05'),
(2, 2, 2, '2020-06-21 08:46:05'),
(3, 3, 3, '2020-06-21 08:46:05'),
(4, 4, 4, '2020-06-21 08:46:05'),
(5, 5, 5, '2020-06-21 08:46:05'),
(6, 6, 6, '2020-06-21 08:46:05'),
(7, 7, 7, '2020-06-21 08:46:05'),
(8, 8, 8, '2020-06-21 08:46:05'),
(9, 9, 9, '2020-06-21 08:46:05'),
(10, 10, 10, '2020-06-21 08:46:05'),
(11, 11, 11, '2020-06-21 08:46:05'),
(12, 12, 12, '2020-06-21 08:46:05'),
(13, 13, 13, '2020-06-21 08:46:05'),
(14, 14, 14, '2020-06-21 08:46:05'),
(15, 15, 15, '2020-06-21 08:46:05'),
(16, 16, 16, '2020-06-21 08:46:05'),
(17, 17, 17, '2020-06-21 08:46:05'),
(18, 18, 18, '2020-06-21 08:46:05'),
(19, 19, 19, '2020-06-21 08:46:05'),
(20, 20, 20, '2020-06-21 08:46:05'),
(21, 21, 21, '2020-06-21 08:46:05'),
(22, 22, 22, '2020-06-21 08:46:05'),
(23, 23, 23, '2020-06-21 08:46:05'),
(24, 24, 24, '2020-06-21 08:46:05'),
(25, 25, 25, '2020-06-21 08:46:05'),
(26, 26, 26, '2020-06-21 08:46:05'),
(27, 27, 27, '2020-06-21 08:46:05'),
(28, 28, 28, '2020-06-21 08:46:05'),
(29, 29, 29, '2020-06-21 08:46:05'),
(30, 30, 30, '2020-06-21 08:46:05'),
(31, 31, 31, '2020-06-21 08:46:05'),
(32, 32, 32, '2020-06-21 08:46:05'),
(33, 33, 33, '2020-06-21 08:46:06'),
(34, 34, 34, '2020-06-21 08:46:06'),
(35, 35, 35, '2020-06-21 08:46:06'),
(36, 36, 36, '2020-06-21 08:46:06'),
(37, 37, 37, '2020-06-21 08:46:06'),
(38, 38, 38, '2020-06-21 08:46:06'),
(39, 39, 39, '2020-06-21 08:46:06'),
(40, 40, 40, '2020-06-21 08:46:06'),
(41, 41, 41, '2020-06-21 08:46:06'),
(42, 42, 42, '2020-06-21 08:46:06'),
(43, 43, 43, '2020-06-21 08:46:06'),
(44, 44, 44, '2020-06-21 08:46:06'),
(45, 45, 45, '2020-06-21 08:46:06'),
(46, 46, 46, '2020-06-21 08:46:06'),
(47, 47, 47, '2020-06-21 08:46:06'),
(48, 48, 48, '2020-06-21 08:46:06'),
(49, 49, 49, '2020-06-21 08:46:06'),
(50, 50, 50, '2020-06-21 08:46:06'),
(53, 3, 53, '2020-06-21 08:46:06'),
(54, 2, 54, '2020-06-21 08:46:06'),
(55, 4, 55, '2020-06-21 08:46:06'),
(56, 5, 56, '2020-06-21 08:46:06'),
(57, 6, 57, '2020-06-21 08:46:06'),
(58, 3, 58, '2020-06-21 08:46:06'),
(59, 4, 59, '2020-06-21 08:46:06'),
(60, 5, 60, '2020-06-21 08:46:06'),
(62, 8, 62, '2020-06-21 08:46:06'),
(63, 7, 63, '2020-06-21 08:46:06'),
(64, 7, 64, '2020-06-21 08:46:06'),
(65, 4, 65, '2020-06-21 08:46:06'),
(66, 2, 66, '2020-06-21 08:46:06'),
(68, 3, 68, '2020-06-21 08:46:06'),
(69, 8, 69, '2020-06-21 08:46:06'),
(70, 9, 70, '2020-06-21 08:46:06'),
(71, 10, 71, '2020-06-21 08:46:06'),
(72, 10, 72, '2020-06-21 08:46:06'),
(73, 9, 73, '2020-06-21 08:46:06'),
(74, 15, 74, '2020-06-21 08:46:06'),
(75, 2, 75, '2020-06-21 08:46:06'),
(76, 14, 76, '2020-06-21 08:46:06'),
(77, 18, 77, '2020-06-21 08:46:06'),
(78, 40, 78, '2020-06-21 08:46:06'),
(79, 19, 79, '2020-06-21 08:46:06'),
(80, 8, 80, '2020-06-21 08:46:06'),
(81, 25, 81, '2020-06-21 08:46:06'),
(82, 15, 82, '2020-06-21 08:46:06'),
(83, 12, 83, '2020-06-21 08:46:06'),
(84, 18, 84, '2020-06-21 08:46:06'),
(85, 9, 85, '2020-06-21 08:46:06'),
(86, 12, 86, '2020-06-21 08:46:06'),
(87, 12, 87, '2020-06-21 08:46:06'),
(89, 27, 89, '2020-06-21 08:46:06'),
(90, 9, 90, '2020-06-21 08:46:06'),
(91, 33, 91, '2020-06-21 08:46:06'),
(92, 34, 92, '2020-06-21 08:46:06'),
(93, 1, 7, '2020-06-21 08:58:37'),
(94, 1, 87, '2020-06-21 08:58:37'),
(95, 16, 7, '2020-06-21 09:06:00'),
(101, 11, 91, '2020-06-21 09:50:13'),
(102, 50, 90, '2020-06-23 09:19:31'),
(103, 11, 68, '2020-06-25 08:52:38'),
(104, 10, 7, '2020-06-25 08:55:44');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_ref`
--

CREATE TABLE `t_personne_ref` (
  `id_pers_ref` int(11) NOT NULL,
  `PrenomPersRef` varchar(30) DEFAULT NULL,
  `NomPersRef` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_personne_ref`
--

INSERT INTO `t_personne_ref` (`id_pers_ref`, `PrenomPersRef`, `NomPersRef`) VALUES
(1, 'Marina', 'Maccaud'),
(2, 'Victor', 'Dufour'),
(3, 'Laurence', 'Clerc'),
(4, 'Abdul-Fattah', 'Dennaoui'),
(5, 'Pascal', 'Zuber'),
(6, 'Anne', 'Dupraz'),
(7, 'Yun', 'Ling'),
(8, 'Gwladys', 'Goypieron'),
(9, 'Gabriela', 'Castro'),
(10, 'Gabrielle', 'Aribot'),
(11, 'Philippe', 'Villachon'),
(12, 'Aline', 'Viret'),
(13, 'Jennifer Anne', 'Gygli'),
(14, 'Pascal', 'Glowaczower'),
(15, 'Shida', 'Zen-Ruffinen'),
(16, 'Henri-Jean', 'Golaz'),
(17, 'Bernard', 'Roder'),
(18, 'Claude', 'Bersier'),
(19, 'Karim', 'Raya'),
(20, 'Pierre Alain', 'Jotterand'),
(21, 'Jean', 'Bertrand'),
(22, 'Hicham', 'Tayebi'),
(23, 'Johannes et Vahid', 'Schroeder et Khoshideh'),
(24, 'Alexandra', 'Rothen'),
(25, 'Khalid', 'Kiwirra'),
(26, 'Naime', 'Corre'),
(27, 'Albert', 'Villa'),
(28, 'Robert', 'Massard'),
(29, 'Ivana', 'Duperrex'),
(30, 'Aleksandra', 'Izycka'),
(31, 'Maria Cristina', 'Fraiz Dobarro'),
(32, 'Heyrani Nobari', 'Ruhollah'),
(33, 'Thi Trong', 'Dang-Nguyen'),
(34, 'Eric', 'Bussat'),
(35, 'Khadija', 'Mountassir'),
(36, 'Fabien', 'Vidonne'),
(37, 'Pierre', 'Ghaliounghi'),
(38, 'Olivier', 'Clavel'),
(39, 'Anastasia-Natalia', 'Ventouri'),
(40, 'Doris', 'Chabrier-Steck'),
(41, 'Diana', 'Biason'),
(42, 'Dounia', 'Ponci'),
(43, 'Katia', 'Di Napoli'),
(44, 'Christophe', 'Boymond'),
(45, 'Laurent', 'Hemsi'),
(46, 'Olivier', 'Mage'),
(47, 'Jacques', 'Zolty'),
(48, 'Isabelle', 'Knopf-Girardet'),
(49, 'Ivan', 'Paradisgarten'),
(50, 'Natasha', 'Broquet');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_avoir_mail`
--

CREATE TABLE `t_pers_avoir_mail` (
  `id_pers_avoir_mail` int(11) NOT NULL,
  `FK_Pers_Ref` int(11) NOT NULL,
  `FK_Mail` int(11) NOT NULL,
  `DateCreationMail` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pers_avoir_mail`
--

INSERT INTO `t_pers_avoir_mail` (`id_pers_avoir_mail`, `FK_Pers_Ref`, `FK_Mail`, `DateCreationMail`) VALUES
(1, 1, 1, '2020-06-20 14:15:26'),
(2, 2, 2, '2020-06-20 14:15:26'),
(3, 3, 3, '2020-06-20 14:15:26'),
(4, 4, 4, '2020-06-20 14:15:26'),
(5, 5, 5, '2020-06-20 14:15:26'),
(6, 6, 6, '2020-06-20 14:15:26'),
(7, 7, 7, '2020-06-20 14:15:26'),
(8, 8, 8, '2020-06-20 14:15:26'),
(9, 9, 9, '2020-06-20 14:15:26'),
(10, 10, 10, '2020-06-20 14:15:26'),
(11, 11, 11, '2020-06-20 14:15:26'),
(12, 12, 12, '2020-06-20 14:15:26'),
(13, 13, 13, '2020-06-20 14:15:26'),
(14, 14, 14, '2020-06-20 14:15:26'),
(15, 15, 15, '2020-06-20 14:15:26'),
(16, 16, 16, '2020-06-20 14:15:26'),
(17, 17, 17, '2020-06-20 14:15:26'),
(18, 18, 18, '2020-06-20 14:15:26'),
(19, 19, 19, '2020-06-20 14:15:26'),
(20, 20, 20, '2020-06-20 14:15:26'),
(21, 21, 21, '2020-06-20 14:15:26'),
(22, 22, 22, '2020-06-20 14:15:26'),
(23, 23, 23, '2020-06-20 14:15:26'),
(24, 24, 24, '2020-06-20 14:15:26'),
(25, 25, 25, '2020-06-20 14:15:26'),
(26, 26, 26, '2020-06-20 14:15:26'),
(27, 27, 27, '2020-06-20 14:15:26'),
(28, 28, 28, '2020-06-20 14:15:26'),
(29, 29, 29, '2020-06-20 14:15:26'),
(30, 30, 30, '2020-06-20 14:15:26'),
(31, 31, 31, '2020-06-20 14:15:26'),
(32, 32, 32, '2020-06-20 14:15:26'),
(33, 33, 33, '2020-06-20 14:15:26'),
(34, 34, 34, '2020-06-20 14:15:26'),
(35, 35, 35, '2020-06-20 14:15:26'),
(36, 36, 36, '2020-06-20 14:15:26'),
(37, 37, 37, '2020-06-20 14:15:26'),
(38, 38, 38, '2020-06-20 14:15:26'),
(39, 39, 39, '2020-06-20 14:15:26'),
(40, 40, 40, '2020-06-20 14:15:26'),
(41, 41, 41, '2020-06-20 14:15:26'),
(42, 42, 42, '2020-06-20 14:15:26'),
(43, 43, 43, '2020-06-20 14:15:26'),
(44, 44, 44, '2020-06-20 14:15:26'),
(45, 45, 45, '2020-06-20 14:15:26'),
(46, 46, 46, '2020-06-20 14:15:26'),
(47, 47, 47, '2020-06-20 14:15:26'),
(48, 48, 48, '2020-06-20 14:15:26'),
(49, 49, 49, '2020-06-20 14:15:26'),
(50, 50, 50, '2020-06-20 14:15:26');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_avoir_num_tel`
--

CREATE TABLE `t_pers_avoir_num_tel` (
  `id_pers_avoir_num_tel` int(11) NOT NULL,
  `FK_Pers_Ref` int(11) NOT NULL,
  `FK_Num_Tel` int(11) NOT NULL,
  `DateCreationNum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pers_avoir_num_tel`
--

INSERT INTO `t_pers_avoir_num_tel` (`id_pers_avoir_num_tel`, `FK_Pers_Ref`, `FK_Num_Tel`, `DateCreationNum`) VALUES
(1, 1, 1, '2020-06-21 08:49:44'),
(2, 2, 2, '2020-06-21 08:49:44'),
(3, 3, 3, '2020-06-21 08:49:44'),
(4, 4, 4, '2020-06-21 08:49:44'),
(5, 5, 5, '2020-06-21 08:49:44'),
(6, 6, 6, '2020-06-21 08:49:44'),
(7, 7, 7, '2020-06-21 08:49:44'),
(8, 8, 8, '2020-06-21 08:49:44'),
(9, 9, 9, '2020-06-21 08:49:44'),
(10, 10, 10, '2020-06-21 08:49:44'),
(11, 11, 11, '2020-06-21 08:49:44'),
(12, 12, 12, '2020-06-21 08:49:44'),
(13, 13, 13, '2020-06-21 08:49:44'),
(14, 14, 14, '2020-06-21 08:49:44'),
(15, 15, 15, '2020-06-21 08:49:44'),
(16, 16, 16, '2020-06-21 08:49:44'),
(17, 17, 17, '2020-06-21 08:49:44'),
(18, 18, 18, '2020-06-21 08:49:44'),
(19, 19, 19, '2020-06-21 08:49:44'),
(20, 20, 20, '2020-06-21 08:49:44'),
(21, 21, 21, '2020-06-21 08:49:44'),
(22, 22, 22, '2020-06-21 08:49:44'),
(23, 23, 23, '2020-06-21 08:49:44'),
(24, 24, 24, '2020-06-21 08:49:44'),
(25, 25, 25, '2020-06-21 08:49:44'),
(26, 26, 26, '2020-06-21 08:49:44'),
(27, 27, 27, '2020-06-21 08:49:44'),
(28, 28, 28, '2020-06-21 08:49:44'),
(29, 29, 29, '2020-06-21 08:49:44'),
(30, 30, 30, '2020-06-21 08:49:44'),
(31, 31, 31, '2020-06-21 08:49:44'),
(32, 32, 32, '2020-06-21 08:49:44'),
(33, 33, 33, '2020-06-21 08:49:44'),
(34, 34, 34, '2020-06-21 08:49:44'),
(35, 35, 35, '2020-06-21 08:49:44'),
(36, 36, 36, '2020-06-21 08:49:44'),
(37, 37, 37, '2020-06-21 08:49:44'),
(38, 38, 38, '2020-06-21 08:49:44'),
(39, 39, 39, '2020-06-21 08:49:44'),
(40, 40, 40, '2020-06-21 08:49:44'),
(41, 41, 41, '2020-06-21 08:49:44'),
(42, 42, 42, '2020-06-21 08:49:44'),
(43, 43, 43, '2020-06-21 08:49:44'),
(44, 44, 44, '2020-06-21 08:49:44'),
(45, 45, 45, '2020-06-21 08:49:44'),
(46, 46, 46, '2020-06-21 08:49:44'),
(47, 47, 47, '2020-06-21 08:49:44'),
(48, 48, 48, '2020-06-21 08:49:44'),
(49, 49, 49, '2020-06-21 08:49:44'),
(50, 50, 50, '2020-06-21 08:49:44');

-- --------------------------------------------------------

--
-- Structure de la table `t_pers_travailler_pdv`
--

CREATE TABLE `t_pers_travailler_pdv` (
  `id_pers_travailler_pdv` int(11) NOT NULL,
  `FK_Pers_Ref` int(11) NOT NULL,
  `FK_PDV` int(11) NOT NULL,
  `DateCreationPersRef` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_pers_travailler_pdv`
--

INSERT INTO `t_pers_travailler_pdv` (`id_pers_travailler_pdv`, `FK_Pers_Ref`, `FK_PDV`, `DateCreationPersRef`) VALUES
(1, 1, 1, '2020-06-21 08:50:37'),
(2, 2, 2, '2020-06-21 08:50:37'),
(3, 3, 3, '2020-06-21 08:50:37'),
(4, 4, 4, '2020-06-21 08:50:37'),
(5, 5, 5, '2020-06-21 08:50:37'),
(6, 6, 6, '2020-06-21 08:50:37'),
(7, 7, 7, '2020-06-21 08:50:37'),
(8, 8, 8, '2020-06-21 08:50:37'),
(9, 9, 9, '2020-06-21 08:50:37'),
(10, 10, 10, '2020-06-21 08:50:37'),
(11, 11, 11, '2020-06-21 08:50:37'),
(12, 12, 12, '2020-06-21 08:50:37'),
(13, 13, 13, '2020-06-21 08:50:37'),
(14, 14, 14, '2020-06-21 08:50:37'),
(15, 15, 15, '2020-06-21 08:50:37'),
(16, 16, 16, '2020-06-21 08:50:37'),
(17, 17, 17, '2020-06-21 08:50:37'),
(18, 18, 18, '2020-06-21 08:50:37'),
(19, 19, 19, '2020-06-21 08:50:37'),
(20, 20, 20, '2020-06-21 08:50:37'),
(21, 21, 21, '2020-06-21 08:50:37'),
(22, 22, 22, '2020-06-21 08:50:37'),
(23, 23, 23, '2020-06-21 08:50:37'),
(24, 24, 24, '2020-06-21 08:50:37'),
(25, 25, 25, '2020-06-21 08:50:37'),
(26, 26, 26, '2020-06-21 08:50:37'),
(27, 27, 27, '2020-06-21 08:50:37'),
(28, 28, 28, '2020-06-21 08:50:37'),
(29, 29, 29, '2020-06-21 08:50:37'),
(30, 30, 30, '2020-06-21 08:50:37'),
(31, 31, 31, '2020-06-21 08:50:37'),
(32, 32, 32, '2020-06-21 08:50:37'),
(33, 33, 33, '2020-06-21 08:50:37'),
(34, 34, 34, '2020-06-21 08:50:37'),
(35, 35, 35, '2020-06-21 08:50:37'),
(36, 36, 36, '2020-06-21 08:50:37'),
(37, 37, 37, '2020-06-21 08:50:37'),
(38, 38, 38, '2020-06-21 08:50:37'),
(39, 39, 39, '2020-06-21 08:50:37'),
(40, 40, 40, '2020-06-21 08:50:37'),
(41, 41, 41, '2020-06-21 08:50:37'),
(42, 42, 42, '2020-06-21 08:50:37'),
(43, 43, 43, '2020-06-21 08:50:37'),
(44, 44, 44, '2020-06-21 08:50:37'),
(45, 45, 45, '2020-06-21 08:50:37'),
(46, 46, 46, '2020-06-21 08:50:37'),
(47, 47, 47, '2020-06-21 08:50:38'),
(48, 48, 48, '2020-06-21 08:50:38'),
(49, 49, 49, '2020-06-21 08:50:38'),
(50, 50, 50, '2020-06-21 08:50:38');

-- --------------------------------------------------------

--
-- Structure de la table `t_points_de_vente`
--

CREATE TABLE `t_points_de_vente` (
  `id_pdv` int(11) NOT NULL,
  `NomPDV` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_points_de_vente`
--

INSERT INTO `t_points_de_vente` (`id_pdv`, `NomPDV`) VALUES
(1, 'Pharmacie des Maccaudo'),
(2, 'Pharmacie de la Batelière SA'),
(3, 'Pharmacie de Chailly'),
(4, 'Pharmacieplus de la Fauvette SA'),
(5, 'Pharmacie Amavita St. Laurent'),
(6, 'Pharmacie Amavita Conod'),
(7, 'Pharmacie Internationale Golaz AMA080'),
(8, 'Pharmacie METRO FLON SA'),
(9, 'Pharmacieplus du Flon SA'),
(10, 'SUN STORE SA 491'),
(11, 'Pharmacie Amavita'),
(12, 'SUN STORE SA 251'),
(13, 'Pharmacie Amavita Théâtre'),
(14, 'Pharmacie Populaire'),
(15, 'Amavita NutriBio La Palud'),
(16, 'Pharmacie Luc Geny'),
(17, 'PharmaciePlus Nouvelle SA'),
(18, 'Pharmacie de Ruchonnet'),
(19, 'Pharmacie de Chauderon SA'),
(20, 'Droguerie de St. Sulpice'),
(21, 'Pharmacie de St. Sulpice'),
(22, 'SUN STORE SA 771'),
(23, 'SUN STORE SA 381'),
(24, 'Pharmacie Plus de Marterey SA'),
(25, 'Pharmacie Etraz'),
(26, 'Pharmacie St-Paul'),
(27, 'SUN STORE SA 361'),
(28, 'Pharmacieplus de la Cigale SA'),
(29, 'Pharmacie Populaire de la Sallaz'),
(30, 'Pharmacie Etoile'),
(31, 'Pharmacie Kupper'),
(32, 'Pharmacie de Valency'),
(33, 'Pharmacie du Valentin'),
(34, 'Droguerie l\'Herboriste'),
(35, 'Pharmacie du Rocher SA'),
(36, 'Pharmacie des Mousquines'),
(37, 'Pharmacie Gamma'),
(38, 'Pharmacie Metro Ouchy'),
(39, 'Droguerie du Boulevard'),
(40, 'Pharmacie Populaire de Grancy'),
(41, 'Pharmacie 24 SA'),
(42, 'Pharmacie de Montolivet'),
(43, 'Pharmacie Lardelli SA'),
(44, 'Pharmacie du Boulevard'),
(45, 'Pharmacie Closelet'),
(46, 'Pharmacie Amavita La Harpe'),
(47, 'Pharmacie de Provence'),
(48, 'Pharmacie de Montelly'),
(49, 'Pharmacie Vidy-Pharm'),
(50, 'Pharmacie Amavita Mont d\'Or');

-- --------------------------------------------------------

--
-- Structure de la table `t_potentiel_client`
--

CREATE TABLE `t_potentiel_client` (
  `id_potentiel_client` int(11) NOT NULL,
  `AttribPotClient` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_potentiel_client`
--

INSERT INTO `t_potentiel_client` (`id_potentiel_client`, `AttribPotClient`) VALUES
(1, 'A-Pot'),
(2, 'A-Pot'),
(3, 'A-Pot'),
(4, 'A-Pot'),
(5, 'A-Pot'),
(6, 'A-Pot'),
(7, 'A-Pot'),
(8, 'A-Pot'),
(9, 'A-Pot'),
(10, 'B-Pot'),
(11, 'B-Pot'),
(12, 'B-Pot'),
(13, 'B-Pot'),
(14, 'B-Pot'),
(15, 'B-Pot'),
(16, 'B-Pot'),
(17, 'B-Pot'),
(18, 'B-Pot'),
(19, 'B-Pot'),
(20, 'B-Pot'),
(21, 'B-Pot'),
(22, 'B-Pot'),
(23, 'B-Pot'),
(24, 'A-Pot'),
(25, 'A-Pot'),
(26, 'A-Pot'),
(27, 'A-Pot'),
(28, 'A-Pot'),
(29, 'A-Pot'),
(30, 'A-Pot'),
(31, 'A-Pot'),
(32, 'A-Pot'),
(33, 'C-Pot'),
(34, 'C-Pot'),
(35, 'C-Pot'),
(36, 'C-Pot'),
(37, 'C-Pot'),
(38, 'C-Pot'),
(39, 'C-Pot'),
(40, 'C-Pot'),
(41, 'C-Pot'),
(42, 'A-Pot'),
(43, 'A-Pot'),
(44, 'B-Pot'),
(45, 'B-Pot'),
(46, 'A-Pot'),
(47, 'C-Pot'),
(48, 'C-Pot'),
(49, 'B-Pot'),
(50, 'B-Pot');

-- --------------------------------------------------------

--
-- Structure de la table `t_produits`
--

CREATE TABLE `t_produits` (
  `id_produits` int(11) NOT NULL,
  `MarqueProduit` varchar(80) DEFAULT NULL,
  `DescriptionProduit` varchar(200) DEFAULT NULL,
  `Pharmacode` varchar(30) DEFAULT NULL,
  `ImageProduit` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_produits`
--

INSERT INTO `t_produits` (`id_produits`, `MarqueProduit`, `DescriptionProduit`, `Pharmacode`, `ImageProduit`) VALUES
(1, 'Alpecin', 'Alpecin Forte Intensiv Haartonikum 200 ml', '2783561', 'https://www.vitaminplus.ch/2573-tm_thickbox_default/alpecin-forte-intensiv-haartonikum-flasche-200-ml.jpg'),
(2, 'Alpecin', 'Alpecin Fresh Vital Haartonikum 200 ml', '2787487', 'https://www.coopvitality.ch/media/catalog/product/cache/74c1057f7991b4edb2bc7bdaa94de933/2/7/2787487-2787487-main_image-2787487_picfront3dplus_f.jpg'),
(3, 'Alpecin', 'Alpecin Special Haartonikum 200 ml', '2787435', 'https://www.amavita.ch/media/catalog/product/cache/030716fc62035027b622eeef186d3d67/2/2/2232631-2232631-main_image-2232631_picfront3d_f.jpg'),
(4, 'Alpecin', 'Alpecin Silver Mineral Haartonikum 200 ml', '2787518', 'https://www.coopvitality.ch/media/catalog/product/cache/74c1057f7991b4edb2bc7bdaa94de933/2/7/2787518-2787518-main_image-2787518_picfront3dplus_f.jpg'),
(5, 'Alpecin', 'Alpecin Shampoo-Konzentrat Anti-Schuppen 200 ml', '2783667', 'https://www.vitaminplus.ch/7487-tm_large_default/alpecin-shampoo-konzentrat-anti-schuppen-200-ml.jpg'),
(6, 'Alpecin', 'Alpecin Shampoo -Konzentrat fettendes Haar 200 ml', '2783762', 'https://cdn.shopify.com/s/files/1/0150/2268/6256/products/4008666206213_300x300.jpg?v=1566115572'),
(7, 'Alpecin', 'Alpecin Liquid 200 ml', '2232559', 'https://cdn.webshopapp.com/shops/252923/files/183523199/750x650x2/alpecin-liquid.jpg'),
(8, 'Alpecin', 'Alpecin Aktiv Shampoo A1 (normales Haar) 250 ml', '2232648', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-active-shampoo-a1-germany-de.png'),
(9, 'Alpecin', 'Alpecin Aktiv Shampoo A2 (fettiges Haar) 250 ml', '2232631', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-active-shampoo-a2-germany-de.png'),
(10, 'Alpecin', 'Alpecin Aktiv Shampoo A3  (gegen Schuppen) 250 ml', '2232588', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-anti-dandruff-shampoo-a3-germany-de.png'),
(11, 'Alpecin', 'Alpecin Sensitiv Shampoo S1 (empfindliche Haut) 250 ml', '3280879', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-senstive-shampoo-s1-germany-de.png'),
(12, 'Alpecin', 'Alpecin Coffein-Shampoo C1 250 ml', '2990320', 'https://storage.googleapis.com/static-shops/products/720/a33e1019fa4732324cefb669b52f3e15f185.jpg'),
(13, 'Alpecin', 'Alpecin Doppel-Effekt Shampoo  200 ml', '3299821', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/CH/alpecin-packshot-double-effect-caffeine-shampoo-switzerland.png'),
(14, 'Alpecin', 'Alpecin Tuning-Shampoo 200 ml', '5031465', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-tuning-shampoo-germany-de.png'),
(15, 'Alpecin', 'Alpecin Schuppen-Killer Shampoo 250 ml', '4868801', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-dandruff-killer-shampoo-germany-de.png'),
(16, 'Alpecin', 'Alpecin PowerGrau Shampoo 250 ml', '5235251', 'https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-powergrey-shampoo-germany-de.png'),
(17, 'Pantur', 'Plantur39 Coffein-Shampoo 250 ml', '3280974', 'https://www.vitaminplus.ch/2162-tm_large_default/plantur-39-coffein-shampoo-feines-bruechiges-haar-250-ml.jpg'),
(18, 'Pantur', 'Plantur39 Coffein-Shampoo Color 250 ml', '3410069', 'https://www.plantur39.com/fileadmin/user_upload/plantur39.com/images/products/DE/plantur39-packshot-phyto-coffein-shampoo-coloriertes-strapaziertes-haar-germany-de.png'),
(19, 'Pantur', 'Plantur21 Nutri-Coffein Shampoo 250 ml', '5064298', 'https://www.plantur21.com/fileadmin/user_upload/plantur21.com/images/products/DE/plantur21-packshot-langehaare-nutri-caffeine-shampoo-germany-de.png'),
(20, 'Pantur', 'Plantur21 Nutri-Coffein Elixir 200 ml', '5525954', 'https://www.plantur21.com/fileadmin/user_upload/plantur21.com/images/products/DE/plantur21-packshot-nutri-coffein-elixir-germany-de.png'),
(21, 'Pantur', 'Plantur21 Nutri-Conditioner 150 ml', '6429939', 'https://www.plantur21.com/fileadmin/user_upload/plantur21.com/images/products/DE/plantur21-packshot-nutri-conditioner-germany-de.png'),
(22, 'Pantur', 'Plantur21 Hydro-Spray 100 ml', '6429922', 'https://www.shampoo-shop.ch/contents/media/l_plantur21_hydro_.png'),
(23, 'Pantur', 'Plantur39 Coffein-Tonikum 200 ml', '3281034', 'https://www.vitaminplus.ch/2164-tm_large_default/plantur-39-coffein-tonikum-flasche-200-ml.jpg'),
(24, 'Pantur', 'Plantur39 Struktur-Pflege 30 ml', '3281040', 'https://www.plantur39.com/fileadmin/user_upload/plantur39.com/images/products/DE/plantur39-packshot-struktur-pflege-germany-de.png'),
(25, 'Pantur', 'Plantur39 Pflege-Spülung für feines, brüchiges Haar 150 ml', '3739556', 'https://www.plantur39.com/fileadmin/user_upload/plantur39.com/images/products/DE/plantur39-packshot-pflege-spuelung-feines-bruechiges-haar-germany-de.png'),
(26, 'Pantur', 'Plantur39 Pflege-Spülung für coloriertes u. strapaziertes Haar 150 ml', '3739562', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8SDxAQEBAQDw0PEA8PDw4PDRAPDQ8PFxEWFhYRFRUYHSggGBolGxUWITEhJSkrLi4wFyA/ODMuNygtOisBCgoKDg0OGxAQGysdHyU3KystLy0rLS0tLSsuLTIuLSstLS0tLS4tLS0wLS0tLSstLS8tLS0tKystLS0tLS0tLf/AABEIAQsAvQMBEQACEQEDEQH/xAAbAAEAAQUBAAAAAAAAAAAAAAAABAECAwUGB//EAEEQAAIBAgMCCggEBQMFAQAAAAABAgMRBBIhEzEFBiJBUVJhcZGhFjJTgZKTwdIUorHCFSMzYnJCsuE0Y3Oj0Qf/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIEAwUG/8QAMxEBAAEDAQQHBwUAAwAAAAAAAAECAxEhBBIxURMiMkFSYaEFFHGBsdHwFSORweEzQmL/2gAMAwEAAhEDEQA/APcQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAczxy4SxFDYbGbgp7RTtCErtZbesn0sw7bduW93cnHHl5OF6qqnGHNrjLj/AG0vkUvsMPvV/wAXpH2celr5q+kfCHtpfIpfYPer/i9I+x0lzmekfCHtpfJpfaT71f8AF6R9jpLnM9IuEPbS+TS+0j3q/wCL6fY6S5zPSLH+2l8ml9o96v8Ai+n2Okr5npFj/bS+TS+0e9X/ABfT7HSV8z0ix/tpfJpfaPer/i+n2Okucz0jx/tpfJpfaPer/i9I+yekr5npHj/bS+RS+we9X/F6R9kdLXzHxlx/tpfJpfYPer/i9I+x0tfN3/BdScqFGU3epKlTlN2SvJxTei3HsWpmbdM1ccQ10zM0xlKOiwAAAAAAAAAAAAHM8fad8PTlzxrLwcJf/EYPaEftxPm4X46rjqUjy4lnhlJWGBY0whblYQqosJXJMC4JGwItZ30W9lKtdFJetUoZYqPNFJeCPpIjEYehC4kAAAAAAAAAAAAA03G+nmwdTpi6cvzpPybMu2RmzP53ud2Oq87pM8SGOEqLLrLgkJAAAAMgY6jEoODKefEUo8zq00+5zV/IWY3rlMecIp1qh6qfRN4AAAAAAAAAAAAACFw3TzYWvHndGpb/ACUW15nK9TvW6o8pVrjNMvLnv7z55h70imy8LMqJSIkLAVyvodumwwKAUbIGCrIrMqy2HFSlmxdHoUpSfuhJrzsd9jpzeha1HWh6Se62gAAAAAAAAAAAAAKSimmnuaafcB5FODVk98eS+9aM+amMTh58r6ciYTDouAsDQqUasqt4tTpwjUTd4uVktNzV2t5t2e1broma+fH4u1FMTGqNiODa1Jyk0mqdVU79aVlJab7NNHOqzXb15ThE0zDBDFzTukrqOV6N2jfc/NX7WVi5VnMK5lmW1nRqTWXZQcVO2ju5K1lz62LdeqiZ7k6zGUBnBVjmwhHqMpKsuj4j0r4lvmjRn4ucUvJM3ez6f3JnydrMdZ3Z67UAAAAAAAAAAAAAAAeW8LUstavHor1bdzm2vJnz9+nFyqPOWGqNZQYs4qQ6ri5hnVwlanHRyrUbvoipRk34Jno7NRNdqqmOcf00W4zTMNripyrxvQaT/GxyT3xWSkrz7VyX36GiuZux1PF9IdJ63Dmz1p5HUrRcZVPwc5OajaNSUNVJpHSqd3Ncazu/zhM6a+RUls1UyJRUqmFnlSVk6lSMZ270hM7kTu86fWcScOHkxToxm6kJK8Xj43T3O1GDs+wruxVMxPi/qEYz/LkeHsZKpXqOVuRKVKCSslCMnZHmbRcmu5Oe7T+Ge5VmprFqzO5uy4h0+ViJdlGC/O3+qPU9nx2p+DTYjWXXHpNAAAAAAAAAAAAAAAB5zxlp2xmIXM3TkvfSjfzueHtkYvVfL6MdyOtLSPeZXJ0fB2Jl/DcSk7ZJ00nHSTU5xTTfPz+JutVz7tVHnHrh3pn9uUjgDhmlRowjK+bbuUkot2g4Zc1+/mL7PtFFuiInn6Jt1xTCTieEsNGk6UKudfhKlCLyyu5NWjfTQvXetRRuUznqzC01U4xHJbjuHKLpzySbnbCuCcZJOVOpmab9wubTRNM7s66ek5RVcjGnkz/xvCK01OScq6rVIShJyg9lkdrLVaLp3l/ebXazxnM/xhbpKeLjcbUUqlSS3SnOS7nJtHlVzmqZjnLLVOrHRWpWCHc8RYfya0utXaXcqcF+tz2NgjFuZ82qzwl0pudgAAAAAAAAAAAAAADhOOMLYz/OhTl71Ka+iPH2+P3fky3e25yotTC4Sy0cVOMJ01JqnUcXONlaTTui0V1RTNMTpKYmcYKcrNPodxE41TDvadqjw7yUpwcJ1HOnFbOVdLSC57LlPXq9h7Udbd0iY46cM8mvjhoOHZuph8NXnFRrzdWE7Ry3UZNK67LeZi2id+3TXVGJ1ca9aYmXPyZhlxYmQhmoImFqXfcTIWwcH1p1pf8Askv0SPb2KMWY+f1arPYbw1OoAAAAAAAAAAAAAABxnHmFq+Hl1qdWPwyi/wBx5ftCNaZ+LPe7UOYrI86XCWFEKssSYS6nAxdDCJVZuhKtXhOm7XnTirXnl7la3auk9G3E27PWndzOnk0U9WnXRruMfCqr1I5b7KmmouStKTdrya5r2XgcNqv9LVpwhS5XvS0s2ZHJYkEJMNEyV44PROLULYPD9tKM/iWb6nv7PGLVPwhrtdiGzOy4AAAAAAAAAAAAAABynH2HJw8+ipOHxRv+w8/2hHVpnzcL/dLk6qPKlwlHaIUSVhpxSl0KM7p+qmsyd+k6dHVEb3zWxKVjFiKktpUbnJp3d1yUs2llpH1ZafU6Vxcrneq1/P8AFp3p1lgeEqWTy6NZl2rLF3/MvMpNurHD8/JRuyj16Uou0lZ66Pfva/VM51UzTOJVmMLYIghlrO0H3MmVp4PUOD6WSjSh1KdOPhFI+jojFMQ20xiIhILJAAAAAAAAAAAAAAAOc49wvhYS6lenLxUo/uMe3Rm1nlLjf7LjZbjx3CeCPNFVJS8Is0WpVssdVkcknJqHJ3tLmijtR1qdaseXyWjWNZSKcrpJ4lp8p3c+SmpWvZ9ZNNe86U6x2/z/AFPzUoU3ujXd2rxUWr3UY6JZui8Vu9Uimnuir808/l8iI82vxDeeV5Zmm1mb32b1OFczmczlSeKtNEQmF9aGa0Ou4w8Xb6lojMxCauT1c+jbgAAAAAAAAAAAAAAABpeOML4Gt/bs5eFWLflczbXGbNTle7EuEhuPEZ44MNRFZVliCpcmAuRIrECRTRMLwkYKGbE4ePTXot9ymm/JHWzGblMecJ41Q9PPoG0AAAAAAAAAAAAAAAAa/jBTzYPErn2FVrvUG15o5X4zbqjylS52JecYd8lHgMlPBSoiJRKlDCVKik4RclC2azV9U2kk3du0XougtRbqrzuxnCIpmeC3F4KrTy7SGXMm1yoydla90npa60dt5aq3VR2oJpmOLDFPoe6+57uk5zEqr4ELQkwLLwm8X45sfh1zKU5P3UpNedjRskZvU/ncmjtw9IPcbAAAAAAAAAAAAAAAABjxFPNCcetGUfFWImMxhE6w8owMrwXcj5uGGjgyVUJTLIq9ahTkknD8RklGemfLGMt3Orqpv07N51iqu3Ty3vz+zM0x8WwXDmIalNUotONSbkqkXKMVkbfKT0VlpbW/YaveLkxnHOeK3SVccE+MyShkTlLLLO5JRUZPZ2UE3K0Vs927UirbMY3fzhw48jpuTTqbbu7bkuTCMF4RSRhzlzZ47iXSG04nRvjv8KNSXnGP7jZsMfu/JNrtvQD2GsAAAAAAAAAAAAAAAAAPJ4QyzqQ6lSpDwk19D52uMVzHxYKdJwyTptq6Ta6bMpK80zPc2GF4Z2cUnTqZoU4xjJVGpzaqU5ZXPLeMLQfJ19Zq9mare0xTGMcMd/nHfy04fJMVTHcwekUkklQSlaKbztRvGCirRtorRWly0bXiOHqr0kx3Ky4fk1ljRcYNTTi6jkpKUaytLkq6vWvb+xe6te1TPCPX4/f0Oknl+a/ddwxi1WnBwVRqMZXclL1pVJTsr35KzJLuKbRdpuVRj81ytVmqdIlEaa36PtOKMY4t1xDhfE15dWlGPxSv+03+z461UrWO1LuT1WoAAAAAAAAAAAAAAAAAPLuEYZcZiY/96pL4nm+p4O0Ri7UwzpXLZUK040qeSpTp/wBRPay5OrbTtz7nfsvu3rJMRNU5jL2dln9qPmrUxNdq34ihG6b1upclK71WnTu3ERTTylozKksXNOb/ABGHcb1XG9RNrW0Iuy5nKCe/euffG7E40lbM81IY6d3fEYVRbtH+ZeyUra82bXd027bzuRjhKJmebJ+KrJuMsRQUo2Ullatdtq91vyr9Ru08pRmeaFwpK9STuneMHdbnyI6ovR2XkbX/AMs/L6Nx/wDnsP8Aqp9MqUPhUn+49b2fGlUuWz98uxPRaQAAAAAAAAAAAAKAUuAzAUzAec8ZYZeEK3RNUpr5cV+qZ4m2Ri9PyYrmlySljVGKTpU6mXNZyV2k96MdVOdctVvapt07uMrKnC8OfDUnZZeZcnXTdu1fiVimea/6jPhj+UT+KUk9MLTvrrm11eq3buwtu1eJH6l/49f8ZqXDMNLYamsrbjyvVbSTtydNEl7iJonmn9Snwev+JC4VhK18NR0sldJ2XMloRFE85P1CZ/6wwYytnblZK9tFuVkl9DpEYjDNduTXVNUun4gRthqkn/rrza7lCEf1TPY2CMW8+a2zx1XTZja7q3AqAAqAAAAAAABQCjYGKUgMbmBa6gHEccY2xkJc06MV74zl9GjydvjrxPkyX+3DXy3GFWeCFWKw5yiTLqSvpESmE2gRC9LNXfJZK9XB2PFFZcFS6ZbSfjUk15WPb2SMWYaLEYohuVUNLquUwMsZAXoC5AAAAAAAAUYFkwME2BHnMDDKoBzXHOOaFFrSSqNKS32cdVf3LwMG3xG7Es+0RpDntnO39SXvUWeZiGfXmjVIz6/5EMQrMSjzpT66+D/knRXC+nTn118H/I0IhJpRn1/CCIxC0Z5q4qE8vryfgvoMQmrPN6HhGo04RSSUYRSS0SSSPoKYxTEQ9CmMQkxqFks0JASIAZ0BcAAAAAAAAAtkgI1RAQ6oEWowNDxp/p0//Kv9sjDt3Yj4/wBOG0cIapLknluDX19L21fN0EwpLXVo1H/rkv8AHkryOkTCuWahtFveZdu/3MrVMSnLZ4aK3lFoMf6viCp21N6LuR9DD0ISKbJEykgJVNAZ0BUAAAAAAAABRgYakQIlaAEGrEDn+Nd9jBpX/mx/2SMW3f8AHHx/qXDaOzDSxrO26Xws8tnywSlfWz+FkKsM12S+CRKF0O6XwSISz0aluZ/CwmFmNqNrRP3pr9SUVO7hHmPoHopdGBInUogSoRAvAAAAAAAAAAAFJICPVpgQ6tEDUcO4DaUZK6Tjaab0V1zM4bRb37cxwc7lO9Thx+2cVaUWrdjPIm1XHcy6wwVK0Xva8bMpuzHcpLBKcOu/jJ15IXQnDrX75MYkSKeIS7e4blXJaEzAYV16sI+rG6bctLpcy7TtZsVVVRnRemiap1dzTontNqXSpAS6cAMqAAAAAAAAAAAAABRoDHKkBqeMMLUl2zivJv6HG/2VLnBpNimtxicmGdCHOo+8aoY3QpdEfAnUVjQp9EfCw1GaFCPQiNRWpHLZpWs7kxxHYxonpNLJGAF4AAAAAAAAAAAAAAAABreHYXpxX96f5Wcb0ZpUr4NV+HXQZt1TCx4VdAwjCx4XsGEYVWFXQMJwvjhl0DdMKVsPpu8hukw6lG93VAAAAAAAAAAAAAAAAAAEbHQul3/QpXGYVqQ9gc91XBsBumDYDdMGwG6YNgN0wPDjdMNotx3dFQAAAAAAAAAAAAAAAAABSUbgW7NEYRhTZIYMGzQwYNmMGFdmhhKqghgXEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//9k='),
(27, 'Pantur', 'Plantur39 Haar-Aktiv-Kapseln 60 Kaps.', '6871121', 'https://www.vitaminplus.ch/5312-tm_large_default/plantur-39-haar-aktiv-kapseln-60-stueck.jpg'),
(28, 'Pantur', 'Plantur39 Sprüh-Kur 125 ml', '4359442', 'https://www.plantur39.com/fileadmin/user_upload/plantur39.com/images/products/DE/plantur39-packshot-die-taegliche-sprueh-kur-germany-de.png'),
(29, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Sang & Protéines 50ml', '5856786', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856786-5856786-main_image-5856786.jpg'),
(30, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Matières Grasses & Sauces 50ml', '5856763', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856763-5856763-main_image-5856763.jpg'),
(31, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Bureau & Bricolage 50ml', '5856792', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856792-5856792-main_image-5856792.jpg'),
(32, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Stylos & Encre 50ml', '5856728', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856728-5856728-main_image-5856728.jpg'),
(33, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Fruits & Boissons 50g', '5856800', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856800-5856800-main_image-5856800.jpg'),
(34, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Rouille & Déodorant 50ml', '5856740', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856740-5856740-main_image-5856740.jpg'),
(35, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Lubrifiants/Huiles 50ml', '5856734', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856734-5856734-main_image-5856734.jpg'),
(36, 'Dr. Beckmann', 'Dr. Beckmann Diable détacheur Nature & Cosmétiques 50ml', '5856757', 'https://www.sunstore.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/8/5856757-5856757-main_image-5856757.jpg'),
(37, 'Dr. Beckmann', 'Dr. Beckmann Lingette Anti-Décoloration à Microfibre 24 pce', '5856823', 'https://www.amavita.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/7/6/7673877-7673877-main_image-7673877_picfront3d_f.jpg'),
(38, 'Dr. Beckmann', 'Dr. Beckmann Lingette Anti-Décoloration à Microfibre + Inhibiteur de Décoloration 28 pce', '7237975', 'https://www.amavita.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/7/6/7673877-7673877-main_image-7673877_picfront3d_f.jpg'),
(39, 'Dr. Beckmann', 'Dr. Beckmann Lingette Anti-Décoloration à Microfibre + Inhibiteur de Décoloration 22 pce', '7673877', 'https://www.amavita.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/7/6/7673877-7673877-main_image-7673877_picfront3d_f.jpg'),
(40, 'Dr. Beckmann', 'Dr. Beckmann Lingette Anti-Décoloration Réutilisable 1 pce', '5856817', 'https://www.dr-beckmann.ch/typo3temp/_processed_/f/f/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Farb-Schmutz-Faenger-MWT-CH-Website-Pack_c4432e6a84_4c21548c80.jpg'),
(41, 'Dr. Beckmann', 'Dr. Beckmann Lingette Anti-Décoloration Ultra  10 pce', '6828958', 'https://www.dr-beckmann.ch/typo3temp/_processed_/5/e/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Farb-Schmutz-Faenger-22-CH-Website-Packs_fe801799e7_ea91796cfd.jpg'),
(42, 'Dr. Beckmann', 'Dr. Beckmann Sel à détacher Intensif 500g', '5856668', 'https://www.dr-beckmann.ch/typo3temp/_processed_/d/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Fleckenbuerste_Oxbile_250ml-CH-Website-P_544fc34616_aaf6fa220d.jpg'),
(43, 'Dr. Beckmann', 'Dr. Beckmann Super Blanc 2x40g', '3135717', 'https://www.dr-beckmann.ch/typo3temp/_processed_/e/c/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Super-Weiss-CH-Website-Packshot_01042019_68cc746b58_d1eb232032.jpg'),
(44, 'Dr. Beckmann', 'Dr. Beckmann Blanc & Hygiène 500g', '6136605', 'https://www.dr-beckmann.ch/typo3temp/_processed_/a/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Hygiene-Weiss-CH-Website-Packshot_010420_1af4673d6f_9518327085.jpg'),
(45, 'Dr. Beckmann', 'Dr. Beckmann Lingettes blanchissantes, 15 Lingettes', '7237981', 'https://www.dr-beckmann.ch/typo3temp/_processed_/8/c/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Aktiv-Weiss-CH-Website-Packshot_01042019_9fb597feab_0a488fcc0c.jpg'),
(46, 'Dr. Beckmann', 'Dr. Beckmann Brosse détachant au fiel naturel 250ml', '5856645', 'https://www.dr-beckmann.ch/typo3temp/_processed_/d/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Fleckenbuerste_Oxbile_250ml-CH-Website-P_544fc34616_aaf6fa220d.jpg'),
(47, 'Dr. Beckmann', 'Dr. Beckmann Spray détachant au fiel naturel 250ml', '5856651', 'https://www.dr-beckmann.ch/typo3temp/_processed_/5/f/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Fleckenspray_250ml-CH-Website-Packshot_0_1f70eec29b_358a727668.jpg'),
(48, 'Dr. Beckmann', 'Dr. Beckmann Détachant Déodorant & Sueur 250ml', '7237998', 'https://www.dr-beckmann.ch/typo3temp/_processed_/0/2/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Fleckenspray-Deo-Schweiss-CH-Website-Pac_f842cfb397_47b2682f3c.jpg'),
(49, 'Dr. Beckmann', 'Dr. Beckmann Parfum de Linge Douceur de printemps 250ml', '6146207', 'https://www.dr-beckmann.nl/uploads/pics/contentelements/standard_wasgoedfris_parfum_linge_BNL_01.jpg'),
(50, 'Dr. Beckmann', 'Dr. Beckmann Parfum de Linge Fraîcheur de l\'aube 250ml', '6146213', 'https://www.dr-beckmann.nl/uploads/pics/contentelements/standard_wasgoedfris_parfum_linge_BNL_01.jpg'),
(51, 'Dr. Beckmann', 'Dr. Beckmann Neutralisateur d\'odeurs 500ml', '6506806', 'https://www.dr-beckmann.ch/typo3temp/_processed_/6/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Geruchs-Entferner-CH-Website-Packshot_01_6088a4fdc9_443372f923.jpg'),
(52, 'Dr. Beckmann', 'Dr. Beckmann Parfum de Linge Spring 250 ml', '7640122', 'https://www.dr-beckmann.ch/typo3temp/_processed_/6/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Geruchs-Entferner-CH-Website-Packshot_01_6088a4fdc9_f0038d35fc.jpg'),
(53, 'Dr. Beckmann', 'Dr. Beckmann Parfum de Linge Fresh 250 ml', '7640139', 'https://www.dr-beckmann.ch/typo3temp/_processed_/6/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Geruchs-Entferner-CH-Website-Packshot_01_6088a4fdc9_f0038d35fc.jpg'),
(54, 'Dr. Beckmann', 'Dr. Beckmann Boule Sèche-Linge 1 pce', '6917538', 'https://www.cdiscount.com/pdt2/5/2/2/1/700x700/47522/rw/dr-beckmann-boule-seche-linge-parfum-de-linge.jpg'),
(55, 'Dr. Beckmann', 'Dr. Beckmann Roll-On détacheur bille 75ml', '5856846', 'https://www.dr-beckmann.ch/typo3temp/_processed_/3/6/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Fleckenroller-CH-Website-Packshot_120420_57be855161_2cb80f399e.jpg'),
(56, 'Dr. Beckmann', 'Dr. Beckmann Express Crayon détacheur 9ml', '3396760', 'https://www.dr-beckmann.ch/typo3temp/_processed_/7/4/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Flecken-Stift-CH-Website-Packshot_120420_36187f24f2_313496a7d8.jpg'),
(57, 'Dr. Beckmann', 'Dr. Beckmann Savon détachant au fiel de boeuf 100g', '1879968', 'https://www.dr-beckmann.ch/typo3temp/_processed_/1/e/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Gallseife-Stueck-CH-Website-Packshot_290_264b758f7f_c5bfbc1d88.jpg'),
(58, 'Dr. Beckmann', 'Dr. Beckmann Savon détachant 100g', '6828987', 'https://www.dr-beckmann.ch/typo3temp/_processed_/1/e/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Gallseife-Stueck-CH-Website-Packshot_290_264b758f7f_c5bfbc1d88.jpg'),
(59, 'Dr. Beckmann', 'Dr. Beckmann Détach\'UP Push & Wipe 3x4ml', '6917544', 'https://www.dr-beckmann.co.uk/wp-content/uploads/PushWipe-1.jpg'),
(60, 'Dr. Beckmann', 'Dr. Beckmann Rénovateur Lave-Linge 250ml', '5856697', 'https://www.dr-beckmann.ch/typo3temp/_processed_/e/f/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Waschmaschinen-Pflege-Reiniger-CH-Websit_77dceb56ac_ef34819050.jpg'),
(61, 'Dr. Beckmann', 'Dr. Beckmann Nettoyant frigo 250ml', '5856680', 'https://www.dr-beckmann.ch/typo3temp/_processed_/3/5/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Kuehlschrank_Hygiene_Trigger_CH_NewGen_1245x1245px_3_32a2471848_1880b19bdd.jpg'),
(62, 'Dr. Beckmann', 'Dr. Beckmann Nettoyant & Hygiène Lave-Vaisselle 75g', '5856705', 'https://www.dr-beckmann.fr/typo3temp/_processed_/9/6/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Nettoyant_Hygiene-Lave-Vaisselle-FR-Webs_6549dd1a35_3e62a864c6.jpg'),
(63, 'Dr. Beckmann', 'Dr. Beckmann Nettoyant Lave-Linge Hygiène 250g', '5856674', 'https://www.dr-beckmann.ch/typo3temp/_processed_/5/e/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Waschmaschinen-Hygiene-Reiniger-CH-Websi_d6692f11d7_c72c8f136e.jpg'),
(64, 'Dr. Beckmann', 'Dr. Beckmann Frigo-fraîche limone 40g', '5980204', 'https://www.dr-beckmann.ch/typo3temp/_processed_/1/5/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Kuehlschrank-Frische-Reiniger-CH-Website_8150508be7_371b4d1a6c.jpg'),
(65, 'Dr. Beckmann', 'Dr. Beckmann Gel Actif Nettoyant Four 375ml', '6136597', 'https://www.dr-beckmann.ch/typo3temp/_processed_/1/3/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Backofen-Aktiv-Gel-CH-Website-Packshot-0_05e208aa4a_1a02b5c85d.jpg'),
(66, 'Dr. Beckmann', 'Dr. Beckmann Pierre d\'argile 400g', '6506522', 'https://www.dr-beckmann.ch/typo3temp/_processed_/a/7/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Putzstein-CH-Website-Packshot-012020_e03_7d3948ab09_fb9666dff3.jpg'),
(67, 'Dr. Beckmann', 'Dr. Beckmann Pierre Induction & Vitro 250g', '6828964', 'https://www.dr-beckmann.fr/typo3temp/_processed_/6/e/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Pierre-Induction_Vitro-FR-Website-Packsh_c2a80c850d_2f28c51af2.jpg'),
(68, 'Dr. Beckmann', 'Dr. Beckmann Stylo Nettoyant Joints 36ml', '6828970', 'https://m.media-amazon.com/images/I/41cCab0Es7L.jpg'),
(69, 'Dr. Beckmann', 'Dr. Beckmann Nettoyant Puissant Induction & Vitro 250 ml', '7640145', 'https://www.dr-beckmann.ch/typo3temp/_processed_/e/6/csm_csm_product-background-1245x1245_6d68f7ced5_csm_Dr-Beckmann-Kochfeld-Kraft-Reiniger-CH-Website-Packs_c8d93eab3c_d74bc8ad9b.jpg'),
(70, 'Linola', 'LINOLA Emulsion 40 g', '876086', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/DE/linola-packshot-linola-fett-germany.png'),
(71, 'Linola', 'LINOLA Emulsion 100 g', '876092', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/DE/linola-packshot-linola-fett-germany.png'),
(72, 'Linola', 'LINOLA GRAS émulsion 40 g', '876100', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/DE/linola-packshot-linola-fett-germany.png'),
(73, 'Linola', 'LINOLA GRAS émulsion 100 g', '876117', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/DE/linola-packshot-linola-fett-germany.png'),
(74, 'Linola', 'LINOLA Crème mi-gras tb 50 ml', '3593549', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/Linola-Halbfett_CH.png'),
(75, 'Linola', 'LINOLA Crème mi-gras tb 100 ml', '3593532', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/Linola-Halbfett_CH.png'),
(76, 'Linola', 'LINOLA Lait pour le corps 200 ml', '3299910', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/linola-packshot-lotion-switzerland-ch.png'),
(77, 'Linola', 'LINOLA Shampooing fl 200 ml', '3299867', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/linola-packshot-shampoo-switzerland-ch.png'),
(78, 'Linola', 'LINOLA Crème visage 50 ml', '4551412', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/linola-packshot-face-switzerland-ch.png'),
(79, 'Linola', 'LINOLA Douche 300 ml', '4551406', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/linola-packshot-shower-and-wash-switzerland-ch.png'),
(80, 'Linola', 'LINOLA Baume protecteur 50 ml', '6011039', 'https://www.vitaminplus.ch/13273-tm_large_default/linola-schutz-balsam-100-ml.jpg'),
(81, 'Linola', 'LINOLA Baume protecteur 100 ml', '6385811', 'https://www.vitaminplus.ch/13273-tm_large_default/linola-schutz-balsam-100-ml.jpg'),
(82, 'Linola', 'LINOLA Plus Crème 50 ml', '6744925', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/DE/linola-plus-packshot-creme-germany.png'),
(83, 'Linola', 'LINOLA Plus Lait pour le corps 200 ml', '6744931', 'https://www.linola.com/fileadmin/user_upload/linola.com/images/products/CH/linola-packshot-lotion-switzerland-ch.png'),
(84, 'Alpicort-F', 'ALPICORT F sol 100 ml', '1563047', 'https://www.manovaistine.lt/private/uploads/images/products/alpicort-tirp-100ml.png'),
(85, 'Arilin', 'ARILIN 500 cpr pell 500 mg 20 pce', '1169652', 'https://documedis.hcisolutions.ch/2020-01/api/products/image/PICFRONT3D/productnumber/2064/350'),
(86, 'Vagisan', 'VAGISAN Crème lubrifiante tb  25 g', '5654605', 'https://www.amavita.ch/media/catalog/product/cache/9d08971813a040f8f96067a40f75c615/5/6/5654605-5654605-main_image-packshot_vagisan_feuchtcreme.jpg'),
(87, 'Vagisan', 'VAGISAN Crème lubrifiante tb  50 g', '4445578', 'https://www.vagisan.com/fileadmin/user_upload/vagisan.com/images/products/BE/vagisan-packshot-moistcream-belgium.png'),
(88, 'Vagisan', 'VAGISAN Crème lubrifiante Combi 10g & 8 Cremolum', '5033725', 'https://www.vagisan.com/fileadmin/user_upload/vagisan.com/images/products/BE/vagisan-packshot-moistcream-belgium.png'),
(89, 'Vagisan', 'VAGISAN ovules acide lactique blist 7 pce', '4445584', 'https://www.vagisan.com/fileadmin/user_upload/vagisan.com/images/products/CH/vagisan-packshot-lactic-acid-switzerland.jpg'),
(90, 'Vagisan', 'VAGISAN lotion nettoyante intime 100 ml', '5746299', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUQExIVFRUVFxgXFRcXFRcVFRUaFxgYFhcYGBUYHSggGBslHRcXIT0hJSkrLi4vFyAzODMsNygtLisBCgoKDg0OGhAQGismHyUtLSsuLSstKy0tLS8tLS0rLS0tLS0tLSstLS4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAQsAvQMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAgMEBQYBB//EAEsQAAEDAgMFBAYHAwkGBwAAAAEAAhEDIQQSMQVBUWFxEyIygQYUkaGxwQcjQlJi0fAVguEkM0NyoqOy0/FTc5KTwtJEZIOzw+Lj/8QAGwEBAQADAQEBAAAAAAAAAAAAAAECAwUEBgf/xAA+EQACAQIDBAcGAwcEAwEAAAAAAQIDEQQhMQUSQVETMmFxgaHwIpGxwdHhBhRyFSMkM0Ji8VKiwtJDgpI0/9oADAMBAAIRAxEAPwD3FACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBAcc6ATwQFfUxrzplaOcuPyA96hRyjVdF3E+z5BAL7U8fghA7V3H4IA7Y8UuA7YpcHe2KXAdsUuCjpekVRzg3I25A37zC+YpbfqVKkYKCzaWr4s2unkPjbzrHK2Ic7f4QYb5k2XrW2G0pbqtaUteCdl4t5GO4TcBj31GB5aBOgvoujgsROvRVSUbX4dhi1Zk6k+RK9hBaAEAIAQAgBACAEAIAQAgEV/C7ofggKoqFJNDwoBaA4UICgOIDqATUNj0KksosGRwoIdPBrj7GmPfC+AwtOUKm/yUn5O3nY3t5El9Mk9mNXObT6BgGbyzGfJe2pTcmqMdZOMPCKW9/ud/Ai5mlYwABo0AgeS+xhBQiox0WRpJWH0WxAdQAgBACAEAIAQAgBACAEA3X8Luh+CAyj227+JPRhg6X0/JQz7kX2EaAwAaCw9gQxHUBwoQFACAEAl4seiks0wUFGjc8wB7XNXzdKjm79i80bGSdnUJqBx+y2fN8n/qPsXpwVBSxCqP+lX8ZXfzZG8i4XcMCRh9FUCg9MfS1uz+zzUXVO1zRBAAyZdTB+97lklc6Oz9nvGOVpJWt5meP0oWBGEPnVj/AONWx0v2BnbpPL7k2h6evdf1SP8A1QfksLnmnslR/rfu+5Bxf0mvZEYQE7x2pEf3d1klc9FPYUZK7qW8PuRm/Sw/U4H+/wD/AM1VE2L8PRf/AJf9v3Lb0b+kRuLxFPDerOY6pmuKgeBlaXGe6OHvSUbHkxuxnhqTq790uy3ZzZuVicQEAIAQAgEVvCeh+CAylOPsYYnd3rbp1M8VDPxLzAklgzCDvA3IYszvoz6SVa+JxGDrsY19EnKWBwDg12UmHE8WnzVaOrjsBTpUKdek21LnbJ2vwt2idtek9WnjqOBpU2ONQNLnOJluYmYA4Nbm80tlcYbZ8J4SeJqSateyXH/LyNSockEBWbX2yzDuoscyo416nZtyNkNNruvz62PBLHqw+ElXjOUWluq7v8izQ8pAGH1XPVGzZbj2DpwCeJW/DU91N82GSV6CEjD6KoHn30zM+qwzuFRw9rR+SzjqfQ/h5/vJrs+Z5+3wK8T6JdY0WA0WpnOralJtLxe34lbEe6j1Sud4VktTdHrGi+ipk7RYfu06h90fNJnN267YR96PblrPiwQAgBACARW8J6H4IDO4ehVDg59SRfuwIM/kVDJtFtQ08/yQhidrUvVts4euLNxTTTdzeBk/yllwO9h5dPsypT4wd13a/wDYh7Pa2rjtoY59XsW0posqmIY7+bzCbEw3Tf2icLG6s3TwmHw8Y7zl7TjzWtvPyEbP9IjTxdBtPGVsTQrEtf2tMtg7nMc4CddBwM6hS2RauAVTDTc6UYTjmt1+TV2S9iVsfjWPxzcV2IDndjRDA6mQy8P3kHSddTwCOyNOKjhMJOOHdPeyW9K9nny+PkQcB6SYp1DBPNYzWxZZUMNuwOYMulh3iq0eirgMOq1aKjlGF1rrZ5kzAbWrV8XVp1cccM9lbLTodm0NcwHTM7xOcPjIkWUsaauGp0cNCdOjvpxu5XeT8NEvsPel+0+yqVMu0zTqATToNpCoAQ2cr3NBPeN+9pO8KKK5GvZ2G6SEd7D3jxk5W46rNadmpDp+kmMruwLaL2sdiadQPBaCwOY57HVIibBpcBMaBVRSyNz2fhqMa7qJtQatnnZpO3nZstdhY7FUsc/A4isK7TS7Wm/IGOFxYgfvWk6BHax5MXRw9TCLE0YbrvZq9zb4fRRHGMJ9Mg/k1A8K/wAWP/JZx1O9+H3+/mv7fmjzlvgV4n0q6xotn+Famc+tqUe0tR5/ErYj3UuqV79D1KyRtjqan6IWzj3cqDz/AGqY+aTOVt9/wy/Uvgz2daz44EAIAQAgEVvCeh+CAx5FCSDWqHdqT5CyxNmfI0OCeCwEaHT2BUwKj0w2PUxNJnZForUqrKtMuMCWm4J4RfyCqPfs7FQoVH0nVkmnbtKPE+htV2zfVQ9ormqazzJyPcSQQTH3SPNoVTzPfDatOOO6Zp7lt1c0v8+THMXsfHYithK9ZlBjaD702uJhpLc7p00bZo0jW6lzCnisJQpVadNybmtWuOdl9yNs3ZuLwtKqzDYnDOwbs721nEvdSaR3iMvdJAHEib2khW5tr4jDYipGVanNVVZbqyTfDt+feV3o1sStXwmBewCKWKdVdJjuh7QY4+F3sR6npx2Lp0cTXjL+qCS77P6lv6R7GxuOik/D4emA+RiA/M9rJNmiM0xFtDG7UROx48FisLhLzjOTy6trK/bwE4XYeOoPxdKmyk5mJc4+sOd32h8yC3VzoJtpO+6NotTGYStGlObknBL2Usnbt4LzsI2F6L4mlUwDnhsYdtYVIcD43VC2OPiCXuXFbRoVIV1G/t7tsuSV/gXVXZNX9psxYANI0DTcZEtcCSJBuQZ1E75hOB4Y4mn+QdB9bev3o1WH0URzTEfTIP5HSP8A5hv/ALdVZw1O5+H/AP8ATL9L+KPNB4PYrxPqF1jR7P8ACtTOdW1KPafiHU/NbI6Huo9VlfV3rJG2JrvocZ/LaruFAj21KZ+STOP+IH/DxX93yZ7EtZ8iCAEAIAQCKvhPQoDMOFaZbRpjzHDrzUM8uZb4EuLO8ADJsOFoQxY+gBAJc0EEEAg2INwQdQQgTad0ZzEeguAe4u7EtBuWte9rD+6DbyhW504bYxcY23r9rSb95oMPQbTa2mxoa1ohrQIAA3BQ5s5ynJyk7tjiGIlxWEmDrVY6A6sgSMPoqgZD6W2TgJ+7Vpn/ABD5rKOp2dhO2K8GeVsP1Y8leJ9X/UaPZ3hC1M51fUo9p6jqVsjoe6j1WQMRosom6mbf6F2fX4l3BjB7XE/JSZwvxC/YprtZ6ysD5UEAIAQAgE1dD0KAyBbTmPWKkzYSdeHLgobM+Rf7PEU2w4uG4nUyAZQwepWYnH1G1zTbDs76bG5iQ1k0a1Uutr/N6b+IVBEO1qzs7jlDRSY4taTIf21Sk6HxpLN+7zQD37XrknLTpwfWcsvd/wCGqGmZhv2uWnPRAD/SINuWd1sueZ8LTTbUpGN5cXhvUO4IBbtr1Q7IKQe7K+zcw+tYwOLM7rOuSJGkcZiAcwu03VSWsyQxgdUdD2mS6owtaw3aWmk4HNoRG4oQjbH2lUyMbUbmPYUagLCXuOcEHNMSZbM783K+uWoLykUpvIC1sBJw+iqBlvpTZOzqp4Opn+8aPmrHU6uxX/GR8fgzyBrvqzyus1qfYpe2jS7LPdHRamc3EdYpNpm/ms4nuo9UgYjRZRNtM9B+hRl8W7/cgf3pPyUnqfPfiJ/y13/I9QWB8yCAEAIAQCamh6FAZYmpJnDsI3Xb0n5qGeXMvMJ4BaOXCwshgRmljqrwWNzU8js5iZc17QZi0NLhM6OKFK+vj8GAH9nmuxgAoPLstZ9nBuWXMLgTmEgkWkqgdGPw7RmeGNh1cd1rnhrRVc2q95DBkBcJcTAmbmJQDja2Fc80crS4ww/VnKTRl7WZy3KS27g2ZAkgRdQhJfs6iXF5pUy505iWNkyIMmOAA8kKH7Oo909lT7ng7je7Jm1rXv1QgllJrT3WtEANEACGtnK3oL2Xncs369aAdw51VovUC3PC3gkYeqMszaVQRtosp16bqL2B7HiCHRHIgEG4MEGLEBDZTqSpSU4uzRkGfRxghuq/80f9iy3mdb9vYns9z+pKoehGHbYPrx/vv/osXmap7YrS1jH3P6kXFfRzhHmS6v5VW/Omqm0bI7cxEVZKPuf1EM+jTBDfXPWq35U1d5mX7fxPKPuf1NP6PbKo4Kn2VJliZc4kOe4/iMCYnyWLObisVUxM9+b+ngXTKgOhQ8wpACAEAIBNTQ9CgMlDLxiXCZ1Ol76kKGzPkXuz3AsEODt0i8kAA+9EYMi4jZznPeRUaGVWtZUaaZLiAHA5Xh4ykhx+yUBHfsd7hLqwL2ikKbuzhoFKoKgzMzd4uIEwWi1gEAk7CIDw2qW9qKrahyAktq1alWG37rmmq8AmRe4VBJZspoLCHHuVnVgI+9TfSy9AHzPJQFggAlG7ZgiNP68nFeKLy9cmwFF2qzw7zYO0cMaknNAnReoEhuHDbSPehBWXmFQJMcUuLCmpcWFFhS4sIIQHBHFLiwoU98+d5QDzKhFiZ3aXQD6AEAIBNTQ9EBlqpdvwzTexEE+zioZ+Ja7Ic005a0sEnukQRv0RGL1F4ipUHgYHcy6N3COKAivrYmLU2yeY7t9/evaPZzsAupUrwIY3S5kCHchOiA4H4g5hkaLDKSQbyJBANxGYzbdYIDgq4iL02gwbyDBgQIm8GRPnyID9R7gw5gAbi2/dPKdYutVeW7TbA1/EexsfNeXTz8o2AnDnXossK/aYJ2zdD1XuIKq6qA40IUZfqgHKKAku0QhGc4cVN5cyjbTdE0wSG6Kg4dR1HxQExUgIAQCX6HogMyDXFwabxJ6xNuUqGeRdYfREYDON3HtOzgySdCOdxaSEKV+RogDEuu7uxJu8OjQ8nHgI3QgOVXM19ZduNiSTPQ6b9LRwkIBVcUnRNfTuiDreQLzmNhfkDvQCYpAD6948LQb3AcYsbEWdygdSQCphQeyAq1CLRJIkAjxDU+a8uJlnGHNlH89p/C4/8ToXm3rxv2N+92BzCG56LPBv2n3Bljs3wnquiYkhu/qoDj0KNIB+loqQUUBj6I7znRvtzM5/+n3r4SjFqrUm1xy7c3L/AI28Te9C62HSgOfxOUdG2nzMr6LY9HdhKpze6u6OXm7s1yZZv0XYMBk6jqPigJioBACA4/Q9EBjapog3FSmQdRaTc8zFlibMy/2YIpiHFwv3jvVRgxOOdxompHQ62t05xogIVWrDR/Jh3oEECCbgTYwbuPQzKAceYJb6sCBoQ20RMQW6zPLogOPqOgRhhrvi3G0eX8LkBbTaThhaIFpuQDq0QYJ9l4QEB2JqGpPZBrWtIN5ibWAA3krmVpp1JSv1Vb5fFlJdWwcOAY33ZitdR7sZLlur5sBgftdFtwLvKQZZ7N8J6rpmJF9IdqHC4epiBT7TIWy3NlMEhpIMHSZ8kSuerB4dYisqTdr8fAi7e2+KDsNTbT7V2JeGsGbLA7svJg2GYe9VK5twuC6aNSUpbqgrvK/hquRbFYngH6WioI+zce2vSFVrXtBkQ9pY6xI8J6Ibq9F0ZuEmm+x3RUmhBd1n2SuA8PuuT8fdcwuW+DpZWNbwH+q7OHpKlSjBcEYt5jztFuINO1HUICWgBACA4/QoDMOdWvGSo2dNDF7cPioZZFrs10s8GS5GXgNyIjEYxr5GWqGSYEgHcSbHXTkgIs1CW/yhsAw+wBMXiYvcgWjTnCA7Uabn1kAZgRpA1dlJBEgjidyAQLROLFh3tJdvJF7eWkckAVXhrXOOKBt+HXNmEAHy81hUnuRcuQIWCqsywaoOaoJJcLCJ1J3yT5HguRTi5JJrrS8lr5vyMh+piA5uYEHM9xtuiIHvWNaT3Lv+qTfyCJGztHHp816dnaSfd8yMstm+E9SuoYnNpYMVqNWidKjHM6ZmwCiNtCq6VWNRcGn7jzmhtBjhSxNbPkwWCDHZCRUFaq52HgEGzoab7jdZH0k6EouVKna9SpdX03UlL3Z+Og1tHamKwbsQxofTnDNqNY+ucSaZNZtLOHHwmHG1xMGSqkmZUcNh8VGnJtP22rqO5f2W7W4q61yyLyth3YbEYBlPF13trPJqB9UvFTKyQ8cjm00NuCi0Z4oTjXo15SpxTisrK1rvT766lbga2LqYLDYl7sRWotFc1xSrFlaQ92VxdIL2gDwg7lZJXaPXWhh4YqrSioxk93dvG8dFdck3zF7WxzMRTf6qcS80MO1zqnrLqTacsL2ueJmpUgSbXiJWtQSefHsNOHoSoTXTqC3ptW3FJvOzS5R5fAVswVcY6r2mIrNAwlCrFOoWDtDTJzW0vJgQCSJ0CyeRa3R4WMdyEX7clmr5X0++qNl6N4l1XB4eo8y51Jhcd5MXJ5lQ4uNpxp4ipGOibJ7tR1CHlJSAEAIDjtEBj3ilJnPSdJ0zQbkz5wTuUNmZe7LBDLvz3Petf2IjBjmIw7X2cJA5kfAoBgbNpfcHDUn4lAL9SpxGQRbjumP8R9qAPU6d+6L9Z/Vh7EBWbWo0xFNrG3OY26gfFy5W0q7VqcddfoWIk0GNzDK2KbA3QHvG3uk+xanJwcs+pG3/ALPXzv7iiMQ0NysAAytEgcT3j8V58S7OMOSXveb+JUTdnDuE8T8guls1fum+0xkWOzfCepXRMSQ3f1UBWv2FhslWn2LcldxdVF++43nWxm9og6K3PT+cr70Z7zvHJdhW1fRylSa52HoUzVLOzPavqOa9hILmvJJJsIBMxA3CEueiOPqVJJVpvdvf2Urp8GtPH6lZsX0Zf6zRq+qU8JToFziBW7d9VxENAd9lgkmOZte2Vz24jHx6CcOlc5SsurupLu4t+mXtT0SwnZtpBjwxgc0NFasAQ4lzgYf3gSTqpd6ngW08Tvud1d533Y8MlwyGa/oxg3OzGiB3WsIDnNY5rRDQ5gMOgRqNw4LByaJHaOJjHdU+N+Dab1s9Vcn7N2PQokmnTyksZTPec6WMENbDidAsjTVxVWqrTd82+Gr10JeFwrKVNtKmIYwBrRJMAaCTdDXVqSqTc5avUU/UdQoYEpUgIAQHHaIDL1O1v4KrJPdtIvEeUeZUM8iz2SRkMMLO8ZB48eiIxZLQHEBxCHHuABJ0FypKSinJ6IFDRq56jqp0b3vZZo9sL5ynV6WtKvLSOf8A1XwNlsrDlNshjDrUdnd0H6cVshFyjTpvWb3n3ershFxFXM5zuJP8F5K1XpKkpc2VFxgmxSbzk+0r6DAxtQj7zF6kzZvhPUr2GInaVRzaZcycwezS8jOyRbdE+SAqa+0a4cQ7LbJYMc3OS6kSQS4mA2pBF9CbTAFOftaq2XPYzK0w6M0gdkyu51+DXObG8tBtMAAqekTmU3PdTDSzVpNy4NdULJJABydmd579gYQEtu2Zqdkcje+9uYu7vcLQ1ocDBqODpixGV0iyEIex9oVHsYHtzONKi8uBH9I0yXSReWHTjotMwX9NbSi3aKkG36t6hQpKVICAEBwoDJVHU8xaS+i6SZ0a6+vTTgoZ5l3s2cgl2Y3uN97e5EYsfQAgOIQqdu4uAKY1N3dNw/XBcTa+L3UqK1eb7vv61M4riRBTOVlEeKoQ53IfZHskrxKm9ynh11pvefYuHldl7R01RNWoNGjs2edreUnzW51U3VrLRLcj8Phn4ktwK4FcxPkZGnyZWhvAAewL7KnDcgo8kkax3ZvhPUrYQMbXyMzSB9ZTBJ0h1RjXa8iUBXVtsuLg1rGwXubJefCwva4kBtiS2wkyJ0Qo0zbndY7ITnIa3vAEuz06bpb9kZqgM3sN0iQLfZuJ7RmaIu5pEzBY4sdfeJaboQkvNkBWvdv/AFxXilK6uUmUdB0Xrj1UBx2ipBt2reoQpKVICAEBwoDLP7S9m1mSbWDm3gjnE9eihnkWmyqbRT7rS0HvQdQSAhiyUgBAM4quKbS86D38AtOIrxoUnUnovVgldmaw31tQvf4R339BoPgF8bhm8VXlVrdVe1LuXDx0tyNryVkPMrnLUxB8TjkZynUjo2y9MMTLo6mMlrL2Y+OvuWSZLcBOMORlOlvjO7q7QeQUxUuhpU6HG28+96e5BZu4jZzM1Rg5z7Ln4LHArpMRCPbf3Z/IPQ0tZfZGsXs3wnqVSD5iDMRMX0vAGvNQpCfiMObZqRzEaZTJbdum8RbogEMdQJLh2ckhpMNBJbBaOJjukcLICThsTTuA5oiSQIESXEkjnDj7SqQVXxbMriHttzHBYVJbsGwV3agiQQRO6+kj5LwTdlb1xXyKWeH8I6Be+n1F3AcdosyDbtW9VCkpUgIAQAUBkxQDnuAD6TxNx4XCTfnrPVQzuXGzg8MOcgmdRw3eaGLJCAEBmds43tHZG3a3T8R4/JfF7Yx/5mr0VPOK836yX3NsI2VzlemWhuGb43EF/U+FvQapWpypQhgafXk05d/BeGr94WftDpY19VtIfzdEGTuMXeT1NlucKdbFRoL+XSWfhnJ+LyGivzK7FVy97nnefduHsXIxWJlWrSq83/jyMkrIs/RylLnP4CB1P+nvXb/D9NzqTqPgre//AB5mFQu6y+qNYvZvhPUoQkDf1QpT1K9ODOGfAb9yONt3H3nRUCPWKIH804CXAd2N0OgE6QB+pUAsPosGcUXE6RFxmaSLTYO06uvoYoI+Mq0Ib9URGZx1kBpnSZvOv5GPNiJWtHx8EERvXqbRIaZJgwJPdAAJPD+PNeGe9ZL1ovncyNLh/COgXUp9RdyMRx2izIIdq3qoUkqkBACAEBmX4p1Oc4Jbue0TGtiNyhla+haYOoHNkEEHeEIPICo25j8o7Np7x8R4Dh1K4G2to9FHoKb9p69i+r+HgZwjfMrcCwMaa7hpamPvO49AuLgacaFN4yotMoLnLn3L1oZyzyFYdxYx1dx775azjJ8Tvks8PJ0KMsZPryuo/OXr5kebscP1dCPtVfcwfmo/4TA2/rq+UV9fg+wusu4r1xjM1GxKGWkDvd3vbp7gF93sah0WEi3rLP36eVjRN3ZLqrqmIvZvhPUqkJA3qFK+q3E/epiztzjBghuuonKd29UDc4i9qfK5JPMmw56KAXRfiQPCwnrrz/h/qqCDi69dxcQ1lzlF9ADczPAExz3rkznGrUbvl1fm/XaZaDbajz4wA7W2l7j4rx1Z3frjn8yo0FIL6FKysYC3KkG3at6qFJSpAQAgBAZMDvO7N3ek5qb9DrMcAZ96hn3lpsp8sPcyQ4gt5624i6EYbTxwpN4uPhHzPJc7aW0I4SnfWT0Xz7l5ljG7M7hKBqvJcbeJ7juG/wA18hhMPLGVnKby1lLkvXrI2t2Q+fr6gaO7TaLfhaNSeZXqk/2hiVThlTisuyK1fezHqo6Ir1QBamwexjfmfnyWSS2hi1FZUoL3RX19aDqrtIuPxPaPLt2jRwA0/XNeDH4r8zXc1pou5afUyirIRhqJe5rBvMfmVpw1B160aa4v/JW7I2LQAIGgsF+kJKKstDzCKqpRezfCepVIO1IgyYEjfF5ECeZgRvmN6hSo7KiQS2u5txq/Q3MEHzt+HkVQIDKYAPrDhAEy6MwJLwTeYh+vLkQoBOLc1rY9Zdm8Pj9s31/Ib5J82LrdFTutXoVK5HFMAZe3k5co7wN3AkkAHc07uAXMzS3bcl4y19yyZkR6OHFSo09q50PBs62tgQDwla41N6sko6vy/wADga1i+gMBTtEINu8TeqFJSpAQAgBAZSq5jnZKjcjp7rtx4EO9llDPuLF2KbSp5jc2ji4x+rryY3GQwtJzn4LmyJXZniX1qnFzvYPyAXw7dfH4jnJ+5L5JG7KKH8bUDR6vTuJ75H23cOgXsxtWNKCwVDP/AFP/AFS5dy9dsSvmzuKPYs7IeN0GoeHBv6+auKawVD8tHryzm+XKP1+5Fm7na/1NIU/t1Lv5N3N/XNZV/wCBwio/11M5di4L12he07lauGbC49HcPLjUO6w6nX3fFfSfh7Db05VnwyXe9fL4mqo+Bfr6w1DdVCi9m+E9VSEgb1ClNUfIAdhTECQGQZtodwgDfvA3FUHTVbDpw7hAE90XAcGwIO4XHIKApqmIpOqE9g7IyYECJ1vfeeE2HRcGriOlrOopLcj58ve/IztZC/WKcE9k7NBLjlHjq903ngfceak3OCzlmk//AKl9Fn3oBsiuO0EU3QGzYX1yxHIunoVMDByrpt3td+QloabBV84nK5t47wgnnHBfQGBIdohBs+JvVCkpUgIAQAgMpWm/9LTzEOb9phn7PGCVDMaxlD6um1uZ2ZxLQdQCBAXz236cqkaUYq7bfyMovNnazhh2mm0zVcO+4fZH3R+vkufWlHZ1J0YO9WS9p/6VyXrt5FXtO/ARg2Ckzt3C+lNvE/e6D9blrwdOOEpfm6qz0gub593rkHm7I5s+nJdXqXay5/E7cP1yU2fTU5SxlfOMc++XBeuwS5Ih4isXuLzqT+guZia8q9WVSer9WM0rKw2FpSbdkU12Aw/Z02s36nqdf1yX6JgcN+XoRp8ePe9Tzyd2SF6zEbqoUXszwnqqQkN+ahRL0BUbaxmUdm3xO15D8yuDtvH9FDoKfWl5L6vT0jOEeJCptDe6dKffqc3aBvwHm5cqCVN9HLSHtT7ZaKPh1fGRkcxNUwAdT33dXae6/wC8pXqtRSlq/afe9PLPxCJewmeJ3QD4n5Lq7ChdTqeHzfyMZl0xd8wFO0VINnxN6/JQEpUAgBACAyddwzmfq3ycjtGvG7NuO72KGfAfrYx1OmHOaO0ktB1A4uHUQuVtXGLC01NK8ndLs5sRjdldgMNnJqVD3G3ceJ4dSvmcBhFXlKvXfsRzk+b5ePH6s2SdskcqPdiKoAEDRo3NaEqTq7SxSjHJaJcIr19OQyihW064tRZ4Gf2nbz+uaz2piIK2Fo9SHm+L9cb9giuLIC45mWOw8LnqZjoy/nu/PyXa2HhOmr9I9I5+PD6+Brm7I0y+1NIIBuqqUVszQ9UISW/NCkXaGKFNuY66AcSvJjsZDC0XUl4Lm/WpUrsztN5E13XcT3ObvvdB8YXx1OclfG1c5N+wucufdHh22XA29iHBTAimdG/WVT8Gz5x1ct3RqNqE+Ht1H28I+dv1MnaRKtYuJcdSZXMqYh1JOctW7maRo9m0ctNo3kSepuvu9mUOhwsIvW133vP7GiTuycxe4gp2ipBr7TevyKAloAQAgBAUVak1wyuAI5/qyhRjFYIvDGCwBueAAXJ2rg5Yro4LS7bfJGUZWK7aOKBilT/m26fiPHmvnNo4uM7YbD/y46W4vn67zZFcWPP/AJPTy/0rxf8AA3h1/W5eqdtm4bcX82az/tXrz7kTrPsKpfPGw6qld2QNXszC9nTDd5u7qfy0X6Bs7CflqChx1ff9tDzyd2S17jEEA3VVKK2ZoeqEH6lQNaXOMAXKwq1Y0oOc3ZLUqMzWqmu8vccrG6/hHD+sf1oviqtV7RrupUe7Tj5L/tL1obkt1HBV/piIDe7SbukaeQ16qqte+LmrKPs048LrTwjq3xZLcBrEHK3J9p3efx/CPfPnyXmxTdGkqT68van8l832vsMlm7nNnYftKjW7pk9Bc/l5rXs7DfmMTGHDV9y1+gk7I1JX6IecWxCi3aKkGh4m9fkUBLQAgBACApVCkPa9ZzaeUGMxg9INlw9v1508OoxfWdn3GcFmRcBRFNvrDx/u28Tx/XXguTgKEMLS/OV1+hc3z+nv5GUnd2RXV6pe4ucZJ1XHrVp1qjqT1ZmlYbWopabCwmd+c+FnvO72a+xdzYeC6Wt0sl7Mfjw92vuNc5WVjSL7I0ggBANVlSi9maHqhCs20XvqNot0jNy3iSeAAXzO2nWr14YWno1fzeb7Fb1kbYWSuQyA89kwxTZdzuPFx+AC527GvJYak7UoZylz5yfwivSy0zepwPBPaERTp2Y3idw+ZVVWFRvESVqdPKEeb4L/AJS+ZLcCC95JJJkm5XGqVJVJOcndvNmZoPRzCw01Tq6w6DX3/BfW/h7CblN13rLJdy+r+BqqPOxYlfRGsWxCinaKkG2+JvU/AqAlqgEAIAQFIdVCjdfDtcA55hjDmdztYLn7Qw9Oqoyqu0Iu77ez13Fi7aFJtDGGq6dGizRwH5r47aGOli6u9pFZJcl9TdGNiKvCZCqVMuIaBJJgLZSpSqzUIrN5Ebsa7B4cU2Bg3anid5X6FhMNHD0o048PN8Wedu7H4XoIcJQCX1Wt1cBuuQOe9ANV6zQCS5sBuY3Fm37x4CxvyKpR3ZLgWkjSSqQrts1Dn7Jg77wATvLZMNHATJK+a2zVk6qoUV7c0k32XeXxb7NcjZBZXZCeySMPTNpmo7cSNT/VC5kqe81gcO8r3nLg2tX+mPDt7TL+5kfF1gSGt8DbN58XHmV4sbiIzap0upHJdvOT7WZRXMRhaBqPDBvPs4lacLh5YitGlHj6bK3ZG0p0w1oaNAIHkv0inTjTgoR0SseYYcsgLYhRT9EA23xN6n4FAS1SAgBACAo3alQpB2y49mBuLr+wr5/8RSaoRSer+RnT1KVfHm4FQXuwMFA7Ui5s3pvPn+tV9ZsLA7kfzE1m9O7n4/DvNM5cCw2jTLmQBPfpkgb2iowu1/CDbevozWVOPwZIdGRrS8kMc5oAb2WQjUhmYzpMTmiZCFHcfVovLc1ajlcHMqNdUYPq3BpI17xlgbrpUd5gIwuJYCyoa9F7w1zXxUFyezAcIB3UwCI38rgMvaAyowOkVKWSRTquymazrBrDLR2oAuLNQF9sV4LCRmiT4g8O/t3QELbNYMd3Z7R7Q3+q2TpzJMeS+c2zXVGp+7/mSSj3K707Xp4GcFcr8R9U3sh43fzh4cGD5rj4lrB0fy0evLrvl/avmZrN3IK5BmXmxKQpgVHTL7NAaXGAC7QDfB9g4r7LYGB6Kn08lnLTu+/wsaZvgWwx9MgkOnK3MYBIiA6QY71nN0nVfQmsYdi2TEyZLYAJkjKYHHxD38CoUUzGs47w3hckiLxvaR5Kgep1g9geNDcafJQAzxN8/gUBLVICAEAxXxIagKhlQOuP1yUKQts+Bv8AW+RXz34i/kQ/V8jOnqZ55qTaInfGn+seU74Xzceg3VfXx9aeduFzZmTtkYN9WpDoyAS6J5W+K92z8DSxVTdjeyd23yzy72SUmjXARYL7ZJJWRpG8Vh2VGmm9oc06g6cR0IN53KkIrNn5PAKbhwfTAd0ztGnVpPNCjzX1R/R0/Kof8sIDvaVvuM/5rv8ALQDdWnUPiqAD8DYPTM4mfIBASdj0w1pAnU6kk+ZNygIe13Cm7ttXZctMa3uS4jlI8yuBtVww9ZYl9a27Fdt3eT7r+/3mcc1Yy/repc1877TO+ea+Wlh3KV99O/G5suWWzcJ2j4+yLu/LzXo2XgXi61n1Vm/p4/USlZGgxVJxylgEtOlwYILe7BAm+8jrZfoCSSsjQMUqbWEuBpMJABDmOZOUANs50GAIBGl+KpCM2kxsRVoiA0A94xlmDPa63MnfoZUKP0GEGWOY8zPdpvLZkmS41csySeN96oLGiwtY1rokW7oho4AA8BA8lADPE3z+BQExUgIAQEbE0ZCAoMRQdTdmb5jcf480AztOqHU2kfeuN4sbFfPfiL+RD9XyZsp6lZTplxDQJJsF8nRpSqzUIK7ZtbsanB4dtJgbI4k6SV+g4LCRwtFU4+L5s0N3Y47EsGr2j94fmvWYjZx9L/aM/wCIFAJO0aX3x5An4BAJO1KXE/8AA/8AJAcO1GcHn9380A3U2mDpTqexv/chSfsd8tJgi5sf4IQTj8AyoZdMi0grwY3ZtDFtOpe64r0zJSaILths3Pd7iuZL8OUOE5eX2MukZLwdAU25RfeTxXXwWDhhKXRw8XzZi3dkum9esg92ipCLUqwdFCnWV+XvQDmeUBxnjb5/AoCYqQEAIDhCAj18OCgKvEbJa7ULCVOM+sk+8DA2I37o9isYRjokBbdjN+6PYsgOt2S3gEA4NmDggFjZw4IBQ2eOCAWMAOCARVwdrICtLq1IksIIOrXaeR3IDo224eOi790gj3woUUNvU97Xj90/IIBP7co8SP3SgFs25Q+/7kFhz9vYf/aBBYYqbZoHR6Cxxu2aPEno1x+SAV+3G/Zpvd5R/ihALo4qq9wOUMA3ak9ShC7oOJF1QOoAQAgBAcyoAyhAGVAEIAhAdhACAEBwhAM1KAKAjvwI4IBl2zhwQCDswcEAk7LHBAH7LHBAA2WOCAWNmjggHWbPHBASaWGAQEhoQHUAIAQAgBACAEAIAQAgBACAEAIAQAgOQgCEAQgCEAQgOoAQAgBAf//Z');
INSERT INTO `t_produits` (`id_produits`, `MarqueProduit`, `DescriptionProduit`, `Pharmacode`, `ImageProduit`) VALUES
(91, 'Vagisan', 'VAGISAN lotion nettoyante intime 200 ml', '4445561', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUQExIVFRUVFxgXFRcXFRcVFRUaFxgYFhcYGBUYHSggGBslHRcXIT0hJSkrLi4vFyAzODMsNygtLisBCgoKDg0OGhAQGismHyUtLSsuLSstKy0tLS8tLS0rLS0tLS0tLSstLS4tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAQsAvQMBEQACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAgMEBQYBB//EAEsQAAEDAgMFBAYHAwkGBwAAAAEAAhEDIQQSMQVBUWFxEyIygQYUkaGxwQcjQlJi0fAVguEkM0NyoqOy0/FTc5KTwtJEZIOzw+Lj/8QAGwEBAQADAQEBAAAAAAAAAAAAAAECAwUEBgf/xAA+EQACAQIDBAcGAwcEAwEAAAAAAQIDEQQhMQUSQVETMmFxgaHwIpGxwdHhBhRyFSMkM0Ji8VKiwtJDgpI0/9oADAMBAAIRAxEAPwD3FACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBAcc6ATwQFfUxrzplaOcuPyA96hRyjVdF3E+z5BAL7U8fghA7V3H4IA7Y8UuA7YpcHe2KXAdsUuCjpekVRzg3I25A37zC+YpbfqVKkYKCzaWr4s2unkPjbzrHK2Ic7f4QYb5k2XrW2G0pbqtaUteCdl4t5GO4TcBj31GB5aBOgvoujgsROvRVSUbX4dhi1Zk6k+RK9hBaAEAIAQAgBACAEAIAQAgEV/C7ofggKoqFJNDwoBaA4UICgOIDqATUNj0KksosGRwoIdPBrj7GmPfC+AwtOUKm/yUn5O3nY3t5El9Mk9mNXObT6BgGbyzGfJe2pTcmqMdZOMPCKW9/ud/Ai5mlYwABo0AgeS+xhBQiox0WRpJWH0WxAdQAgBACAEAIAQAgBACAEA3X8Luh+CAyj227+JPRhg6X0/JQz7kX2EaAwAaCw9gQxHUBwoQFACAEAl4seiks0wUFGjc8wB7XNXzdKjm79i80bGSdnUJqBx+y2fN8n/qPsXpwVBSxCqP+lX8ZXfzZG8i4XcMCRh9FUCg9MfS1uz+zzUXVO1zRBAAyZdTB+97lklc6Oz9nvGOVpJWt5meP0oWBGEPnVj/AONWx0v2BnbpPL7k2h6evdf1SP8A1QfksLnmnslR/rfu+5Bxf0mvZEYQE7x2pEf3d1klc9FPYUZK7qW8PuRm/Sw/U4H+/wD/AM1VE2L8PRf/AJf9v3Lb0b+kRuLxFPDerOY6pmuKgeBlaXGe6OHvSUbHkxuxnhqTq790uy3ZzZuVicQEAIAQAgEVvCeh+CAylOPsYYnd3rbp1M8VDPxLzAklgzCDvA3IYszvoz6SVa+JxGDrsY19EnKWBwDg12UmHE8WnzVaOrjsBTpUKdek21LnbJ2vwt2idtek9WnjqOBpU2ONQNLnOJluYmYA4Nbm80tlcYbZ8J4SeJqSateyXH/LyNSockEBWbX2yzDuoscyo416nZtyNkNNruvz62PBLHqw+ElXjOUWluq7v8izQ8pAGH1XPVGzZbj2DpwCeJW/DU91N82GSV6CEjD6KoHn30zM+qwzuFRw9rR+SzjqfQ/h5/vJrs+Z5+3wK8T6JdY0WA0WpnOralJtLxe34lbEe6j1Sud4VktTdHrGi+ipk7RYfu06h90fNJnN267YR96PblrPiwQAgBACARW8J6H4IDO4ehVDg59SRfuwIM/kVDJtFtQ08/yQhidrUvVts4euLNxTTTdzeBk/yllwO9h5dPsypT4wd13a/wDYh7Pa2rjtoY59XsW0posqmIY7+bzCbEw3Tf2icLG6s3TwmHw8Y7zl7TjzWtvPyEbP9IjTxdBtPGVsTQrEtf2tMtg7nMc4CddBwM6hS2RauAVTDTc6UYTjmt1+TV2S9iVsfjWPxzcV2IDndjRDA6mQy8P3kHSddTwCOyNOKjhMJOOHdPeyW9K9nny+PkQcB6SYp1DBPNYzWxZZUMNuwOYMulh3iq0eirgMOq1aKjlGF1rrZ5kzAbWrV8XVp1cccM9lbLTodm0NcwHTM7xOcPjIkWUsaauGp0cNCdOjvpxu5XeT8NEvsPel+0+yqVMu0zTqATToNpCoAQ2cr3NBPeN+9pO8KKK5GvZ2G6SEd7D3jxk5W46rNadmpDp+kmMruwLaL2sdiadQPBaCwOY57HVIibBpcBMaBVRSyNz2fhqMa7qJtQatnnZpO3nZstdhY7FUsc/A4isK7TS7Wm/IGOFxYgfvWk6BHax5MXRw9TCLE0YbrvZq9zb4fRRHGMJ9Mg/k1A8K/wAWP/JZx1O9+H3+/mv7fmjzlvgV4n0q6xotn+Famc+tqUe0tR5/ErYj3UuqV79D1KyRtjqan6IWzj3cqDz/AGqY+aTOVt9/wy/Uvgz2daz44EAIAQAgEVvCeh+CAx5FCSDWqHdqT5CyxNmfI0OCeCwEaHT2BUwKj0w2PUxNJnZForUqrKtMuMCWm4J4RfyCqPfs7FQoVH0nVkmnbtKPE+htV2zfVQ9ormqazzJyPcSQQTH3SPNoVTzPfDatOOO6Zp7lt1c0v8+THMXsfHYithK9ZlBjaD702uJhpLc7p00bZo0jW6lzCnisJQpVadNybmtWuOdl9yNs3ZuLwtKqzDYnDOwbs721nEvdSaR3iMvdJAHEib2khW5tr4jDYipGVanNVVZbqyTfDt+feV3o1sStXwmBewCKWKdVdJjuh7QY4+F3sR6npx2Lp0cTXjL+qCS77P6lv6R7GxuOik/D4emA+RiA/M9rJNmiM0xFtDG7UROx48FisLhLzjOTy6trK/bwE4XYeOoPxdKmyk5mJc4+sOd32h8yC3VzoJtpO+6NotTGYStGlObknBL2Usnbt4LzsI2F6L4mlUwDnhsYdtYVIcD43VC2OPiCXuXFbRoVIV1G/t7tsuSV/gXVXZNX9psxYANI0DTcZEtcCSJBuQZ1E75hOB4Y4mn+QdB9bev3o1WH0URzTEfTIP5HSP8A5hv/ALdVZw1O5+H/AP8ATL9L+KPNB4PYrxPqF1jR7P8ACtTOdW1KPafiHU/NbI6Huo9VlfV3rJG2JrvocZ/LaruFAj21KZ+STOP+IH/DxX93yZ7EtZ8iCAEAIAQCKvhPQoDMOFaZbRpjzHDrzUM8uZb4EuLO8ADJsOFoQxY+gBAJc0EEEAg2INwQdQQgTad0ZzEeguAe4u7EtBuWte9rD+6DbyhW504bYxcY23r9rSb95oMPQbTa2mxoa1ohrQIAA3BQ5s5ynJyk7tjiGIlxWEmDrVY6A6sgSMPoqgZD6W2TgJ+7Vpn/ABD5rKOp2dhO2K8GeVsP1Y8leJ9X/UaPZ3hC1M51fUo9p6jqVsjoe6j1WQMRosom6mbf6F2fX4l3BjB7XE/JSZwvxC/YprtZ6ysD5UEAIAQAgE1dD0KAyBbTmPWKkzYSdeHLgobM+Rf7PEU2w4uG4nUyAZQwepWYnH1G1zTbDs76bG5iQ1k0a1Uutr/N6b+IVBEO1qzs7jlDRSY4taTIf21Sk6HxpLN+7zQD37XrknLTpwfWcsvd/wCGqGmZhv2uWnPRAD/SINuWd1sueZ8LTTbUpGN5cXhvUO4IBbtr1Q7IKQe7K+zcw+tYwOLM7rOuSJGkcZiAcwu03VSWsyQxgdUdD2mS6owtaw3aWmk4HNoRG4oQjbH2lUyMbUbmPYUagLCXuOcEHNMSZbM783K+uWoLykUpvIC1sBJw+iqBlvpTZOzqp4Opn+8aPmrHU6uxX/GR8fgzyBrvqzyus1qfYpe2jS7LPdHRamc3EdYpNpm/ms4nuo9UgYjRZRNtM9B+hRl8W7/cgf3pPyUnqfPfiJ/y13/I9QWB8yCAEAIAQCamh6FAZYmpJnDsI3Xb0n5qGeXMvMJ4BaOXCwshgRmljqrwWNzU8js5iZc17QZi0NLhM6OKFK+vj8GAH9nmuxgAoPLstZ9nBuWXMLgTmEgkWkqgdGPw7RmeGNh1cd1rnhrRVc2q95DBkBcJcTAmbmJQDja2Fc80crS4ww/VnKTRl7WZy3KS27g2ZAkgRdQhJfs6iXF5pUy505iWNkyIMmOAA8kKH7Oo909lT7ng7je7Jm1rXv1QgllJrT3WtEANEACGtnK3oL2Xncs369aAdw51VovUC3PC3gkYeqMszaVQRtosp16bqL2B7HiCHRHIgEG4MEGLEBDZTqSpSU4uzRkGfRxghuq/80f9iy3mdb9vYns9z+pKoehGHbYPrx/vv/osXmap7YrS1jH3P6kXFfRzhHmS6v5VW/Omqm0bI7cxEVZKPuf1EM+jTBDfXPWq35U1d5mX7fxPKPuf1NP6PbKo4Kn2VJliZc4kOe4/iMCYnyWLObisVUxM9+b+ngXTKgOhQ8wpACAEAIBNTQ9CgMlDLxiXCZ1Ol76kKGzPkXuz3AsEODt0i8kAA+9EYMi4jZznPeRUaGVWtZUaaZLiAHA5Xh4ykhx+yUBHfsd7hLqwL2ikKbuzhoFKoKgzMzd4uIEwWi1gEAk7CIDw2qW9qKrahyAktq1alWG37rmmq8AmRe4VBJZspoLCHHuVnVgI+9TfSy9AHzPJQFggAlG7ZgiNP68nFeKLy9cmwFF2qzw7zYO0cMaknNAnReoEhuHDbSPehBWXmFQJMcUuLCmpcWFFhS4sIIQHBHFLiwoU98+d5QDzKhFiZ3aXQD6AEAIBNTQ9EBlqpdvwzTexEE+zioZ+Ja7Ic005a0sEnukQRv0RGL1F4ipUHgYHcy6N3COKAivrYmLU2yeY7t9/evaPZzsAupUrwIY3S5kCHchOiA4H4g5hkaLDKSQbyJBANxGYzbdYIDgq4iL02gwbyDBgQIm8GRPnyID9R7gw5gAbi2/dPKdYutVeW7TbA1/EexsfNeXTz8o2AnDnXossK/aYJ2zdD1XuIKq6qA40IUZfqgHKKAku0QhGc4cVN5cyjbTdE0wSG6Kg4dR1HxQExUgIAQCX6HogMyDXFwabxJ6xNuUqGeRdYfREYDON3HtOzgySdCOdxaSEKV+RogDEuu7uxJu8OjQ8nHgI3QgOVXM19ZduNiSTPQ6b9LRwkIBVcUnRNfTuiDreQLzmNhfkDvQCYpAD6948LQb3AcYsbEWdygdSQCphQeyAq1CLRJIkAjxDU+a8uJlnGHNlH89p/C4/8ToXm3rxv2N+92BzCG56LPBv2n3Bljs3wnquiYkhu/qoDj0KNIB+loqQUUBj6I7znRvtzM5/+n3r4SjFqrUm1xy7c3L/AI28Te9C62HSgOfxOUdG2nzMr6LY9HdhKpze6u6OXm7s1yZZv0XYMBk6jqPigJioBACA4/Q9EBjapog3FSmQdRaTc8zFlibMy/2YIpiHFwv3jvVRgxOOdxompHQ62t05xogIVWrDR/Jh3oEECCbgTYwbuPQzKAceYJb6sCBoQ20RMQW6zPLogOPqOgRhhrvi3G0eX8LkBbTaThhaIFpuQDq0QYJ9l4QEB2JqGpPZBrWtIN5ibWAA3krmVpp1JSv1Vb5fFlJdWwcOAY33ZitdR7sZLlur5sBgftdFtwLvKQZZ7N8J6rpmJF9IdqHC4epiBT7TIWy3NlMEhpIMHSZ8kSuerB4dYisqTdr8fAi7e2+KDsNTbT7V2JeGsGbLA7svJg2GYe9VK5twuC6aNSUpbqgrvK/hquRbFYngH6WioI+zce2vSFVrXtBkQ9pY6xI8J6Ibq9F0ZuEmm+x3RUmhBd1n2SuA8PuuT8fdcwuW+DpZWNbwH+q7OHpKlSjBcEYt5jztFuINO1HUICWgBACA4/QoDMOdWvGSo2dNDF7cPioZZFrs10s8GS5GXgNyIjEYxr5GWqGSYEgHcSbHXTkgIs1CW/yhsAw+wBMXiYvcgWjTnCA7Uabn1kAZgRpA1dlJBEgjidyAQLROLFh3tJdvJF7eWkckAVXhrXOOKBt+HXNmEAHy81hUnuRcuQIWCqsywaoOaoJJcLCJ1J3yT5HguRTi5JJrrS8lr5vyMh+piA5uYEHM9xtuiIHvWNaT3Lv+qTfyCJGztHHp816dnaSfd8yMstm+E9SuoYnNpYMVqNWidKjHM6ZmwCiNtCq6VWNRcGn7jzmhtBjhSxNbPkwWCDHZCRUFaq52HgEGzoab7jdZH0k6EouVKna9SpdX03UlL3Z+Og1tHamKwbsQxofTnDNqNY+ucSaZNZtLOHHwmHG1xMGSqkmZUcNh8VGnJtP22rqO5f2W7W4q61yyLyth3YbEYBlPF13trPJqB9UvFTKyQ8cjm00NuCi0Z4oTjXo15SpxTisrK1rvT766lbga2LqYLDYl7sRWotFc1xSrFlaQ92VxdIL2gDwg7lZJXaPXWhh4YqrSioxk93dvG8dFdck3zF7WxzMRTf6qcS80MO1zqnrLqTacsL2ueJmpUgSbXiJWtQSefHsNOHoSoTXTqC3ptW3FJvOzS5R5fAVswVcY6r2mIrNAwlCrFOoWDtDTJzW0vJgQCSJ0CyeRa3R4WMdyEX7clmr5X0++qNl6N4l1XB4eo8y51Jhcd5MXJ5lQ4uNpxp4ipGOibJ7tR1CHlJSAEAIDjtEBj3ilJnPSdJ0zQbkz5wTuUNmZe7LBDLvz3Petf2IjBjmIw7X2cJA5kfAoBgbNpfcHDUn4lAL9SpxGQRbjumP8R9qAPU6d+6L9Z/Vh7EBWbWo0xFNrG3OY26gfFy5W0q7VqcddfoWIk0GNzDK2KbA3QHvG3uk+xanJwcs+pG3/ALPXzv7iiMQ0NysAAytEgcT3j8V58S7OMOSXveb+JUTdnDuE8T8guls1fum+0xkWOzfCepXRMSQ3f1UBWv2FhslWn2LcldxdVF++43nWxm9og6K3PT+cr70Z7zvHJdhW1fRylSa52HoUzVLOzPavqOa9hILmvJJJsIBMxA3CEueiOPqVJJVpvdvf2Urp8GtPH6lZsX0Zf6zRq+qU8JToFziBW7d9VxENAd9lgkmOZte2Vz24jHx6CcOlc5SsurupLu4t+mXtT0SwnZtpBjwxgc0NFasAQ4lzgYf3gSTqpd6ngW08Tvud1d533Y8MlwyGa/oxg3OzGiB3WsIDnNY5rRDQ5gMOgRqNw4LByaJHaOJjHdU+N+Dab1s9Vcn7N2PQokmnTyksZTPec6WMENbDidAsjTVxVWqrTd82+Gr10JeFwrKVNtKmIYwBrRJMAaCTdDXVqSqTc5avUU/UdQoYEpUgIAQHHaIDL1O1v4KrJPdtIvEeUeZUM8iz2SRkMMLO8ZB48eiIxZLQHEBxCHHuABJ0FypKSinJ6IFDRq56jqp0b3vZZo9sL5ynV6WtKvLSOf8A1XwNlsrDlNshjDrUdnd0H6cVshFyjTpvWb3n3ershFxFXM5zuJP8F5K1XpKkpc2VFxgmxSbzk+0r6DAxtQj7zF6kzZvhPUr2GInaVRzaZcycwezS8jOyRbdE+SAqa+0a4cQ7LbJYMc3OS6kSQS4mA2pBF9CbTAFOftaq2XPYzK0w6M0gdkyu51+DXObG8tBtMAAqekTmU3PdTDSzVpNy4NdULJJABydmd579gYQEtu2Zqdkcje+9uYu7vcLQ1ocDBqODpixGV0iyEIex9oVHsYHtzONKi8uBH9I0yXSReWHTjotMwX9NbSi3aKkG36t6hQpKVICAEBwoDJVHU8xaS+i6SZ0a6+vTTgoZ5l3s2cgl2Y3uN97e5EYsfQAgOIQqdu4uAKY1N3dNw/XBcTa+L3UqK1eb7vv61M4riRBTOVlEeKoQ53IfZHskrxKm9ynh11pvefYuHldl7R01RNWoNGjs2edreUnzW51U3VrLRLcj8Phn4ktwK4FcxPkZGnyZWhvAAewL7KnDcgo8kkax3ZvhPUrYQMbXyMzSB9ZTBJ0h1RjXa8iUBXVtsuLg1rGwXubJefCwva4kBtiS2wkyJ0Qo0zbndY7ITnIa3vAEuz06bpb9kZqgM3sN0iQLfZuJ7RmaIu5pEzBY4sdfeJaboQkvNkBWvdv/AFxXilK6uUmUdB0Xrj1UBx2ipBt2reoQpKVICAEBwoDLP7S9m1mSbWDm3gjnE9eihnkWmyqbRT7rS0HvQdQSAhiyUgBAM4quKbS86D38AtOIrxoUnUnovVgldmaw31tQvf4R339BoPgF8bhm8VXlVrdVe1LuXDx0tyNryVkPMrnLUxB8TjkZynUjo2y9MMTLo6mMlrL2Y+OvuWSZLcBOMORlOlvjO7q7QeQUxUuhpU6HG28+96e5BZu4jZzM1Rg5z7Ln4LHArpMRCPbf3Z/IPQ0tZfZGsXs3wnqVSD5iDMRMX0vAGvNQpCfiMObZqRzEaZTJbdum8RbogEMdQJLh2ckhpMNBJbBaOJjukcLICThsTTuA5oiSQIESXEkjnDj7SqQVXxbMriHttzHBYVJbsGwV3agiQQRO6+kj5LwTdlb1xXyKWeH8I6Be+n1F3AcdosyDbtW9VCkpUgIAQAUBkxQDnuAD6TxNx4XCTfnrPVQzuXGzg8MOcgmdRw3eaGLJCAEBmds43tHZG3a3T8R4/JfF7Yx/5mr0VPOK836yX3NsI2VzlemWhuGb43EF/U+FvQapWpypQhgafXk05d/BeGr94WftDpY19VtIfzdEGTuMXeT1NlucKdbFRoL+XSWfhnJ+LyGivzK7FVy97nnefduHsXIxWJlWrSq83/jyMkrIs/RylLnP4CB1P+nvXb/D9NzqTqPgre//AB5mFQu6y+qNYvZvhPUoQkDf1QpT1K9ODOGfAb9yONt3H3nRUCPWKIH804CXAd2N0OgE6QB+pUAsPosGcUXE6RFxmaSLTYO06uvoYoI+Mq0Ib9URGZx1kBpnSZvOv5GPNiJWtHx8EERvXqbRIaZJgwJPdAAJPD+PNeGe9ZL1ovncyNLh/COgXUp9RdyMRx2izIIdq3qoUkqkBACAEBmX4p1Oc4Jbue0TGtiNyhla+haYOoHNkEEHeEIPICo25j8o7Np7x8R4Dh1K4G2to9FHoKb9p69i+r+HgZwjfMrcCwMaa7hpamPvO49AuLgacaFN4yotMoLnLn3L1oZyzyFYdxYx1dx775azjJ8Tvks8PJ0KMsZPryuo/OXr5kebscP1dCPtVfcwfmo/4TA2/rq+UV9fg+wusu4r1xjM1GxKGWkDvd3vbp7gF93sah0WEi3rLP36eVjRN3ZLqrqmIvZvhPUqkJA3qFK+q3E/epiztzjBghuuonKd29UDc4i9qfK5JPMmw56KAXRfiQPCwnrrz/h/qqCDi69dxcQ1lzlF9ADczPAExz3rkznGrUbvl1fm/XaZaDbajz4wA7W2l7j4rx1Z3frjn8yo0FIL6FKysYC3KkG3at6qFJSpAQAgBAZMDvO7N3ek5qb9DrMcAZ96hn3lpsp8sPcyQ4gt5624i6EYbTxwpN4uPhHzPJc7aW0I4SnfWT0Xz7l5ljG7M7hKBqvJcbeJ7juG/wA18hhMPLGVnKby1lLkvXrI2t2Q+fr6gaO7TaLfhaNSeZXqk/2hiVThlTisuyK1fezHqo6Ir1QBamwexjfmfnyWSS2hi1FZUoL3RX19aDqrtIuPxPaPLt2jRwA0/XNeDH4r8zXc1pou5afUyirIRhqJe5rBvMfmVpw1B160aa4v/JW7I2LQAIGgsF+kJKKstDzCKqpRezfCepVIO1IgyYEjfF5ECeZgRvmN6hSo7KiQS2u5txq/Q3MEHzt+HkVQIDKYAPrDhAEy6MwJLwTeYh+vLkQoBOLc1rY9Zdm8Pj9s31/Ib5J82LrdFTutXoVK5HFMAZe3k5co7wN3AkkAHc07uAXMzS3bcl4y19yyZkR6OHFSo09q50PBs62tgQDwla41N6sko6vy/wADga1i+gMBTtEINu8TeqFJSpAQAgBAZSq5jnZKjcjp7rtx4EO9llDPuLF2KbSp5jc2ji4x+rryY3GQwtJzn4LmyJXZniX1qnFzvYPyAXw7dfH4jnJ+5L5JG7KKH8bUDR6vTuJ75H23cOgXsxtWNKCwVDP/AFP/AFS5dy9dsSvmzuKPYs7IeN0GoeHBv6+auKawVD8tHryzm+XKP1+5Fm7na/1NIU/t1Lv5N3N/XNZV/wCBwio/11M5di4L12he07lauGbC49HcPLjUO6w6nX3fFfSfh7Db05VnwyXe9fL4mqo+Bfr6w1DdVCi9m+E9VSEgb1ClNUfIAdhTECQGQZtodwgDfvA3FUHTVbDpw7hAE90XAcGwIO4XHIKApqmIpOqE9g7IyYECJ1vfeeE2HRcGriOlrOopLcj58ve/IztZC/WKcE9k7NBLjlHjq903ngfceak3OCzlmk//AKl9Fn3oBsiuO0EU3QGzYX1yxHIunoVMDByrpt3td+QloabBV84nK5t47wgnnHBfQGBIdohBs+JvVCkpUgIAQAgMpWm/9LTzEOb9phn7PGCVDMaxlD6um1uZ2ZxLQdQCBAXz236cqkaUYq7bfyMovNnazhh2mm0zVcO+4fZH3R+vkufWlHZ1J0YO9WS9p/6VyXrt5FXtO/ARg2Ckzt3C+lNvE/e6D9blrwdOOEpfm6qz0gub593rkHm7I5s+nJdXqXay5/E7cP1yU2fTU5SxlfOMc++XBeuwS5Ih4isXuLzqT+guZia8q9WVSer9WM0rKw2FpSbdkU12Aw/Z02s36nqdf1yX6JgcN+XoRp8ePe9Tzyd2SF6zEbqoUXszwnqqQkN+ahRL0BUbaxmUdm3xO15D8yuDtvH9FDoKfWl5L6vT0jOEeJCptDe6dKffqc3aBvwHm5cqCVN9HLSHtT7ZaKPh1fGRkcxNUwAdT33dXae6/wC8pXqtRSlq/afe9PLPxCJewmeJ3QD4n5Lq7ChdTqeHzfyMZl0xd8wFO0VINnxN6/JQEpUAgBACAyddwzmfq3ycjtGvG7NuO72KGfAfrYx1OmHOaO0ktB1A4uHUQuVtXGLC01NK8ndLs5sRjdldgMNnJqVD3G3ceJ4dSvmcBhFXlKvXfsRzk+b5ePH6s2SdskcqPdiKoAEDRo3NaEqTq7SxSjHJaJcIr19OQyihW064tRZ4Gf2nbz+uaz2piIK2Fo9SHm+L9cb9giuLIC45mWOw8LnqZjoy/nu/PyXa2HhOmr9I9I5+PD6+Brm7I0y+1NIIBuqqUVszQ9UISW/NCkXaGKFNuY66AcSvJjsZDC0XUl4Lm/WpUrsztN5E13XcT3ObvvdB8YXx1OclfG1c5N+wucufdHh22XA29iHBTAimdG/WVT8Gz5x1ct3RqNqE+Ht1H28I+dv1MnaRKtYuJcdSZXMqYh1JOctW7maRo9m0ctNo3kSepuvu9mUOhwsIvW133vP7GiTuycxe4gp2ipBr7TevyKAloAQAgBAUVak1wyuAI5/qyhRjFYIvDGCwBueAAXJ2rg5Yro4LS7bfJGUZWK7aOKBilT/m26fiPHmvnNo4uM7YbD/y46W4vn67zZFcWPP/AJPTy/0rxf8AA3h1/W5eqdtm4bcX82az/tXrz7kTrPsKpfPGw6qld2QNXszC9nTDd5u7qfy0X6Bs7CflqChx1ff9tDzyd2S17jEEA3VVKK2ZoeqEH6lQNaXOMAXKwq1Y0oOc3ZLUqMzWqmu8vccrG6/hHD+sf1oviqtV7RrupUe7Tj5L/tL1obkt1HBV/piIDe7SbukaeQ16qqte+LmrKPs048LrTwjq3xZLcBrEHK3J9p3efx/CPfPnyXmxTdGkqT68van8l832vsMlm7nNnYftKjW7pk9Bc/l5rXs7DfmMTGHDV9y1+gk7I1JX6IecWxCi3aKkGh4m9fkUBLQAgBACApVCkPa9ZzaeUGMxg9INlw9v1508OoxfWdn3GcFmRcBRFNvrDx/u28Tx/XXguTgKEMLS/OV1+hc3z+nv5GUnd2RXV6pe4ucZJ1XHrVp1qjqT1ZmlYbWopabCwmd+c+FnvO72a+xdzYeC6Wt0sl7Mfjw92vuNc5WVjSL7I0ggBANVlSi9maHqhCs20XvqNot0jNy3iSeAAXzO2nWr14YWno1fzeb7Fb1kbYWSuQyA89kwxTZdzuPFx+AC527GvJYak7UoZylz5yfwivSy0zepwPBPaERTp2Y3idw+ZVVWFRvESVqdPKEeb4L/AJS+ZLcCC95JJJkm5XGqVJVJOcndvNmZoPRzCw01Tq6w6DX3/BfW/h7CblN13rLJdy+r+BqqPOxYlfRGsWxCinaKkG2+JvU/AqAlqgEAIAQFIdVCjdfDtcA55hjDmdztYLn7Qw9Oqoyqu0Iu77ez13Fi7aFJtDGGq6dGizRwH5r47aGOli6u9pFZJcl9TdGNiKvCZCqVMuIaBJJgLZSpSqzUIrN5Ebsa7B4cU2Bg3anid5X6FhMNHD0o048PN8Wedu7H4XoIcJQCX1Wt1cBuuQOe9ANV6zQCS5sBuY3Fm37x4CxvyKpR3ZLgWkjSSqQrts1Dn7Jg77wATvLZMNHATJK+a2zVk6qoUV7c0k32XeXxb7NcjZBZXZCeySMPTNpmo7cSNT/VC5kqe81gcO8r3nLg2tX+mPDt7TL+5kfF1gSGt8DbN58XHmV4sbiIzap0upHJdvOT7WZRXMRhaBqPDBvPs4lacLh5YitGlHj6bK3ZG0p0w1oaNAIHkv0inTjTgoR0SseYYcsgLYhRT9EA23xN6n4FAS1SAgBACAo3alQpB2y49mBuLr+wr5/8RSaoRSer+RnT1KVfHm4FQXuwMFA7Ui5s3pvPn+tV9ZsLA7kfzE1m9O7n4/DvNM5cCw2jTLmQBPfpkgb2iowu1/CDbevozWVOPwZIdGRrS8kMc5oAb2WQjUhmYzpMTmiZCFHcfVovLc1ajlcHMqNdUYPq3BpI17xlgbrpUd5gIwuJYCyoa9F7w1zXxUFyezAcIB3UwCI38rgMvaAyowOkVKWSRTquymazrBrDLR2oAuLNQF9sV4LCRmiT4g8O/t3QELbNYMd3Z7R7Q3+q2TpzJMeS+c2zXVGp+7/mSSj3K707Xp4GcFcr8R9U3sh43fzh4cGD5rj4lrB0fy0evLrvl/avmZrN3IK5BmXmxKQpgVHTL7NAaXGAC7QDfB9g4r7LYGB6Kn08lnLTu+/wsaZvgWwx9MgkOnK3MYBIiA6QY71nN0nVfQmsYdi2TEyZLYAJkjKYHHxD38CoUUzGs47w3hckiLxvaR5Kgep1g9geNDcafJQAzxN8/gUBLVICAEAxXxIagKhlQOuP1yUKQts+Bv8AW+RXz34i/kQ/V8jOnqZ55qTaInfGn+seU74Xzceg3VfXx9aeduFzZmTtkYN9WpDoyAS6J5W+K92z8DSxVTdjeyd23yzy72SUmjXARYL7ZJJWRpG8Vh2VGmm9oc06g6cR0IN53KkIrNn5PAKbhwfTAd0ztGnVpPNCjzX1R/R0/Kof8sIDvaVvuM/5rv8ALQDdWnUPiqAD8DYPTM4mfIBASdj0w1pAnU6kk+ZNygIe13Cm7ttXZctMa3uS4jlI8yuBtVww9ZYl9a27Fdt3eT7r+/3mcc1Yy/repc1877TO+ea+Wlh3KV99O/G5suWWzcJ2j4+yLu/LzXo2XgXi61n1Vm/p4/USlZGgxVJxylgEtOlwYILe7BAm+8jrZfoCSSsjQMUqbWEuBpMJABDmOZOUANs50GAIBGl+KpCM2kxsRVoiA0A94xlmDPa63MnfoZUKP0GEGWOY8zPdpvLZkmS41csySeN96oLGiwtY1rokW7oho4AA8BA8lADPE3z+BQExUgIAQEbE0ZCAoMRQdTdmb5jcf480AztOqHU2kfeuN4sbFfPfiL+RD9XyZsp6lZTplxDQJJsF8nRpSqzUIK7ZtbsanB4dtJgbI4k6SV+g4LCRwtFU4+L5s0N3Y47EsGr2j94fmvWYjZx9L/aM/wCIFAJO0aX3x5An4BAJO1KXE/8AA/8AJAcO1GcHn9380A3U2mDpTqexv/chSfsd8tJgi5sf4IQTj8AyoZdMi0grwY3ZtDFtOpe64r0zJSaILths3Pd7iuZL8OUOE5eX2MukZLwdAU25RfeTxXXwWDhhKXRw8XzZi3dkum9esg92ipCLUqwdFCnWV+XvQDmeUBxnjb5/AoCYqQEAIDhCAj18OCgKvEbJa7ULCVOM+sk+8DA2I37o9isYRjokBbdjN+6PYsgOt2S3gEA4NmDggFjZw4IBQ2eOCAWMAOCARVwdrICtLq1IksIIOrXaeR3IDo224eOi790gj3woUUNvU97Xj90/IIBP7co8SP3SgFs25Q+/7kFhz9vYf/aBBYYqbZoHR6Cxxu2aPEno1x+SAV+3G/Zpvd5R/ihALo4qq9wOUMA3ak9ShC7oOJF1QOoAQAgBAcyoAyhAGVAEIAhAdhACAEBwhAM1KAKAjvwI4IBl2zhwQCDswcEAk7LHBAH7LHBAA2WOCAWNmjggHWbPHBASaWGAQEhoQHUAIAQAgBACAEAIAQAgBACAEAIAQAgOQgCEAQgCEAQgOoAQAgBAf//Z'),
(92, 'Vagisan', 'VAGISAN Biotin-Lacto 30 caps.', '6550285', 'https://www.vagisan.com/fileadmin/user_upload/vagisan.com/images/products/CH/vagisan-packshot-biotin-lacto-switzerland.jpg');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  ADD PRIMARY KEY (`id_adresse`);

--
-- Index pour la table `t_date_visite_client`
--
ALTER TABLE `t_date_visite_client`
  ADD PRIMARY KEY (`id_date_visite_client`);

--
-- Index pour la table `t_groupage`
--
ALTER TABLE `t_groupage`
  ADD PRIMARY KEY (`id_groupage`);

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`id_mail`);

--
-- Index pour la table `t_num_tel_fax`
--
ALTER TABLE `t_num_tel_fax`
  ADD PRIMARY KEY (`id_num_tel`);

--
-- Index pour la table `t_pdv_appart_groupage`
--
ALTER TABLE `t_pdv_appart_groupage`
  ADD PRIMARY KEY (`id_pdv_appart_groupage`),
  ADD KEY `FK_Entreprise` (`FK_PDV`),
  ADD KEY `FK_Groupage` (`FK_Groupage`);

--
-- Index pour la table `t_pdv_avoir_adresse`
--
ALTER TABLE `t_pdv_avoir_adresse`
  ADD PRIMARY KEY (`id_pdv_avoir_adresse`),
  ADD KEY `FK_Entreprise` (`FK_PDV`),
  ADD KEY `FK_Adresse` (`FK_Adresse`);

--
-- Index pour la table `t_pdv_avoir_date_visite`
--
ALTER TABLE `t_pdv_avoir_date_visite`
  ADD PRIMARY KEY (`id_pdv_avoir_date_visite`),
  ADD KEY `FK_Entreprise` (`FK_PDV`),
  ADD KEY `FK_Date_Visite_Client` (`FK_Date_Visite_Client`);

--
-- Index pour la table `t_pdv_avoir_potentiel`
--
ALTER TABLE `t_pdv_avoir_potentiel`
  ADD PRIMARY KEY (`id_pdv_avoir_potentiel`),
  ADD KEY `FK_Entreprise` (`FK_PDV`),
  ADD KEY `FK_Potentiel_Client` (`FK_Potentiel_Client`);

--
-- Index pour la table `t_pdv_vendre_produits`
--
ALTER TABLE `t_pdv_vendre_produits`
  ADD PRIMARY KEY (`id_pdv_vendre_produits`),
  ADD KEY `FK_Entreprise` (`FK_PDV`),
  ADD KEY `FK_Marque_Produit` (`FK_Produits`);

--
-- Index pour la table `t_personne_ref`
--
ALTER TABLE `t_personne_ref`
  ADD PRIMARY KEY (`id_pers_ref`);

--
-- Index pour la table `t_pers_avoir_mail`
--
ALTER TABLE `t_pers_avoir_mail`
  ADD PRIMARY KEY (`id_pers_avoir_mail`),
  ADD KEY `FK_Pers_Ref` (`FK_Pers_Ref`),
  ADD KEY `FK_Mail` (`FK_Mail`);

--
-- Index pour la table `t_pers_avoir_num_tel`
--
ALTER TABLE `t_pers_avoir_num_tel`
  ADD PRIMARY KEY (`id_pers_avoir_num_tel`),
  ADD KEY `FK_Pers_Ref` (`FK_Pers_Ref`),
  ADD KEY `FK_Num_Tel` (`FK_Num_Tel`);

--
-- Index pour la table `t_pers_travailler_pdv`
--
ALTER TABLE `t_pers_travailler_pdv`
  ADD PRIMARY KEY (`id_pers_travailler_pdv`),
  ADD KEY `FK_Pers_Ref` (`FK_Pers_Ref`),
  ADD KEY `FK_Entreprise` (`FK_PDV`);

--
-- Index pour la table `t_points_de_vente`
--
ALTER TABLE `t_points_de_vente`
  ADD PRIMARY KEY (`id_pdv`);

--
-- Index pour la table `t_potentiel_client`
--
ALTER TABLE `t_potentiel_client`
  ADD PRIMARY KEY (`id_potentiel_client`);

--
-- Index pour la table `t_produits`
--
ALTER TABLE `t_produits`
  ADD PRIMARY KEY (`id_produits`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_adresse`
--
ALTER TABLE `t_adresse`
  MODIFY `id_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_date_visite_client`
--
ALTER TABLE `t_date_visite_client`
  MODIFY `id_date_visite_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_groupage`
--
ALTER TABLE `t_groupage`
  MODIFY `id_groupage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `id_mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_num_tel_fax`
--
ALTER TABLE `t_num_tel_fax`
  MODIFY `id_num_tel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pdv_appart_groupage`
--
ALTER TABLE `t_pdv_appart_groupage`
  MODIFY `id_pdv_appart_groupage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pdv_avoir_adresse`
--
ALTER TABLE `t_pdv_avoir_adresse`
  MODIFY `id_pdv_avoir_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pdv_avoir_date_visite`
--
ALTER TABLE `t_pdv_avoir_date_visite`
  MODIFY `id_pdv_avoir_date_visite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pdv_avoir_potentiel`
--
ALTER TABLE `t_pdv_avoir_potentiel`
  MODIFY `id_pdv_avoir_potentiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pdv_vendre_produits`
--
ALTER TABLE `t_pdv_vendre_produits`
  MODIFY `id_pdv_vendre_produits` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT pour la table `t_personne_ref`
--
ALTER TABLE `t_personne_ref`
  MODIFY `id_pers_ref` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pers_avoir_mail`
--
ALTER TABLE `t_pers_avoir_mail`
  MODIFY `id_pers_avoir_mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pers_avoir_num_tel`
--
ALTER TABLE `t_pers_avoir_num_tel`
  MODIFY `id_pers_avoir_num_tel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_pers_travailler_pdv`
--
ALTER TABLE `t_pers_travailler_pdv`
  MODIFY `id_pers_travailler_pdv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_points_de_vente`
--
ALTER TABLE `t_points_de_vente`
  MODIFY `id_pdv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_potentiel_client`
--
ALTER TABLE `t_potentiel_client`
  MODIFY `id_potentiel_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `t_produits`
--
ALTER TABLE `t_produits`
  MODIFY `id_produits` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_pdv_appart_groupage`
--
ALTER TABLE `t_pdv_appart_groupage`
  ADD CONSTRAINT `t_pdv_appart_groupage_ibfk_1` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`),
  ADD CONSTRAINT `t_pdv_appart_groupage_ibfk_2` FOREIGN KEY (`FK_Groupage`) REFERENCES `t_groupage` (`id_groupage`);

--
-- Contraintes pour la table `t_pdv_avoir_adresse`
--
ALTER TABLE `t_pdv_avoir_adresse`
  ADD CONSTRAINT `t_pdv_avoir_adresse_ibfk_1` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`),
  ADD CONSTRAINT `t_pdv_avoir_adresse_ibfk_2` FOREIGN KEY (`FK_Adresse`) REFERENCES `t_adresse` (`id_adresse`);

--
-- Contraintes pour la table `t_pdv_avoir_date_visite`
--
ALTER TABLE `t_pdv_avoir_date_visite`
  ADD CONSTRAINT `t_pdv_avoir_date_visite_ibfk_1` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`),
  ADD CONSTRAINT `t_pdv_avoir_date_visite_ibfk_2` FOREIGN KEY (`FK_Date_Visite_Client`) REFERENCES `t_date_visite_client` (`id_date_visite_client`);

--
-- Contraintes pour la table `t_pdv_avoir_potentiel`
--
ALTER TABLE `t_pdv_avoir_potentiel`
  ADD CONSTRAINT `t_pdv_avoir_potentiel_ibfk_1` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`),
  ADD CONSTRAINT `t_pdv_avoir_potentiel_ibfk_2` FOREIGN KEY (`FK_Potentiel_Client`) REFERENCES `t_potentiel_client` (`id_potentiel_client`);

--
-- Contraintes pour la table `t_pdv_vendre_produits`
--
ALTER TABLE `t_pdv_vendre_produits`
  ADD CONSTRAINT `t_pdv_vendre_produits_ibfk_1` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`),
  ADD CONSTRAINT `t_pdv_vendre_produits_ibfk_2` FOREIGN KEY (`FK_Produits`) REFERENCES `t_produits` (`id_produits`);

--
-- Contraintes pour la table `t_pers_avoir_mail`
--
ALTER TABLE `t_pers_avoir_mail`
  ADD CONSTRAINT `t_pers_avoir_mail_ibfk_1` FOREIGN KEY (`FK_Pers_Ref`) REFERENCES `t_personne_ref` (`id_pers_ref`),
  ADD CONSTRAINT `t_pers_avoir_mail_ibfk_2` FOREIGN KEY (`FK_Mail`) REFERENCES `t_mail` (`id_mail`);

--
-- Contraintes pour la table `t_pers_avoir_num_tel`
--
ALTER TABLE `t_pers_avoir_num_tel`
  ADD CONSTRAINT `t_pers_avoir_num_tel_ibfk_1` FOREIGN KEY (`FK_Pers_Ref`) REFERENCES `t_personne_ref` (`id_pers_ref`),
  ADD CONSTRAINT `t_pers_avoir_num_tel_ibfk_2` FOREIGN KEY (`FK_Num_Tel`) REFERENCES `t_num_tel_fax` (`id_num_tel`);

--
-- Contraintes pour la table `t_pers_travailler_pdv`
--
ALTER TABLE `t_pers_travailler_pdv`
  ADD CONSTRAINT `t_pers_travailler_pdv_ibfk_1` FOREIGN KEY (`FK_Pers_Ref`) REFERENCES `t_personne_ref` (`id_pers_ref`),
  ADD CONSTRAINT `t_pers_travailler_pdv_ibfk_2` FOREIGN KEY (`FK_PDV`) REFERENCES `t_points_de_vente` (`id_pdv`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
