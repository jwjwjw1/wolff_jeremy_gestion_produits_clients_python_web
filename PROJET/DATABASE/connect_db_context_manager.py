# connect_db_context_manager.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Classe pour se connecter à la base de données
# --------------------------------------------------------------------------------------------------------------------
#

from PROJET.DATABASE.erreurs import *
# Petits messages "flash", échange entre Python et Jinja dans une page en HTML
from flask import flash


class MaBaseDeDonnee():
    # Quand on instancie la classe, le code __init__ est interprété
    def __init__(self):
        self.host = '127.0.0.1'
        self.user = 'root'
        self.password = 'root'
        self.db = "wolff_jeremy_gestion_produits_clients_104_2020"

        self.connexion_bd = None
        try:
            # Connexion à notre base de données
            self.connexion_bd = pymysql.connect(host=self.host,
                                                user=self.user,
                                                password=self.password,
                                                db=self.db,
                                                cursorclass=pymysql.cursors.DictCursor,
                                                autocommit=False)
            print("CONNEXION A LA BASE DE DONNEES ETABLIE AVEC SUCCES")
            print("self.connexion", dir(self.connexion_bd), "type of self.connexion : ", type(self.connexion_bd))

        # Problème avec la base de données (non connectée, nom erroné, etc)
        except (Exception,
                ConnectionRefusedError,
                pymysql.err.OperationalError,
                pymysql.err.DatabaseError) as erreur:
            # SI LA BD N'EST PAS CONNECTÉE, ON ENVOIE AU TERMINAL DES MESSAGES POUR RASSURER L'UTILISATEUR.
            # Petits messages "flash", échange entre Python et Jinja dans une page en HTML
            flash(f"Flash...Base de données non connectée. Erreur : {erreur.args[1]}", "danger")
            print("erreur...MaBaseDeDonnee.__init__ ", erreur.args[1])
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")
        print("Avec CM BD  INIT!")

    # Après la méthode __init__ il passe à __enter__, c'est là qu'il faut surveiller le bon déroulement
    # des actions. En cas de problèmes, il ne va pas dans la méthode __exit__
    def __enter__(self):
        return self

    # Méthode de sortie de la classe, c'est là que se trouve tout ce qui doit être fermé
    # Si un problème (une Exception est levée avant (__init__ ou __enter__) cette méthode
    # n'est pas interprétée
    def __exit__(self, exc_type, exc_val, traceback):
        # La valeur des paramètres est "None" si tout s'est bien déroulé.
        print("exc_val ", exc_val)
        """
            Si la sortie se passe bien ==> commit. Si exception ==> rollback
            
            Tous les paramètres sont de valeur "None" s'il n'y a pas eu d'EXCEPTION.
            En Python "None" est défini par la valeur "False"
        """
        if exc_val is None:
            print("commit! Dans le destructeur ")
            self.connexion_bd.commit()
        else:
            print("rollback! Dans le destructeur ")
            self.connexion_bd.rollback()

        # Fermeture de la connexion à la base de donnée.
        self.connexion_bd.close()
        print("La BD est FERMÉE! Dans le destructeur")

    # Cette méthode est définie pour utiliser les "context manager"
    # Une fois l'interprétation de cette méthode terminée
    # le destructeur "__exit__" sera automatiquement interprété.
    # ainsi après avoir exécuté la requête MySql on va faire un commit (enregistrer les modifications)
    # s'il n'y a pas erreur ou un rollback (retour en arrière) en cas d'erreur
    # et finalement fermer la connexion à la BD.
    def mabd_execute(self, sql, params=None):
        print("execute", sql, " params", params)
        return self.connexion_bd.cursor().execute(sql, params or ())

    # Cette méthode est définie pour utiliser les "context manager"
    def mabd_fetchall(self):
        return self.connexion_bd.cursor().fetchall()
