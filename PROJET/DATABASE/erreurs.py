# erreurs.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Définition des erreurs "personnalisées"
# --------------------------------------------------------------------------------------------------------------------
#

# Dérivation des classes standard des "except" dans les blocs "try...except"
import pymysql
from pymysql import IntegrityError


# Définition d'une classe qui permet, dès que "except" est détecté dans le bloc "try... except MonErreur"
# @app.errorhandler(Exception) est activé et permet de transmettre le message "flash"
# à la "home.html". Voir le fichier "run_mon_app.py"
# c'est une dérivation des classes "standard built-in exceptions"
class MonErreur(Exception):
    pass


class MaBdErreurConnexion(Exception):
    pass


class MaBdErreurOperation(Exception):
    pass


class MaBdErreurDoublon(IntegrityError):
    pass


class MaBdErreurPyMySl(pymysql.Error):
    pass


class MaBdErreurDelete(pymysql.Error):
    pass


msg_erreurs = {
    "ErreurConnexionBD": {
        "message": "Pas de connexion à la base de données! Veuillez démarrer un serveur MySQL.",
        "status": 400
    },
    "ErreurDoublonValue": {
        "message": "Cette valeur existe déjà.",
        "status": 400
    },
    "ErreurDictionnaire": {
        "message": "Une valeur du dictionnaire n'existe pas.",
        "status": 400
    },
    "ErreurStructureTable": {
        "message": "Problème dans la structure des tables.",
        "status": 400
    },
    "ErreurNomBD": {
        "message": "Problème avec le nom de la base de données.",
        "status": 400
    },
    "ErreurPyMySql": {
        "message": "Problème en relation avec la base de données.",
        "status": 400
    },
    "ErreurDeleteContrainte": {
        "message": "Impossible de supprimer: cette valeur est référencée ailleurs!",
        "status": 400
    }
}

# En préparation pour galérer avec la gestion des erreurs
from pymysql.constants import ER

error_codes = {
    ER.TABLE_EXISTS_ERROR: "Table already exists",
    ER.ACCESS_DENIED_ERROR: "Access denied",
    ER.BAD_FIELD_ERROR: "Colonne inexistante"
}
