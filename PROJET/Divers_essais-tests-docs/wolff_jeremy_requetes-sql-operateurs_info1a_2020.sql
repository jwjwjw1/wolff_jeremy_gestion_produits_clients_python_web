-- JW, 10.03.2020
-- FICHIER MYSQL DE DIVERSES REQUETES SQL
--
-- Requêtes SQL utilisant des opérateurs dans le but d'apprendre le langage informatique SQL (nous permettant de manipuler des données au sein d'une BDD)
--
-- Requêtes effectuées dans PHPMyAdmin dans la BDD wolff_jeremy_gestion_produits_clients_104_2020
--
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
	Affiche toutes les lignes de la table `t_personne_ref` où la colonne `PrenomPersRef` a comme valeur 'Robert'
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` LIKE 'Robert' 

/*
	Affiche toutes les lignes de la table `t_personne_ref` où la colonne `PrenomPersRef` contient 'obe' dans sa valeur (pour le prénom Robert, cela fonctionne)
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` LIKE '%obe%' 

/*
	Affiche toutes les lignes de la table `t_personne_ref` où la colonne `PrenomPersRef` n'a pas comme valeur 'Olivier'
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` NOT LIKE 'Olivier' 

/*
	Affiche toutes les lignes de la table `t_personne_ref` où la colonne `PrenomPersRef` a la valeur 'Isabelle'
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` = 'Isabelle' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `NomPersRef` n'a pas comme valeur 'Maccaud'
*/
SELECT * FROM `t_personne_ref` WHERE `NomPersRef` != 'Maccaud' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PersPersRef` a une entrée commençant par la lettre 'r'
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` REGEXP '^r' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `NomPersRef` a exactement 'maret' comme valeur
*/
SELECT * FROM `t_personne_ref` WHERE `NomPersRef` REGEXP '^maret$' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PrenomPersRef` n'a pas de 'a', de 'b' et 'c'
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` NOT REGEXP '[a-c]' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PrenomPersRef` est vide
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` = '' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PrenomPersRef` n'est pas vide
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` != '' 

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PrenomPersRef` contient le mot indiqué en chaîne de caractères (ici yves)
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` IN ('yves')

/*
	Affiche tous les enregistrements de la table `t_personne_ref` où la colonne `PrenomPersRef` ne contient pas le mot indiqué en chaîne de caractères (ici arnuld)
*/
SELECT * FROM `t_personne_ref` WHERE `PrenomPersRef` NOT IN ('arnuld')

/*
	Affiche tous les enregistrements de la table `t_date_visite_client` où la colonne `DateVisiteClient` contient des dates de visite de clients entre le 10.01.2013 et le 10.01.2016
*/
SELECT * FROM `t_date_visite_client` WHERE `DateVisiteClient` BETWEEN '2013-01-10' AND '2016-01-10' 

/*
	Affiche tous les enregistrements de la table `t_date_visite_client` où la colonne `DateVisiteClient` contient des dates de visite de clients qui ne sont pas entre le 10.01.2013 et le 10.01.2016
*/
SELECT * FROM `t_date_visite_client` WHERE `DateVisiteClient` NOT BETWEEN '2013-01-10' AND '2016-01-10' 

/*
	Affiche tous les enregistrements de la table `t_num_tel` où la colonne `NumTel` ne contient pas de numéro de téléphone
*/
SELECT * FROM `t_num_tel` WHERE `NumTel` IS NULL 

/*
	Affiche tous les enregistrements de la table `t_num_tel` où la colonne `NumTel` contient des numéros de téléphone
*/
SELECT * FROM `t_num_tel` WHERE `NumTel` IS NOT NULL 