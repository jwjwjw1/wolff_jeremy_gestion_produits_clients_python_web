# requete_sql_facile.py
# Wolff Jérémy - INFO1A - 2020.06.14
# Création d'une requête SQL d'insertion de données dans la BD à l'aide d'une boucle while
# --------------------------------------------------------------------------------------------------------------------
#

# Création de plusieurs lignes de requêtes SQL à l'aide d'une boucle "while"
i = 1
while i < 51:
    print(
        "INSERT INTO t_pdv_vendre_produits(id_pdv_vendre_produits,FK_PDV,FK_Produits,DateInsertionProduit) VALUES ("
        "NULL,",
        i, ",", i, ",NULL);")
    i += 1
