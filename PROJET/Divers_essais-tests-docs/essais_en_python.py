# essais_en_python.py
# Wolff Jérémy - INFO1A - 2020.03.12
# Essais, tests, de petites fonctions, opérations basiques, en Python dans le but d'apprendre gentiment ce langage
# --------------------------------------------------------------------------------------------------------------------
#

# Fonctions print() et input()
# Dans les parenthèses se trouvent les arguments
prenom = input("Entrez votre prénom : ")
# "\n" inséré dans une chaîne de caractères (string) permet de faire un saut de ligne lors de l'exécution
print("Bonjour", prenom, "!\n")

# Concaténation de chaînes de caractères
a = prenom
b = " est un stupide hobbit jouflu!\n"
c = a + b
print(c)

# Calculs d'une moyenne suite à une saisie de plusieurs notes Utilisation de la boucle while, de la fonction print(),
# d'incrémentation (compteur), concaténation, calculs tout bête...
note = 1
i = 1
sommeNote = 0
moyenne = 0

print("Calculons tes notes mon petit " + a + " :\nNous allons voir si t'es si stupide que ça..."
                                             "\nEntre '0' pour afficher ta moyenne mon brave\n----------")
while note != 0:
    note = float(input("Note " + str(i) + ": "))
    i = i + 1
    sommeNote = sommeNote + note

moyenne = sommeNote / (i-2)
print("----------\nTa moyenne de champion...: " + str(moyenne))
