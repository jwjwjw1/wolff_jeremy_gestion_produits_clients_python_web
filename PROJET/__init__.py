# __init__.py
# Wolff Jérémy - INFO1A - 2020.04.24
# Permet d'exécuter le code d'initialisation de l'application, le constructeur
# -------------------------------------------------------------------------------------
from flask import Flask, flash, render_template
from PROJET.DATABASE import connect_db_context_manager

# Un objet "app" pour utiliser la classe Flask et qui fait "exister" notre application
app = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
app.secret_key = '_(_oeilDeSauron_)?^'


# Sont indiquées ci-dessous les routes de l'application
from PROJET import routes
from PROJET.TABLES_DB.ADRESSE import routes_gestion_adresse
from PROJET.TABLES_DB.POINTS_DE_VENTE import routes_gestion_pdv
from PROJET.TABLES_DB.PRODUITS import routes_gestion_produits
from PROJET.TABLES_DB.PDV_AVOIR_DONNEES import routes_gestion_pdv_avoir_donnees
from PROJET.TABLES_DB.PERSONNE_REF import routes_gestion_pers_ref
from PROJET.TABLES_DB.NUM_TEL_FAX import routes_gestion_num_tel_fax
from PROJET.TABLES_DB.GROUPAGE import routes_gestion_groupage
from PROJET.TABLES_DB.POTENTIEL_CLIENT import routes_gestion_potentiel_client
from PROJET.TABLES_DB.MAIL import routes_gestion_mail
from PROJET.TABLES_DB.DATE_VISITE_CLIENT import routes_gestion_date_visite_client
from PROJET.TABLES_DB.POTENTIEL_CLIENT import routes_gestion_potentiel_client
