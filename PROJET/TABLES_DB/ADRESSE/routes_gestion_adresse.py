# routes_gestion_adresse.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Gestions des "routes" FLASK pour les adresses
# --------------------------------------------------------------------------------------------------------------------
#
import string

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.ADRESSE.data_gestion_adresse import GestionAdresse
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresse_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/adresse_afficher//<string:order_by>/<int:id_adresse_sel>", methods=['GET', 'POST'])
def adresse_afficher(order_by, id_adresse_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = GestionAdresse()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionAdresse()
            # Fichier data_gestion_pdv.py
            data_adresse = obj_actions_adresse.adresse_afficher_data(order_by, id_adresse_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data adresse", data_adresse, "type ", type(data_adresse))

            # Différencier les messages si la table est vide.
            if not data_adresse and id_adresse_sel == 0:
                flash("""La table "t_adresse" est vide!""", "warning")
            elif not data_adresse and id_adresse_sel > 0:
                # Si l'utilisateur change l'id_adresse dans l'URL et que le numéro n'existe pas,
                flash(f"L'adresse demandée n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_adresse" est vide.
                # La ligne ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données des adresses affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("adresse/adresse_afficher.html", data=data_adresse)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresse_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/adresse_add", methods=['GET', 'POST'])
def adresse_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = GestionAdresse()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            RueNom = request.form['RueNom_html']
            NumeroRue = request.form['NumeroRue_html']
            CodePostal = request.form['CodePostal_html']
            VilleNom = request.form['VilleNom_html']
            Canton = request.form['Canton_html']

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*["
                            "'\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$",
                                RueNom):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.
                return render_template("adresse/adresse_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_RueNom": RueNom,
                                                  "value_NumeroRue": NumeroRue,
                                                  "value_CodePostal": CodePostal,
                                                  "value_VilleNom": VilleNom,
                                                  "value_Canton": Canton}
                obj_actions_adresse.add_adresse_data(valeurs_insertion_dictionnaire)

                # Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'adresse_afficher', car l'utilisateur
                # doit voir la nouvelle adressse qu'il vient d'insérer.
                return redirect(url_for('adresse_afficher', order_by='DESC', id_adresse_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("adresse/adresse_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresse_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/adresse_edit', methods=['POST', 'GET'])
def adresse_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "num_tel_fax_afficher.html"
    if request.method == 'GET':
        try:
            id_adresse_edit = request.values['id_adresse_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_adresse_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_adresse": id_adresse_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = GestionAdresse()

            # La commande MySql est envoyée à la BD
            data_id_adresse = obj_actions_adresse.edit_adresse_data(valeur_select_dictionnaire)
            print("dataIdAdresse ", data_id_adresse, "type ", type(data_id_adresse))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer une adresse!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("adresse/adresse_edit.html", data=data_id_adresse)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresse_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/adresse_update', methods=['POST', 'GET'])
def adresse_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_adresse_edit = request.values['id_adresse_edit_html']

            # Récupère les contenus des champs de l'adresse dans le formulaire HTML "num_tel_fax_edit.html"
            RueNom = request.values['name_edit_RueNom_html']
            NumeroRue = request.values['name_edit_NumeroRue_html']
            CodePostal = request.values['name_edit_CodePostal_html']
            VilleNom = request.values['name_edit_VilleNom_html']
            Canton = request.values['name_edit_Canton_html']
            valeur_edit_list = [{'id_adresse': id_adresse_edit,
                                 'RueNom': RueNom,
                                 'NumeroRue': NumeroRue,
                                 'CodePostal': CodePostal,
                                 'VilleNom': VilleNom,
                                 'Canton': Canton}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$", RueNom):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "Danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des adresses
                valeur_edit_list = [{'id_adresse': id_adresse_edit,
                                     'RueNom': RueNom,
                                     'NumeroRue': NumeroRue,
                                     'CodePostal': CodePostal,
                                     'VilleNom': VilleNom,
                                     'Canton': Canton}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "num_tel_fax_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('adresse/adresse_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_adresse": id_adresse_edit,
                                              "value_RueNom": RueNom,
                                              "value_NumeroRue": NumeroRue,
                                              "value_CodePostal": CodePostal,
                                              "value_VilleNom": VilleNom,
                                              "value_Canton": Canton}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_adresse = GestionAdresse()

                # La commande MySql est envoyée à la base de données
                data_id_adresse = obj_actions_adresse.update_adresse_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIdAdresse ", data_id_adresse, "type ", type(data_id_adresse))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Adresse modifiée.", "success")
                # Affichage des adresses
                return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=id_adresse_edit))

        except (Exception,
                # pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                # pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args[0])
            flash(f"problème adresse update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une adresse, on renvoie le formulaire "EDIT"
            return render_template('adresse/adresse_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresse_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une adresse par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/adresse_select_delete', methods=['POST', 'GET'])
def adresse_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = GestionAdresse()
            # Récupérer la valeur de "idAdresseDeleteHTML" du formulaire html "num_tel_fax_delete.html"
            id_adresse_delete = request.args.get('id_adresse_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}


            # La commande MySql est envoyée à la base de données
            data_id_adresse = obj_actions_adresse.delete_select_adresse_data(valeur_delete_dictionnaire)
            flash(f"Effacer une adresse!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('adresse/adresse_delete.html', data = data_id_adresse)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /adresseUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une adresse, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/adresse_delete', methods=['POST', 'GET'])
def adresse_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_adresse = GestionAdresse()
            # Récupérer la valeur de "id_adresse" du formulaire html "AdresseAfficher.html"
            id_adresse_delete = request.form['id_adresse_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_adresse": id_adresse_delete}

            data_adresse = obj_actions_adresse.delete_adresse_data(valeur_delete_dictionnaire)
            # Affichage de la liste des adresses
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les adresses
            return redirect(url_for('adresse_afficher', order_by="'ASC", id_adresse_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer une adresse qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des adresses
                return redirect(url_for('adresse_afficher', order_by="ASC", id_adresse_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur adresse_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('adresse/adresse_afficher.html', data=data_adresse)