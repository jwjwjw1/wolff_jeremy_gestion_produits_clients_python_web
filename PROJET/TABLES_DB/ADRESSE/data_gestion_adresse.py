# data_gestion_adresse.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Permet de gérer (CRUD) les données de la table "t_adresse"
# --------------------------------------------------------------------------------------------------------------------
#

from flask import flash

from PROJET.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from PROJET.DATABASE.erreurs import *


class GestionAdresse():
    def __init__(self):
        try:
            # DEBUG : Pour afficher un message dans la console.
            print("dans le try de gestions adresse")
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans GestionAdresse: Erreur, il faut connecter une base de donnée!", "danger")
            # DEBUG : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionAdresse {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionAdresse ")

    def adresse_afficher_data(self, valeur_order_by, id_adresse_sel):
        try:
            print("valeur_order_by ", valeur_order_by, type(valeur_order_by))

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Afficher soit la liste des adresses dans l'ordre inverse ou simplement l'adresse sélectionnée
                # par l'action edit
                if valeur_order_by == "ASC" and id_adresse_sel == 0:
                    strsql_adresse_afficher = """SELECT id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton 
                    FROM t_adresse ORDER BY id_adresse ASC """
                    mc_afficher.execute(strsql_adresse_afficher)
                elif valeur_order_by == "ASC":
                    # Constitution d'un dictionnaire pour associer l'id de l'adresse sélectionnée avec un nom de
                    # variable
                    valeur_id_adresse_selected_dictionnaire = {"value_id_adresse_selected": id_adresse_sel}
                    strsql_adresse_afficher = """SELECT id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton 
                    FROM t_adresse  WHERE id_adresse = %(value_id_adresse_selected)s """
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_adresse_afficher, valeur_id_adresse_selected_dictionnaire)
                else:
                    strsql_adresse_afficher = """SELECT id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton 
                    FROM t_adresse ORDER BY id_adresse DESC """
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_adresse_afficher)
                # Récupère les données de la requête.
                data_adresse = mc_afficher.fetchall()
                # Retourne les données du "SELECT"
                return data_adresse
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_adresse_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            strsql_insert_adresse = """INSERT INTO t_adresse 
            (id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton) 
            VALUES (NULL,%(value_RueNom)s, %(value_NumeroRue)s, %(value_CodePostal)s, 
            %(value_VilleNom)s, %(value_Canton)s)"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_adresse, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")


    def edit_adresse_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Commande MySql pour afficher l'adresse sélectionnée dans le tableau dans le formulaire HTML
            str_sql_id_adresse = "SELECT id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton FROM t_adresse " \
                                 "WHERE id_adresse = %(value_id_adresse)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_adresse, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_adresse_data Data Gestions Adresse numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Adresse numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_adresse_data d'une adresse Data Gestions Adresse {erreur}")

    def update_adresse_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleAdresseHTML" du form HTML "num_tel_fax_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_RueNom = "UPDATE t_adresse SET RueNom = %(value_RueNom)s WHERE id_adresse = %(value_id_adresse)s"
            str_sql_update_NumeroRue = "UPDATE t_adresse SET NumeroRue = %(value_NumeroRue)s WHERE id_adresse = %(value_id_adresse)s"
            str_sql_update_CodePostal = "UPDATE t_adresse SET CodePostal = %(value_CodePostal)s WHERE id_adresse = %(value_id_adresse)s"
            str_sql_update_VilleNom = "UPDATE t_adresse SET VilleNom = %(value_VilleNom)s WHERE id_adresse = %(value_id_adresse)s"
            str_sql_update_Canton = "UPDATE t_adresse SET Canton = %(value_Canton)s WHERE id_adresse = %(value_id_adresse)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_RueNom, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_NumeroRue, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_CodePostal, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_VilleNom, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_Canton, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_adresse_data Data Gestions Adresse numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Adresse numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_adresse_data d\'une adresse Data Gestions Adresse {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon! Introduire une valeur différente!')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_adresse_data Data Gestions Adresse numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_adresse_data d'une aderesse DataGestionsAdresse {erreur}")

    def delete_select_adresse_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleAdresseHTML" du
            # form HTML "num_tel_fax_edit.html" le "%s" permet d'éviter des injections SQL "simples"

            # Commande MySql pour afficher l'adresse sélectionnée dans le tableau dans le formulaire HTML
            str_sql_select_id_adresse = "SELECT id_adresse, RueNom, NumeroRue, CodePostal, VilleNom, Canton " \
                                        "FROM t_adresse WHERE id_adresse = %(value_id_adresse)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_adresse, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_select_adresse_data Gestions Adresse numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_adresse_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_adresse_data d\'une adresse Data Gestions Adresse {erreur}")


    def delete_adresse_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "num_tel_fax_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_adresse = "DELETE FROM t_adresse WHERE id_adresse = %(value_id_adresse)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_adresse, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_adresse_data Data Gestions Adresse numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Adresse numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # flash(f"Flash. Impossible d'effacer! Cette adresse est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}", "danger")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette adresse est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")