# routes_gestion_pdv_avoir_donnees.py
# Wolff Jérémy - 2020.06.25 - INFO1A
# Gestions des "routes" FLASK pour les table intermédiaires qui associent les points de vente et leurs données
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, request, flash, session
from PROJET import app
from PROJET.TABLES_DB.PRODUITS.data_gestion_produits import GestionProduits
from PROJET.TABLES_DB.PDV_AVOIR_DONNEES.data_gestion_pdv_avoir_donnees import GestionPDVVendreProduits

# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_avoir-donnees_afficher_concat
# Récupère la liste de toutes les données associées aux points de vente.
# ---------------------------------------------------------------------------------------------------
@app.route("/pdv_avoir_donnees_afficher_concat/<int:id_pdv_sel>", methods=['GET', 'POST'])
def pdv_avoir_donnees_afficher_concat (id_pdv_sel):
    print("id_pdv_sel ", id_pdv_sel)
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionPDVVendreProduits()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionPDVVendreProduits()
            # Fichier data_gestion_pdv.py
            data_pdv_avoir_donnees_afficher_concat = obj_actions_produits.pdv_avoir_donnees_afficher_data_concat(id_pdv_sel)
            # DEBUG : Pour afficher le résultat et son type.
            print(" data produits", data_pdv_avoir_donnees_afficher_concat, "type ", type(data_pdv_avoir_donnees_afficher_concat))

            if data_pdv_avoir_donnees_afficher_concat:
                flash(f"Données par point de vente affichées!", "success")
            else:
                flash(f"""Le produit demandé n'existe pas. Ou la table "t_pdv_vendre_produits" est vide!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("pdv_avoir_donnees/pdv_avoir_donnees_afficher.html",
                           data=data_pdv_avoir_donnees_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_avoir_donnees_edit_pdv_avoir_donnees_selected
# Récupère la liste de tous les pdv ayant les produits sélectionnés.
# Nécessaire pour afficher tous les "TAGS" des points de vente, ainsi l'utilisateur voit les points de vente à disposition
# ---------------------------------------------------------------------------------------------------
@app.route("/pdv_avoir_donnees_edit_pdv_avoir_donnees_selected", methods=['GET', 'POST'])
def pdv_avoir_donnees_edit_pdv_avoir_donnees_selected():
    if request.method == "GET":
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionPDV()
            # Fichier data_gestion_pdv.py
            # Pour savoir si la table "t_pdv" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(pdv_appart_groupage_modifier_tags_dropbox.html)
            data_produits_all = obj_actions_produits.produits_afficher_data('ASC', 0)

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_produits = GestionPDVVendreProduits()

            # Récupère la valeur de "id_pdv" du formulaire html "pdv_avoir_donnees_afficher.html"
            id_pdv_avoir_donnees_edit = request.values['id_pdv_avoir_donnees_edit_html']

            # Mémorise l'id du point de vente dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_pdv_avoir_donnees_edit'] = id_pdv_avoir_donnees_edit

            # Constitution d'un dictionnaire pour associer l'id du point de vente sélectionné avec un nom de variable
            valeur_id_pdv_selected_dictionnaire = {"value_id_pdv_selected": id_pdv_avoir_donnees_edit}

            data_pdv_avoir_donnees_selected, data_pdv_avoir_donnees_non_attribues, data_pdv_avoir_donnees_attribues = \
                obj_actions_produits.pdv_avoir_donnees_afficher_data(valeur_id_pdv_selected_dictionnaire)

            lst_data_pdv_selected = [item['id_pdv'] for item in data_pdv_avoir_donnees_selected]
            # DEBUG : Pour afficher le résultat et son type.
            print("lst_data_pdv_selected  ", lst_data_pdv_selected,
                  type(lst_data_pdv_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les produits qui ne sont pas encore sélectionnés.
            lst_data_pdv_avoir_donnees_non_attribues = [item['id_produits'] for item in data_pdv_avoir_donnees_non_attribues]
            session['session_lst_data_pdv_avoir_donnees_non_attribues'] = lst_data_pdv_avoir_donnees_non_attribues
            # DEBUG : Pour afficher le résultat et son type.
            print("lst_data_produits_pdv_non_attribues  ", lst_data_pdv_avoir_donnees_non_attribues,
                  type(lst_data_pdv_avoir_donnees_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les produits qui sont déjà sélectionnés.
            lst_data_pdv_avoir_donnees_old_attribues = [item['id_produits'] for item in data_pdv_avoir_donnees_attribues]
            session['session_lst_data_pdv_avoir_donnees_old_attribues'] = lst_data_pdv_avoir_donnees_old_attribues
            # DEBUG : Pour afficher le résultat et son type.
            print("lst_data_pdv_avoir_donnees_old_attribues  ", lst_data_pdv_avoir_donnees_old_attribues,
                  type(lst_data_pdv_avoir_donnees_old_attribues))

            # DEBUG : Pour afficher le résultat et son type.
            print(" data data_pdv_avoir_donnees_selected", data_pdv_avoir_donnees_selected, "type ", type(data_pdv_avoir_donnees_selected))
            print(" data data_pdv_avoir_donnees_non_attribues ", data_pdv_avoir_donnees_non_attribues, "type ",
                  type(data_pdv_avoir_donnees_non_attribues))
            print(" data_pdv_avoir_donnees_attribues ", data_pdv_avoir_donnees_attribues, "type ",
                  type(data_pdv_avoir_donnees_attribues))

            # Extrait les valeurs contenues dans la table "t_produits", colonne "DescriptionProduit"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_produits
            lst_data_pdv_avoir_donnees_non_attribues = [item['DescriptionProduit'] for item in data_pdv_avoir_donnees_non_attribues]
            # DEBUG : Pour afficher le résultat et son type.
            print("lst_all_donnees pdv_avoir_donnees_edit_pdv_avoir_donnees_selected ", lst_data_pdv_avoir_donnees_non_attribues,
                  type(lst_data_pdv_avoir_donnees_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_pdv_selected == [None]:
                flash(f"""Le PDV demandé n'existe pas. Ou la table "t_pdv_vendre_produits" est vide!""", "warning")
            else:
                flash(f"Données affichées!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # On dérive "Exception" par le "@app.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("pdv_avoir_donnees/pdv_avoir_donnees_modifier_tags_dropbox.html",
                           data_produits=data_produits_all,
                           data_pdv_selected=data_pdv_avoir_donnees_selected,
                           data_donnees_attribues=data_pdv_avoir_donnees_attribues,
                           data_donnees_non_attribues=data_pdv_avoir_donnees_non_attribues)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_avoir_donnees_update_pdv_avoir_donnees_selected
# Récupère la liste de tous les produits du point de vente sélectionné.
# Nécessaire pour afficher tous les "TAGS" des produits, ainsi l'utilisateur voit les produits à disposition
# ---------------------------------------------------------------------------------------------------
@app.route("/pdv_avoir_donnees_update_pdv_avoir_donnees_selected", methods=['GET', 'POST'])
def pdv_avoir_donnees_update_pdv_avoir_donnees_selected():
    if request.method == "POST":
        try:
            # Récupère l'id du point de vente sélectionné
            id_pdv_selected = session['session_id_pdv_avoir_donnees_edit']
            print("session['session_id_pdv_avoir_donnees_edit'] ", session['session_id_pdv_avoir_donnees_edit'])

            # Récupère la liste des produits qui ne sont pas associés au point de vente sélectionné.
            old_lst_data_pdv_avoir_donnees_non_attribues = session['session_lst_data_pdv_avoir_donnees_non_attribues']
            print("old_lst_data_pdv_avoir_donnees_non_attribues ", old_lst_data_pdv_avoir_donnees_non_attribues)

            # Récupère la liste des produits qui sont associés au point de vente sélectionné.
            old_lst_data_pdv_avoir_donnees_attribues = session['session_lst_data_pdv_avoir_donnees_old_attribues']
            print("old_lst_data_pdv_avoir_donnees_old_attribues ", old_lst_data_pdv_avoir_donnees_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme produits dans le composant "tags-selector-tagselect"
            # dans le fichier "pdv_appart_groupage_modifier_tags_dropbox.html"
            new_lst_str_pdv_avoir_donnees = request.form.getlist('name_select_tags')
            print("new_lst_str_pdv_avoir_donnees ", new_lst_str_pdv_avoir_donnees)

            # On transforme en une liste de valeurs numériques. Exemple: [4,65,2]
            new_lst_int_pdv_avoir_donnees_old = list(map(int, new_lst_str_pdv_avoir_donnees))
            print("new_lst_pdv_avoir_donnees ", new_lst_int_pdv_avoir_donnees_old, "type new_lst_pdv_avoir_donnees ",
                  type(new_lst_int_pdv_avoir_donnees_old))

            # Une liste de "id_produits" qui doivent être effacés de la table intermédiaire "t_pdv_vendre_produits".
            lst_diff_donnees_delete_b = list(
                set(old_lst_data_pdv_avoir_donnees_attribues) - set(new_lst_int_pdv_avoir_donnees_old))
            # DEBUG : Pour afficher le résultat de la liste.
            print("lst_diff_donnees_delete_b ", lst_diff_donnees_delete_b)

            # Une liste de "id_produits" qui doivent être ajoutés à la BD
            lst_diff_donnees_insert_a = list(
                set(new_lst_int_pdv_avoir_donnees_old) - set(old_lst_data_pdv_avoir_donnees_attribues))
            # DEBUG : Pour afficher le résultat de la liste.
            print("lst_diff_pdv_insert_a ", lst_diff_donnees_insert_a)

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionPDVVendreProduits()

            # Pour le point de vente sélectionné, parcourir la liste des produits à INSÉRER dans la "t_pdv_vendre_produits".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_donnees_ins in lst_diff_donnees_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du point de vente sélectionné avec un nom de variable
                # et "id_donnees_ins" (l'id du produit dans la liste) associé à une variable.
                valeurs_pdv_sel_donnees_sel_dictionnaire = {"value_FK_PDV": id_pdv_selected,
                                                           "value_FK_Produits": id_donnees_ins}
                # Insérer une association entre un(des) produit(s) et le point de vente sélectionné.
                obj_actions_produits.pdv_avoir_donnees_add(valeurs_pdv_sel_donnees_sel_dictionnaire)

            # Pour le point de vente sélectionné, parcourir la liste des produits à EFFACER dans la "t_pdv_vendre_produits".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_donnees_del in lst_diff_donnees_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du point de vente sélectionné avec un nom de variable
                # et "id_donnees_del" (l'id du produit dans la liste) associé à une variable.
                valeurs_pdv_sel_donnees_sel_dictionnaire = {"value_FK_PDV": id_pdv_selected,
                                                           "value_FK_Produits": id_donnees_del}
                # Effacer une association entre un(des) produit(s) et le point de vente sélectionné.
                obj_actions_produits.pdv_avoir_donnees_delete(valeurs_pdv_sel_donnees_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe GestionProduits()
            # Fichier data_gestion_produits.py
            # Afficher seulement le point de vente dont les produits sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_pdv_avoir_donnees_afficher_concat = obj_actions_produits.pdv_avoir_donnees_afficher_data_concat(id_pdv_selected)
            # DEBUG : Pour afficher le résultat et son type.
            print(" data produits", data_pdv_avoir_donnees_afficher_concat, "type ", type(data_pdv_avoir_donnees_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_pdv_avoir_donnees_afficher_concat == None:
                flash(f"""Le PDV demandé n'existe pas. Ou la table "t_pdv_vendre_produits" est vide!""", "warning")
            else:
                flash(f"Données Produits affichées dans PDVVendreProduits!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Affichage des points de vente et de leurs données associées
    return render_template("pdv_avoir_donnees/pdv_avoir_donnees_afficher.html",
                           data=data_pdv_avoir_donnees_afficher_concat)
