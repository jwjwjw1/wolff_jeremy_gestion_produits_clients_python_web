# data_gestion_pdv_avoir_donnees.py
# Wolff Jérémy - 2020.06.25 - INFO1A
# Permet de gérer (CRUD) les données de la table intermédiaire "t_pdv_avoir_donnees"
# --------------------------------------------------------------------------------------------------------------------
#

from flask import flash
from PROJET.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from PROJET.DATABASE.erreurs import *


class GestionPDVVendreProduits():
    def __init__(self):
        try:
            # DEBUG : Pour afficher un message dans la console.
            print("dans le try de gestions produits")
            # La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion pdv_avoir_donnees... Il faut connecter une base de donnée!", "danger")
            # DEBUG : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionPDVVendreProduits {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionPDVVendreProduits ")

    def produits_afficher_data(self):
        try:
            # Affiche tous les données des produits de la table "t_produits" et ceci ordonné par id dans l'ordre croissant
            strsql_produits_afficher = """SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits ORDER BY id_produits ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_produits_afficher)
                # Récupère les données de la requête.
                data_produits = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_produits ", data_produits, " Type : ", type(data_produits))
                # Retourne les données du "SELECT"
                return data_produits
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def pdv_avoir_donnees_afficher_data(self, valeur_id_pdv_selected_dict):
        print("valeur_id_pdv_selected_dict...", valeur_id_pdv_selected_dict)
        try:
            # Affiche l'id du point de vente sélectionné, son nom, ainsi que les id des produits y associés
            strsql_pdv_selected = """SELECT id_pdv, NomPDV, GROUP_CONCAT(id_produits) as PDVVendreProduits FROM t_pdv_vendre_produits AS T1
                                        INNER JOIN t_points_de_vente AS T2 ON T2.id_pdv = T1.FK_PDV
                                        INNER JOIN t_produits AS T3 ON T3.id_produits = T1.FK_Produits
                                        WHERE id_pdv = %(value_id_pdv_selected)s"""

            # Affiche toutes les données produits, sauf celles du produit sélectionné
            strsql_pdv_avoir_donnees_non_attribues = """SELECT id_produits, MarqueProduit, ' ',DescriptionProduit, ' ', Pharmacode, ' ',ImageProduit FROM t_produits
                                                    WHERE id_produits not in(SELECT id_produits as idPDVVendreProduits FROM t_pdv_vendre_produits AS T1
                                                    INNER JOIN t_points_de_vente AS T2 ON T2.id_pdv = T1.FK_PDV
                                                    INNER JOIN t_produits AS T3 ON T3.id_produits = T1.FK_Produits
                                                    WHERE id_pdv = %(value_id_pdv_selected)s)"""

            # Affiche les données des produits associés au point de vente sélectionné
            strsql_pdv_avoir_donnees_attribues = """SELECT id_pdv, id_produits, MarqueProduit, ' ',DescriptionProduit, ' ', Pharmacode, ' ',ImageProduit FROM t_pdv_vendre_produits AS T1
                                            INNER JOIN t_points_de_vente AS T2 ON T2.id_pdv = T1.FK_PDV
                                            INNER JOIN t_produits AS T3 ON T3.id_produits = T1.FK_Produits
                                            WHERE id_pdv = %(value_id_pdv_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_pdv_avoir_donnees_non_attribues, valeur_id_pdv_selected_dict)
                # Récupère les données de la requête.
                data_pdv_avoir_donnees_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_pdv_avoir_donnees_non_attribues ", data_pdv_avoir_donnees_non_attribues, " Type : ",
                      type(data_pdv_avoir_donnees_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_pdv_selected, valeur_id_pdv_selected_dict)
                # Récupère les données de la requête.
                data_pdv_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_pdv_selected  ", data_pdv_selected, " Type : ", type(data_pdv_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_pdv_avoir_donnees_attribues, valeur_id_pdv_selected_dict)
                # Récupère les données de la requête.
                data_pdv_avoir_donnees_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_pdv_avoir_donnees_attribues ", data_pdv_avoir_donnees_attribues, " Type : ",
                      type(data_pdv_avoir_donnees_attribues))

                # Retourne les données du "SELECT"
                return data_pdv_selected, data_pdv_avoir_donnees_non_attribues, data_pdv_avoir_donnees_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def pdv_avoir_donnees_afficher_data_concat(self, id_pdv_selected):
        print("id_pdv_selected  ", id_pdv_selected)
        try:
            # Grande requête SQL permettant d'afficher toutes les données
            # (personnes, mail, tél, pot-client, produits, etc.) associées à chaque point de vente
            # (cf. onglet "Point de vente" dans l'application pour le rendu)
            strsql_pdv_avoir_donnees_afficher_data_concat = """SELECT id_pdv, NomPDV, 
            GROUP_CONCAT(DISTINCT ' ',DescriptionProduit, ' (', Pharmacode, ')') AS PDVVendreProduits,
            t_produits.id_produits,
            GROUP_CONCAT(DISTINCT PrenomPersRef,' ', NomPersRef) AS PersTravaillerPDV,
            t_personne_ref.id_pers_ref,
            GROUP_CONCAT(DISTINCT NomGroupage) AS PDVAppartenirGroupage,
            t_groupage.id_groupage,
            GROUP_CONCAT(DISTINCT AttribPotClient) AS PDVAvoirPotentiel,
            t_potentiel_client.id_potentiel_client,
            GROUP_CONCAT(DISTINCT NomMail) AS PersAvoirMail,
            t_mail.id_mail,
            GROUP_CONCAT(DISTINCT NumTel, ' / ', NumFax) AS PersAvoirTel,
            t_num_tel_fax.id_num_tel,
            GROUP_CONCAT(DISTINCT DateVisiteClient) AS PDVAvoirDateVisite,
            t_date_visite_client.id_date_visite_client,
            GROUP_CONCAT(DISTINCT RueNom,' ',NumeroRue,', ',CodePostal,' ',VilleNom,', ',Canton) AS PDVAvoirAdresse,
            t_adresse.id_adresse
            FROM t_points_de_vente 
            LEFT JOIN t_pdv_vendre_produits ON t_points_de_vente.id_pdv = t_pdv_vendre_produits.FK_PDV 
            LEFT JOIN t_produits ON t_pdv_vendre_produits.FK_Produits = t_produits.id_produits 
            LEFT JOIN t_pdv_avoir_adresse ON t_points_de_vente.id_pdv = t_pdv_avoir_adresse.FK_PDV 
            LEFT JOIN t_adresse ON t_pdv_avoir_adresse.FK_Adresse = t_adresse.id_adresse
            LEFT JOIN t_pers_travailler_pdv ON t_points_de_vente.id_pdv = t_pers_travailler_pdv.FK_PDV 
            LEFT JOIN t_personne_ref ON t_pers_travailler_pdv.FK_Pers_Ref = t_personne_ref.id_pers_ref
            LEFT JOIN t_pdv_appart_groupage ON t_points_de_vente.id_pdv = t_pdv_appart_groupage.FK_PDV 
            LEFT JOIN t_groupage ON t_pdv_appart_groupage.FK_Groupage = t_groupage.id_groupage
            LEFT JOIN t_pdv_avoir_potentiel ON t_points_de_vente.id_pdv = t_pdv_avoir_potentiel.FK_PDV 
            LEFT JOIN t_potentiel_client ON t_pdv_avoir_potentiel.FK_Potentiel_Client = t_potentiel_client.id_potentiel_client
            LEFT JOIN t_pers_avoir_mail ON t_personne_ref.id_pers_ref = t_pers_avoir_mail.FK_Pers_Ref 
            LEFT JOIN t_mail ON t_pers_avoir_mail.FK_Mail = t_mail.id_mail
            LEFT JOIN t_pers_avoir_num_tel ON t_personne_ref.id_pers_ref = t_pers_avoir_num_tel.FK_Pers_Ref
            LEFT JOIN t_num_tel_fax ON t_pers_avoir_num_tel.FK_Num_Tel = t_num_tel_fax.id_num_tel
            LEFT JOIN t_pdv_avoir_date_visite ON t_points_de_vente.id_pdv = t_pdv_avoir_date_visite.FK_PDV
            LEFT JOIN t_date_visite_client ON t_pdv_avoir_date_visite.FK_Date_Visite_Client = t_date_visite_client.id_date_visite_client
            GROUP BY id_pdv
            """

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:

                # le paramètre 0 permet d'afficher tous les points de vente
                # Sinon le paramètre représente la valeur de l'id du produit
                if id_pdv_selected == 0:
                    mc_afficher.execute(strsql_pdv_avoir_donnees_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du produit sélectionné avec un nom de variable
                    valeur_id_pdv_selected_dictionnaire = {"value_id_pdv_selected": id_pdv_selected}
                    strsql_pdv_avoir_donnees_afficher_data_concat += """ HAVING id_pdv= %(value_id_pdv_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_pdv_avoir_donnees_afficher_data_concat,
                                        valeur_id_pdv_selected_dictionnaire)

                # Récupère les données de la requête.
                data_pdv_avoir_donnees_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_pdv_avoir_donnees_afficher_concat ", data_pdv_avoir_donnees_afficher_concat,
                      " Type : ",
                      type(data_pdv_avoir_donnees_afficher_concat))

                # Retourne les données du "SELECT"
                return data_pdv_avoir_donnees_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def pdv_avoir_donnees_add(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # Insérer une (des) nouvelle(s) association(s) entre "id_produits" et "id_pdv" dans la "t_pdv_vendre_produits"
            strsql_insert_pdv_vendre_produits = """INSERT INTO t_pdv_vendre_produits (id_pdv_vendre_produits, FK_Produits, FK_PDV)
                                            VALUES (NULL, %(value_FK_Produits)s, %(value_FK_PDV)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_pdv_vendre_produits, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def pdv_avoir_donnees_delete(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # Effacer une (des) association(s) existantes entre "id_produits" et "id_pdv" dans la "t_pdv_vendre_produits"
            strsql_delete_pdv_avoir_donnees = """DELETE FROM t_pdv_vendre_produits WHERE FK_Produits = %(value_FK_Produits)s AND FK_PDV = %(value_FK_PDV)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_pdv_avoir_donnees, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème pdv_avoir_donnees_delete Gestions PDV_avoir_Donnees numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème pdv_avoir_donnees_delete Gestions PDV_avoir_Donnees numéro de l'erreur : {erreur}",
                  "danger")
            raise Exception(
                "Raise exception... Problème pdv_avoir_donnees_delete Gestions PDV_avoir_Donnees {erreur}")

    def edit_donnees_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Commande MySql pour afficher le PDV sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_produits = "SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode FROM t_produits WHERE id_produits = %(value_id_produits)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_produits, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_pdv_data d'un PDV Data Gestions PDV {erreur}")

    def update_donnees_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur SAISIE dans le champ "nameEditNomPDVHTML" du form HTML "PDVEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_MarqueProduit = "UPDATE t_produits SET MarqueProduit = %(value_name_MarqueProduit)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_DescriptionProduit = "UPDATE t_produits SET DescriptionProduit = %(value_name_DescriptionProduit)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_Pharmacode = "UPDATE t_produits SET Pharmacode = %(value_name_Pharmacode)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_ImageProduit = "UPDATE t_produits SET ImageProduit = %(value_name_ImageProduit)s WHERE id_produits = %(value_id_produits)s"


            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_MarqueProduit, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_DescriptionProduit, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_Pharmacode, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_ImageProduit, valeur_update_dictionnaire)


        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_donnees_data d\'un produit Data Gestions Produits {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_pdv_data d'un PDV DataGestionsPDV {erreur}")

    def delete_select_donnees_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur SAISIE dans le champ "nameEditNomPDVHTML" du form HTML "PDVEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            # Commande MySql pour afficher le produit sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_produits = "SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits WHERE id_produits = %(value_id_produits)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_produits, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_select_pdv_data Gestions PDV numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_pdv_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_pdv_data d\'un PDV Data Gestions PDV {erreur}")

    def delete_donnees_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "PDVEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_produit = "DELETE FROM t_produits WHERE id_produits = %(value_id_produits)s"

            # Commit après insertion des données
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_produit, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Erreur affichée en cas de contrainte, données associées à une table intermédiaire
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(
                    f"Flash. IMPOSSIBLE d'effacer! Ce point de vente est associé à des produits dans la t_pdv_vendre_produits! : {erreur}",
                    "danger")
                # DEBUG : Pour afficher un message dans la console.
                print(
                    f"IMPOSSIBLE d'effacer! Ce point de vente est associé à des produits dans la t_pdv_vendre_produits! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")
