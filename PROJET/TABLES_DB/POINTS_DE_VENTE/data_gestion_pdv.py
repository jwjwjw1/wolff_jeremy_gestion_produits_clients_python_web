# data_gestion_pdv.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Permet de gérer (CRUD) les données de la table t_points_de_vente
# --------------------------------------------------------------------------------------------------------------------
#

from flask import flash

from PROJET.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from PROJET.DATABASE.erreurs import *


class GestionPDV():
    def __init__(self):
        try:
            # DEBUG : Pour afficher un message dans la console.
            print("dans le try de gestions PDV")
            # La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans GestionPDV: Erreur, il faut connecter une base de donnée!", "danger")
            # DEBUG : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionPDV {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionPDV ")

    def pdv_afficher_data(self, valeur_order_by, id_pdv_sel):
        try:
            print("valeur_order_by ", valeur_order_by, type(valeur_order_by))

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Afficher des points de vente
                if valeur_order_by == "ASC" and id_pdv_sel == 0:
                    strsql_pdv_afficher = """SELECT id_pdv, NomPDV FROM t_points_de_vente ORDER BY id_pdv ASC"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_pdv_afficher)
                elif valeur_order_by == "ASC":
                    valeur_id_pdv_selected_dictionnaire = {"value_id_pdv_selected": id_pdv_sel}
                    strsql_pdv_afficher = """SELECT id_pdv, NomPDV FROM t_points_de_vente  WHERE id_pdv = %(value_id_pdv_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_pdv_afficher, valeur_id_pdv_selected_dictionnaire)
                else:
                    strsql_pdv_afficher = """SELECT id_pdv, NomPDV FROM t_points_de_vente ORDER BY id_pdv DESC"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_pdv_afficher)

                # Récupère les données de la requête.
                data_pdv = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_pdv ", data_pdv, " Type : ", type(data_pdv))
                # Retourne les données du "SELECT"
                return data_pdv
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise  MaBdErreurPyMySl(f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # Dérivation "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_pdv_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            strsql_insert_pdv = """INSERT INTO t_points_de_vente (id_pdv,NomPDV) VALUES (NULL,%(value_NomPDV)s)"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_pdv, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")


    def edit_pdv_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Commande MySql pour afficher le point de vente sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_pdv = "SELECT id_pdv, NomPDV FROM t_points_de_vente WHERE id_pdv = %(value_id_pdv)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_pdv, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_pdv_data d'un PDV Data Gestions PDV {erreur}")

    def update_pdv_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntitulePDVHTML" du form HTML "pdv_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntitulePDVHTML" value="{{ row.NomPDV }}"/></td>
            str_sql_update_PDV = "UPDATE t_points_de_vente SET NomPDV = %(value_name_pdv)s WHERE id_pdv = %(value_id_pdv)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_PDV, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_pdv_data d\'un PDV Data Gestions PDV {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon! Introduire une valeur différente!')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_pdv_data d'un PDV DataGestionsPDV {erreur}")

    def delete_select_pdv_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntitulePDVHTML" du form HTML "pdv_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntitulePDVHTML" value="{{ row.intitule_pdv }}"/></td>

            # Commande MySql pour afficher le PDV sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_pdv = "SELECT id_pdv, NomPDV FROM t_points_de_vente WHERE id_pdv = %(value_id_pdv)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_pdv, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_select_pdv_data Gestions PDV numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_pdv_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_pdv_data d\'un PDV Data Gestions PDV {erreur}")


    def delete_pdv_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "pdv_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            str_sql_delete_PDV = "DELETE FROM t_points_de_vente WHERE id_pdv = %(value_id_pdv)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_PDV, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_pdv_data Data Gestions PDV numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions PDV numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un PDV qui est associé à un autre valeur dans une table intermédiaire
                # il y a une contrainte sur les FK d'une table intermédiaire
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                # DEBUG: Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Ce point de vente est associé à une valeur dans une table intermédiaire! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")