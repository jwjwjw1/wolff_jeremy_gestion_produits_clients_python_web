# routes_gestion_pdv.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Gestions des "routes" FLASK pour les points de vente
# ---------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.POINTS_DE_VENTE.data_gestion_pdv import GestionPDV
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/pdv_afficher
# ---------------------------------------------------------------------------------------------------
@app.route("/pdv_afficher//<string:order_by>/<int:id_pdv_sel>", methods=['GET', 'POST'])
def pdv_afficher(order_by,id_pdv_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de données par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pdv = GestionPDV()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionPDV().
            # Fichier data_gestion_pdv.py
            data_pdv = obj_actions_pdv.pdv_afficher_data(order_by, id_pdv_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data PDV", data_pdv, "type ", type(data_pdv))

            # Différencier les messages si la table est vide.
            if not data_pdv and id_pdv_sel == 0:
                flash("""La table "t_points_de_vente" est vide!""", "warning")
            elif not data_pdv and id_pdv_sel > 0:
                # Si l'utilisateur change l'id_pdv dans l'URL et que le point de vente n'existe pas,
                flash(f"Le point de vente demandé n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_points_de_vente" est vide.
                # La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données des points de vente affichées !", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("points_de_vente/pdv_afficher.html", data=data_pdv)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "pdv_add.html"
# Pour la tester http://127.0.0.1:1234/pdv_add
# ---------------------------------------------------------------------------------------------------
@app.route("/pdv_add", methods=['GET', 'POST'])
def pdv_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de données par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pdv = GestionPDV()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "pdv_add.html"
            name_pdv = request.form['name_pdv_html']

            # On ne doit pas accepter des valeurs vides ou
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$",
                                name_pdv):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.
                return render_template("points_de_vente/pdv_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_NomPDV": name_pdv}
                obj_actions_pdv.add_pdv_data(valeurs_insertion_dictionnaire)

                # Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'pdv_afficher', car l'utilisateur
                # doit voir le nouveau point de vente qu'il vient d'insérer.
                return redirect(url_for('pdv_afficher', order_by= 'DESC', id_pdv_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py".
            # Nous pouvons alors avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # Dérivation de "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Nous pouvons alors avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoie la page "HTML" au serveur.
    return render_template("points_de_vente/pdv_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un point de vente par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/pdv_edit', methods=['POST', 'GET'])
def pdv_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "pdv_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "id_pdv" du formulaire html "pdv_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_pdv"
            # grâce à la variable "id_pdv_edit_html"
            # <a href="{{ url_for('pdv_edit', id_pdv_edit_html=row.id_pdv) }}">Edit</a>
            id_pdv_edit = request.values['id_pdv_edit_html']

            # Pour afficher dans la console la valeur de "id_pdv_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(id_pdv_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_pdv": id_pdv_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pdv = GestionPDV()

            # La commande MySql est envoyée à la base de données
            data_id_pdv = obj_actions_pdv.edit_pdv_data(valeur_select_dictionnaire)
            print("dataIdPDV ", data_id_pdv, "type ", type(data_id_pdv))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer un point de vente!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # Dérivation de "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Nous pouvons alors avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("points_de_vente/pdv_edit.html", data=data_id_pdv)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un point de vente par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/pdv_update', methods=['POST', 'GET'])
def pdv_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "pdv_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur d'un point de vente alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            # Récupérer la valeur de "id_pdv" du formulaire html "pdv_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "id_pdv"
            # grâce à la variable "id_pdv_edit_html"
            # <a href="{{ url_for('pdv_edit', id_pdv_edit_html=row.id_pdv) }}">Edit</a>
            id_pdv_edit = request.values['id_pdv_edit_html']

            # Récupère le contenu du champ "NomPDV" dans le formulaire HTML "pdv_edit.html"
            name_pdv = request.values['name_edit_pdv_html']
            valeur_edit_list_pdv = [{'id_pdv': id_pdv_edit, 'NomPDV': name_pdv}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$", name_pdv):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "NomPDV" dans le formulaire HTML "pdv_edit.html"
                #name_pdv = request.values['name_edit_NomPDV_html']
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # On doit afficher à nouveau le formulaire "pdv_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "pdv_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'id_pdv': 13, 'NomPDV': 'Pharmacie X'}]
                valeur_edit_list_pdv = [{'id_pdv': id_pdv_edit, 'NomPDV': name_pdv}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "pdv_edit.html"
                print(valeur_edit_list_pdv, "type ..",  type(valeur_edit_list_pdv))
                return render_template('points_de_vente/pdv_edit.html', data=valeur_edit_list_pdv)
            else:
                # Constitution d'un dictionnaire et insertion dans la base de données
                valeur_update_dictionnaire = {"value_id_pdv": id_pdv_edit, "value_name_pdv": name_pdv}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_pdv = GestionPDV()

                # La commande MySql est envoyée à la base de données
                data_id_pdv = obj_actions_pdv.update_pdv_data(valeur_update_dictionnaire)
                # DEBUG  :
                print("dataIdPDV ", data_id_pdv, "type ", type(data_id_pdv))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Point de vente édité!", "success")
                # On affiche les points de vente
                return redirect(url_for('pdv_afficher', order_by="ASC", id_pdv_sel=id_pdv_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème PDV update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_NomPDV_html" alors on renvoie le formulaire "EDIT"
            return render_template('points_de_vente/pdv_edit.html', data=valeur_edit_list_pdv)

    return render_template("points_de_vente/pdv_edit.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pdv_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un point de vente par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/pdv_select_delete', methods=['POST', 'GET'])
def pdv_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pdv = GestionPDV()
            # Récupérer la valeur de "idPDVDeleteHTML" du formulaire html "pdv_delete.html"
            id_pdv_delete = request.args.get('id_pdv_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_pdv": id_pdv_delete}


            # La commande MySql est envoyée à la base de données
            data_id_pdv = obj_actions_pdv.delete_select_pdv_data(valeur_delete_dictionnaire)
            flash(f"Supprimer un point de vente!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur pdv_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur pdv_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('points_de_vente/pdv_delete.html', data = data_id_pdv)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /PDVUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un point de vente, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/pdv_delete', methods=['POST', 'GET'])
def pdv_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pdv = GestionPDV()
            # Récupérer la valeur de "id_pdv" du formulaire html "pdv_afficher.html"
            id_pdv_delete = request.form['id_pdv_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_pdv": id_pdv_delete}

            data_pdv = obj_actions_pdv.delete_pdv_data(valeur_delete_dictionnaire)
            # Affichage de la liste des points de vente
            # Envoi de la page "HTML" au serveur. Passage d'un message d'information dans "message_html"

            # Affichage des points de vente
            return redirect(url_for('pdv_afficher', order_by="ASC", id_pdv_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer un "point de vente" qui est associé dans "t_pdv_avoir_adresse"
            # ou autre table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une (des) autre(s) table(s).', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer !! Ce point de vente est associé à d'autres valeurs dans des tables intermédiaires! : {erreur}")
                # Affichage de la liste des points de vente
                return redirect(url_for('pdv_afficher', order_by="ASC", id_pdv_sel=0))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur pdv_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur pdv_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('points_de_vente/pdv_afficher.html', data=data_pdv)