# routes_gestion_produits.py
# Wolff Jérémy - 2020.05.10 - INFO1A
# Gestions des "routes" FLASK pour les produits
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.PRODUITS.data_gestion_produits import GestionProduits
from PROJET.DATABASE.erreurs import *
# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re
import urllib


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produits_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/produits_afficher/<string:order_by>/<int:id_produits_sel>", methods=['GET', 'POST'])
def produits_afficher(order_by,id_produits_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionProduits()
            # Fichier data_gestion_pdv.py
            data_produits = obj_actions_produits.produits_afficher_data(order_by,id_produits_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data produits", data_produits, "type ", type(data_produits))

            # Différencier les messages si la table est vide.
            if not data_produits and id_produits_sel == 0:
                flash("""La table "t_produits" est vide!""", "warning")
            elif not data_produits and id_produits_sel > 0:
                # Si l'utilisateur change l'id_produits dans l'URL et que le produit n'existe pas,
                flash(f"Le produit demandé n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_produits" est vide.
                # La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données des produits affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("produits/produits_afficher.html", data=data_produits)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produits_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/produits_add", methods=['GET', 'POST'])
def produits_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            MarqueProduit = request.form['MarqueProduit_html']
            DescriptionProduit = request.form['DescriptionProduit_html']
            Pharmacode = request.form['Pharmacode_html']
            ImageProduit = request.form['ImageProduit_html']


            # Regex partiellement utilisée. Si l'application requiert d'une amélioration de cette regex, j'y procéderai
            # pour l'instant le résultat convient bien
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$",
                                DescriptionProduit):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "produits_add.html" à cause des erreurs relatives à la regex.
                return render_template("produits/produits_add.html")


            elif not re.match("http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+",
                              ImageProduit):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte !", "danger")
                # Réaffichage du formulaire "produits_add.html" à cause des erreurs relatives à la regex.
                return render_template("produits/produits_add.html")
            else:
                # https://www.alpecin.com/fileadmin/user_upload/alpecin.com/images/products/DE/alpecin-packshot-tuning-cream-germany-de.png
                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_MarqueProduit": MarqueProduit,
                                                  "value_DescriptionProduit": DescriptionProduit,
                                                  "value_Pharmacode": Pharmacode,
                                                  "value_ImageProduit": ImageProduit}

                obj_actions_produits.add_produits_data(valeurs_insertion_dictionnaire)

            # Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Données insérées!", "success")
            print(f"Données insérées!")
            # On va interpréter la "route" 'produits_afficher', car l'utilisateur
            # doit voir le nouveau produit qu'il vient d'insérer.
            return redirect(url_for('produits_afficher', order_by='DESC', id_produits_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                # pymysql.ProgrammingError,
                # pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        # except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("produits/produits_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produits_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une produits par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/produits_edit', methods=['POST', 'GET'])
def produits_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "produits_afficher.html"
    if request.method == 'GET':
        try:
            id_produits_edit = request.values['id_produits_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_produits_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_produits": id_produits_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()

            # La commande MySql est envoyée à la BD
            data_id_produits = obj_actions_produits.edit_produits_data(valeur_select_dictionnaire)
            print("dataIdProduits ", data_id_produits, "type ", type(data_id_produits))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer un produit!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("produits/produits_edit.html", data=data_id_produits)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produits_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une produits par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/produits_update', methods=['POST', 'GET'])
def produits_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_produits_edit = request.values['id_produits_edit_html']

            # Récupère les contenus des champs de produits dans le formulaire HTML "produits_edit.html"
            MarqueProduit = request.values['name_edit_MarqueProduit_html']
            DescriptionProduit = request.values['name_edit_DescriptionProduit_html']
            Pharmacode = request.values['name_edit_Pharmacode_html']
            ImageProduit = request.values['name_edit_ImageProduit_html']

            valeur_edit_list = [{'id_produits': id_produits_edit,
                                 'MarqueProduit': MarqueProduit,
                                 'DescriptionProduit': DescriptionProduit,
                                 'Pharmacode': Pharmacode,
                                 'ImageProduit': ImageProduit}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$", DescriptionProduit):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des produitss
                valeur_edit_list = [{'id_produits': id_produits_edit,
                                     'MarqueProduit': MarqueProduit,
                                     'DescriptionProduit': DescriptionProduit,
                                     'Pharmacode': Pharmacode,
                                     'ImageProduit': ImageProduit}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "produits_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('produits/produits_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_produits": id_produits_edit,
                                              "value_MarqueProduit": MarqueProduit,
                                              "value_DescriptionProduit": DescriptionProduit,
                                              "value_Pharmacode": Pharmacode,
                                              "value_ImageProduit": ImageProduit}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_produits = GestionProduits()

                # La commande MySql est envoyée à la base de données
                data_id_produits = obj_actions_produits.update_produits_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIdProduits ", data_id_produits, "type ", type(data_id_produits))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Produit édité avec succès!", "success")
                # Affichage des produitss
                return redirect(url_for('produits_afficher', order_by="ASC", id_produits_sel=id_produits_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème produits update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une produits, on renvoie le formulaire "EDIT"

        return render_template('produits/produits_edit.html', data=valeur_edit_list)




# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produits_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une produits par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/produits_select_delete', methods=['POST', 'GET'])
def produits_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()
            # Récupérer la valeur de "idproduitsDeleteHTML" du formulaire html "produits_delete.html"
            id_produits_delete = request.args.get('id_produits_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_produits": id_produits_delete}


            # La commande MySql est envoyée à la base de données
            data_id_produits = obj_actions_produits.delete_select_produits_data(valeur_delete_dictionnaire)
            flash(f"Effacer un produit!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur produits_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur produits_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('produits/produits_delete.html', data = data_id_produits)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /produitsUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une produits, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/produits_delete', methods=['POST', 'GET'])
def produits_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_produits = GestionProduits()
            # Récupérer la valeur de "id_produits" du formulaire html "ProduitsAfficher.html"
            id_produits_delete = request.form['id_produits_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_produits": id_produits_delete}

            data_produits = obj_actions_produits.delete_produits_data(valeur_delete_dictionnaire)
            # Affichage de la liste des produitss
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les produitss
            return redirect(url_for('produits_afficher', order_by="ASC", id_produits_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer une produits qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des produitss
                return redirect(url_for('produits_afficher', order_by="ASC", id_produits_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur produits_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur produits_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('produits/produits_afficher.html', data=data_produits)