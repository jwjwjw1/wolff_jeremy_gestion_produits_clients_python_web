# data_gestion_produits.py
# Wolff Jérémy - 2020.05.10 - INFO1A
# Permet de gérer (CRUD) les données de la table t_produits
# --------------------------------------------------------------------------------------------------------------------
#

from flask import flash

from PROJET.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from PROJET.DATABASE.erreurs import *

class GestionProduits():
    def __init__(self):
        try:
            # DEBUG : Pour afficher un message dans la console.
            print("dans le try de gestions produits")
            # La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans GestionProduits: Erreur, il faut connecter une base de donnée!", "Danger")
            # DEBUG : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionProduits {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionProduits ")

    def produits_afficher_data(self, valeur_order_by, id_produits_sel):
        try:
            print("valeur_order_by ", valeur_order_by, type(valeur_order_by))

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if valeur_order_by == "ASC" and id_produits_sel == 0:
                    # Envoi de la commande MySql
                    strsql_produits_afficher = """SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits ORDER BY id_produits ASC"""
                    mc_afficher.execute(strsql_produits_afficher)
                elif valeur_order_by == "ASC":
                    valeur_id_produits_selected_dictionnaire = {"value_id_produits_selected": id_produits_sel}
                    strsql_produits_afficher = """SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits  WHERE id_produits = %(value_id_produits_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_produits_afficher, valeur_id_produits_selected_dictionnaire)
                else:
                    strsql_produits_afficher = """SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits ORDER BY id_produits DESC"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_produits_afficher)
                # Récupère les données de la requête.
                data_produits = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_produits ", data_produits, " Type : ", type(data_produits))
                # Retourne les données du "SELECT"
                return data_produits
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise  MaBdErreurPyMySl(f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_produits_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)

            strsql_insert_produits = """INSERT INTO t_produits
            (id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit)
            VALUES (NULL,%(value_MarqueProduit)s, %(value_DescriptionProduit)s, %(value_Pharmacode)s, %(value_ImageProduit)s)"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_produits, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")


    def edit_produits_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Commande MySql pour afficher le produit sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_produits = "SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit FROM t_produits WHERE id_produits = %(value_id_produits)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_produits, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_produits_data Data Gestions Produits numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Produits numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_produits_data d'un produit Data Gestions Produits {erreur}")

    def update_produits_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleProduitsHTML" du form HTML "produits_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_MarqueProduit = "UPDATE t_produits SET MarqueProduit = %(value_MarqueProduit)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_DescriptionProduit = "UPDATE t_produits SET DescriptionProduit = %(value_DescriptionProduit)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_Pharmacode = "UPDATE t_produits SET Pharmacode = %(value_Pharmacode)s WHERE id_produits = %(value_id_produits)s"
            str_sql_update_ImageProduit = "UPDATE t_produits SET ImageProduit = %(value_ImageProduit)s WHERE id_produits = %(value_id_produits)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_MarqueProduit, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_DescriptionProduit, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_Pharmacode, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_ImageProduit, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_produits_data Data Gestions Produits numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Produits numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_produits_data d\'un produit Data Gestions Produits {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon! Introduire une valeur différente!')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_produits_data Data Gestions Produits numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_produits_data d'une aderesse DataGestionsProduits {erreur}")

    def delete_select_produits_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleProduitsHTML" du form HTML "produits_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            # Commande MySql pour afficher le produit sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_produits = "SELECT id_produits, MarqueProduit, DescriptionProduit, Pharmacode, ImageProduit " \
                                         "FROM t_produits WHERE id_produits = %(value_id_produits)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_produits, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_select_produits_data Gestions Produits numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_produits_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_produits_data d\'un produit Data Gestions Produits {erreur}")


    def delete_produits_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "produits_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_produits = "DELETE FROM t_produits WHERE id_produits = %(value_id_produits)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_produits, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_produits_data Data Gestions Produits numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Produits numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un produit qui est associé à une autre valeur dans une table intermédiaire
                # il y a une contrainte sur des FK de tables intermédiaires
                # flash(f"Flash. Impossible d'effacer! Cet produit est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}", "danger")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Ce produit est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")