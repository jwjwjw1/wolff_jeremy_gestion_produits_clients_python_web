# routes_gestion_mail.py
# Wolff Jérémy - 2020.06.19 - INFO1A
# Gestions des "routes" FLASK pour les mails
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.MAIL.data_gestion_mail import GestionMail
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mail_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/mail_afficher//<string:order_by>/<int:id_mail_sel>", methods=['GET', 'POST'])
def mail_afficher(order_by, id_mail_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = GestionMail()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionMail()
            # Fichier data_gestion_pdv.py
            data_mail = obj_actions_mail.mail_afficher_data(order_by, id_mail_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data mail", data_mail, "type ", type(data_mail))

            # Différencier les messages si la table est vide.
            if not data_mail and id_mail_sel == 0:
                flash("""La table "t_mail" est vide!""", "warning")
            elif not data_mail and id_mail_sel > 0:
                # Si l'utilisateur change l'id_mail dans l'URL et que le mail n'existe pas,
                flash(f"Le mail demandé n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_mail" est vide.
                # La ligne ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données des adresses e-mail affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("mail/mail_afficher.html", data=data_mail)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mail_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/mail_add", methods=['GET', 'POST'])
def mail_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = GestionMail()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            NomMail = request.form['NomMail_html']

            # Regex pour la validation des mails
            if not re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)",
                                NomMail):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Veuillez saisir un format d'adresse e-mail valide!",
                      f"danger")
                # Réaffichage du formulaire "mail_add.html" à cause des erreurs relatives à la regex.
                return render_template("mail/mail_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_NomMail": NomMail}

                obj_actions_mail.add_mail_data(valeurs_insertion_dictionnaire)

                # Message indiquant à l'utilisateur que les données ont été insérées
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'mail_afficher', car l'utilisateur
                # doit voir la nouvelle adressse qu'il vient d'insérer.
                return redirect(url_for('mail_afficher', order_by='DESC', id_mail_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("mail/mail_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mail_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une mail par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mail_edit', methods=['POST', 'GET'])
def mail_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "mail_afficher.html"
    if request.method == 'GET':
        try:
            id_mail_edit = request.values['id_mail_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_mail_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_mail": id_mail_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = GestionMail()

            # La commande MySql est envoyée à la BD
            data_id_mail = obj_actions_mail.edit_mail_data(valeur_select_dictionnaire)
            print("dataIdmail ", data_id_mail, "type ", type(data_id_mail))
            # Message indiquant aux utilisateurs que le mail a bien été édité.
            flash(f"Editer une adresse e-mail!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("mail/mail_edit.html", data=data_id_mail)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mail_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une mail par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mail_update', methods=['POST', 'GET'])
def mail_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_mail_edit = request.values['id_mail_edit_html']

            # Récupère les contenus des champs de mail dans le formulaire HTML "mail_edit.html"
            NomMail = request.values['name_edit_mail_html']

            valeur_edit_list = [{'id_mail': id_mail_edit,
                                 'NomMail': NomMail}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)",
                            NomMail):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Veuillez saisir une adresse e-mail de format valide!",
                      f"danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des mail
                valeur_edit_list = [{'id_mail': id_mail_edit,
                                     'NomMail': NomMail}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "mail_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('mail/mail_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_mail": id_mail_edit,
                                              "value_NomMail": NomMail}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_mail = GestionMail()

                # La commande MySql est envoyée à la base de données
                data_id_mail = obj_actions_mail.update_mail_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIdmail ", data_id_mail, "type ", type(data_id_mail))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Adresse e-mail éditée!", "success")
                # Affichage des mail
                return redirect(url_for('mail_afficher', order_by="ASC", id_mail_sel=id_mail_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"Problème mail update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une mail, on renvoie le formulaire "EDIT"
            return render_template('mail/mail_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mail_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une mail par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/mail_select_delete', methods=['POST', 'GET'])
def mail_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = GestionMail()
            # Récupérer la valeur de "idmailDeleteHTML" du formulaire html "mail_delete.html"
            id_mail_delete = request.args.get('id_mail_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}


            # La commande MySql est envoyée à la base de données
            data_id_mail = obj_actions_mail.delete_select_mail_data(valeur_delete_dictionnaire)
            flash(f"Effacer une adresse e-mail!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('mail/mail_delete.html', data = data_id_mail)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /mailUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une mail, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/mail_delete', methods=['POST', 'GET'])
def mail_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_mail = GestionMail()
            # Récupérer la valeur de "id_mail" du formulaire html "mailAfficher.html"
            id_mail_delete = request.form['id_mail_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_mail": id_mail_delete}

            data_mail = obj_actions_mail.delete_mail_data(valeur_delete_dictionnaire)
            # Affichage de la liste des mail
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les mail
            return redirect(url_for('mail_afficher', order_by="ASC", id_mail_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer une adresse e-mail qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des mail
                return redirect(url_for('mail_afficher', order_by="ASC", id_mail_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur mail_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('mail/mail_afficher.html', data=data_mail)