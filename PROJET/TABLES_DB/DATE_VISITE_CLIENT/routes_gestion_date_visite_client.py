# routes_gestion_date_visite_client.py
# Wolff Jérémy - 2020.06.19 - INFO1A
# Gestions des "routes" FLASK pour les dates de visite dans les points de vente
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.DATE_VISITE_CLIENT.data_gestion_date_visite_client import GestionDateVisiteClient
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_afficher_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/date_visite_client_afficher//<string:order_by>/<int:id_date_visite_client_sel>", methods=['GET', 'POST'])
def date_visite_client_afficher(order_by, id_date_visite_client_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_date_visite_client = GestionDateVisiteClient()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionDateVisiteClient()
            # Fichier data_gestion_pdv.py
            data_date_visite_client = obj_actions_date_visite_client.date_visite_client_afficher_data(order_by, id_date_visite_client_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data date_visite_client", data_date_visite_client, "type ", type(data_date_visite_client))

            # Différencier les messages si la table est vide.
            if not data_date_visite_client and id_date_visite_client_sel == 0:
                flash("""La table "t_date_visite_client" est vide!""", "warning")
            elif not data_date_visite_client and id_date_visite_client_sel > 0:
                # Si l'utilisateur change l'id_date_visite_client dans l'URL et que le date_visite_client n'existe pas,
                flash(f"Le date de visite demandée n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_date_visite_client" est vide.
                # La ligne ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données des dates de visite affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("date_visite_client/date_visite_client_afficher.html", data=data_date_visite_client)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_client_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/date_visite_client_add", methods=['GET', 'POST'])
def date_visite_client_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_date_visite_client = GestionDateVisiteClient()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            DateVisiteClient = request.form['DateVisiteClient_html']

            # Regex pour la validation des date_visite_clients
            if not re.match("([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))",
                                DateVisiteClient):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Veuillez saisir un format de date tel que celui indiqué ci-dessous!",
                      f"danger")
                # Réaffichage du formulaire "date_visite_client_add.html" à cause des erreurs relatives à la regex.
                return render_template("date_visite_client/date_visite_client_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_DateVisiteClient": DateVisiteClient}

                obj_actions_date_visite_client.add_date_visite_client_data(valeurs_insertion_dictionnaire)

                # Message indiquant à l'utilisateur que les données ont été insérées
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'date_visite_client_afficher', car l'utilisateur
                # doit voir la nouvelle adressse qu'il vient d'insérer.
                return redirect(url_for('date_visite_client_afficher', order_by='DESC', id_date_visite_client_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("date_visite_client/date_visite_client_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_client_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une date_visite_client par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/date_visite_client_edit', methods=['POST', 'GET'])
def date_visite_client_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "date_visite_client_afficher.html"
    if request.method == 'GET':
        try:
            id_date_visite_client_edit = request.values['id_date_visite_client_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_date_visite_client_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_date_visite_client": id_date_visite_client_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_date_visite_client = GestionDateVisiteClient()

            # La commande MySql est envoyée à la BD
            data_id_date_visite_client = obj_actions_date_visite_client.edit_date_visite_client_data(valeur_select_dictionnaire)
            print("dataIddate_visite_client ", data_id_date_visite_client, "type ", type(data_id_date_visite_client))
            # Message indiquant aux utilisateurs que le date_visite_client a bien été édité.
            flash(f"Editer une date de visite!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("date_visite_client/date_visite_client_edit.html", data=data_id_date_visite_client)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_client_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une date_visite_client par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/date_visite_client_update', methods=['POST', 'GET'])
def date_visite_client_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_date_visite_client_edit = request.values['id_date_visite_client_edit_html']

            # Récupère les contenus des champs de date_visite_client dans le formulaire HTML "date_visite_client_edit.html"
            DateVisiteClient = request.values['name_edit_date_visite_client_html']

            valeur_edit_list = [{'id_date_visite_client': id_date_visite_client_edit,
                                 'DateVisiteClient': DateVisiteClient}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))",
                            DateVisiteClient):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Veuillez saisir une date au format JJ-MM-AAAA!",
                      f"danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des date_visite_client
                valeur_edit_list = [{'id_date_visite_client': id_date_visite_client_edit,
                                     'DateVisiteClient': DateVisiteClient}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "date_visite_client_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('date_visite_client/date_visite_client_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_date_visite_client": id_date_visite_client_edit,
                                              "value_DateVisiteClient": DateVisiteClient}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_date_visite_client = GestionDateVisiteClient()

                # La commande MySql est envoyée à la base de données
                data_id_date_visite_client = obj_actions_date_visite_client.update_date_visite_client_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIddate_visite_client ", data_id_date_visite_client, "type ", type(data_id_date_visite_client))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Date de visite éditée!", "success")
                # Affichage des date_visite_client
                return redirect(url_for('date_visite_client_afficher', order_by="ASC", id_date_visite_client_sel=id_date_visite_client_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"Problème de mise à jour de la date de visite{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une date_visite_client, on renvoie le formulaire "EDIT"
            return render_template('date_visite_client/date_visite_client_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_client_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une date_visite_client par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/date_visite_client_select_delete', methods=['POST', 'GET'])
def date_visite_client_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_date_visite_client = GestionDateVisiteClient()
            # Récupérer la valeur de "iddate_visite_clientDeleteHTML" du formulaire html "date_visite_client_delete.html"
            id_date_visite_client_delete = request.args.get('id_date_visite_client_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_date_visite_client": id_date_visite_client_delete}


            # La commande MySql est envoyée à la base de données
            data_id_date_visite_client = obj_actions_date_visite_client.delete_select_date_visite_client_data(valeur_delete_dictionnaire)
            flash(f"Effacer une date de visite!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur de suppression de la date de visite {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur de suppression de la date de visite {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('date_visite_client/date_visite_client_delete.html', data = data_id_date_visite_client)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /date_visite_clientUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une date_visite_client, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/date_visite_client_delete', methods=['POST', 'GET'])
def date_visite_client_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_date_visite_client = GestionDateVisiteClient()
            # Récupérer la valeur de "id_date_visite_client" du formulaire html "date_visite_clientAfficher.html"
            id_date_visite_client_delete = request.form['id_date_visite_client_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_date_visite_client": id_date_visite_client_delete}

            data_date_visite_client = obj_actions_date_visite_client.delete_date_visite_client_data(valeur_delete_dictionnaire)
            # Affichage de la liste des date_visite_client
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les date_visite_client
            return redirect(url_for('date_visite_client_afficher', order_by="ASC", id_date_visite_client_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer une date de visite qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des date_visite_client
                return redirect(url_for('date_visite_client_afficher', order_by="ASC", id_date_visite_client_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur de suppression de la date de visite {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur de suppression de la date de visite {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('date_visite_client/date_visite_client_afficher.html', data=data_date_visite_client)