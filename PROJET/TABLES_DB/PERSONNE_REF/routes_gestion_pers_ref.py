# routes_gestion_pers_ref.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Gestions des "routes" FLASK pour les personnes de référence des points de vente
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.PERSONNE_REF.data_gestion_pers_ref import GestionPersRef
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /personne_ref_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/personne_ref_afficher//<string:order_by>/<int:id_pers_ref_sel>", methods=['GET', 'POST'])
def personne_ref_afficher(order_by, id_pers_ref_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pers_ref = GestionPersRef()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionPersRef()
            # Fichier data_gestion_pdv.py
            data_pers_ref = obj_actions_pers_ref.personne_ref_afficher_data(order_by, id_pers_ref_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data pers_ref", data_pers_ref, "type ", type(data_pers_ref))

            # Différencier les messages si la table est vide.
            if not data_pers_ref and id_pers_ref_sel == 0:
                flash("""La table "t_personne_ref" est vide!""", "warning")
            elif not data_pers_ref and id_pers_ref_sel > 0:
                # Si l'utilisateur change l'id_pers_ref dans l'URL et que la personne n'existe pas,
                flash(f"La personne de référence demandée n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_personne_ref" est vide.
                # La ligne ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données des personnes de référence affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("personne_ref/personne_ref_afficher.html", data=data_pers_ref)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /personne_ref_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/personne_ref_add", methods=['GET', 'POST'])
def personne_ref_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pers_ref = GestionPersRef()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            PrenomPersRef = request.form['PrenomPersRef_html']
            NomPersRef = request.form['NomPersRef_html']

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$",
                            PrenomPersRef):
                 # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                 flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                       f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                 # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.
                 return render_template("personne_ref/personne_ref_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_PrenomPersRef": PrenomPersRef,
                                                  "value_NomPersRef": NomPersRef}
                obj_actions_pers_ref.add_pers_ref_data(valeurs_insertion_dictionnaire)

                # Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'personne_ref_afficher', car l'utilisateur
                # doit voir la nouvelle adressse qu'il vient d'insérer.
                return redirect(url_for('personne_ref_afficher', order_by='DESC', id_pers_ref_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("personne_ref/personne_ref_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /personne_ref_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une personne_ref par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/personne_ref_edit', methods=['POST', 'GET'])
def personne_ref_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "personne_ref_afficher.html"
    if request.method == 'GET':
        try:
            id_pers_ref_edit = request.values['id_pers_ref_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_pers_ref_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_pers_ref": id_pers_ref_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pers_ref = GestionPersRef()

            # La commande MySql est envoyée à la BD
            data_id_pers_ref = obj_actions_pers_ref.edit_pers_ref_data(valeur_select_dictionnaire)
            print("dataIdpers_ref ", data_id_pers_ref, "type ", type(data_id_pers_ref))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer les données d'une personne de référence (prénom et nom)!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("personne_ref/personne_ref_edit.html", data=data_id_pers_ref)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /personne_ref_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une personne de référence par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/personne_ref_update', methods=['POST', 'GET'])
def personne_ref_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_pers_ref_edit = request.values['id_pers_ref_edit_html']

            # Récupère les contenus des champs de pers_ref dans le formulaire HTML "num_tel_fax_edit.html"
            PrenomPersRef = request.values['name_edit_PrenomPersRef_html']
            NomPersRef = request.values['name_edit_NomPersRef_html']

            valeur_edit_list = [{'id_pers_ref': id_pers_ref_edit,
                                 'PrenomPersRef': PrenomPersRef,
                                 'NomPersRef': NomPersRef}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]?[A-Za-z\u00C0-\u00FF\0-9]*['\\- ]*[A-Za-z\u00C0-\u00FF\0-9]+$", PrenomPersRef):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "Danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des pers_refs
                valeur_edit_list = [{'id_pers_ref': id_pers_ref_edit,
                                     'PrenomPersRef': PrenomPersRef,
                                     'NomPersRef': NomPersRef}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "num_tel_fax_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('personne_ref/personne_ref_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_pers_ref": id_pers_ref_edit,
                                              "value_PrenomPersRef": PrenomPersRef,
                                              "value_NomPersRef": NomPersRef}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_pers_ref = GestionPersRef()

                # La commande MySql est envoyée à la base de données
                data_id_pers_ref = obj_actions_pers_ref.update_pers_ref_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIdpers_ref ", data_id_pers_ref, "type ", type(data_id_pers_ref))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données de la personne de référence modifiées!", "success")
                # Affichage des pers_refs
                return redirect(url_for('personne_ref_afficher', order_by="ASC", id_pers_ref_sel=id_pers_ref_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème pers_ref update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une pers_ref, on renvoie le formulaire "EDIT"
            return render_template('personne_ref/personne_ref_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /personne_ref_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une pers_ref par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/personne_ref_select_delete', methods=['POST', 'GET'])
def personne_ref_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pers_ref = GestionPersRef()
            # Récupérer la valeur de "idpers_refDeleteHTML" du formulaire html "num_tel_fax_delete.html"
            id_pers_ref_delete = request.args.get('id_pers_ref_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_pers_ref": id_pers_ref_delete}


            # La commande MySql est envoyée à la base de données
            data_id_pers_ref = obj_actions_pers_ref.delete_select_pers_ref_data(valeur_delete_dictionnaire)
            flash(f"Effacer une personne de référence!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur personne_ref_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur personne_ref_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('personne_ref/personne_ref_delete.html', data = data_id_pers_ref)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /pers_refUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une pers_ref, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/personne_ref_delete', methods=['POST', 'GET'])
def personne_ref_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_pers_ref = GestionPersRef()
            # Récupérer la valeur de "id_pers_ref" du formulaire html "pers_refAfficher.html"
            id_pers_ref_delete = request.form['id_pers_ref_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_pers_ref": id_pers_ref_delete}

            data_pers_ref = obj_actions_pers_ref.delete_pers_ref_data(valeur_delete_dictionnaire)
            # Affichage de la liste des pers_refs
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les pers_refs
            return redirect(url_for('personne_ref_afficher', order_by="ASC", id_pers_ref_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer une pers_ref qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des pers_refs
                return redirect(url_for('personne_ref_afficher', order_by="ASC", id_pers_ref_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur personne_ref_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur personne_ref_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('personne_ref/personne_ref_afficher.html', data=data_pers_ref)