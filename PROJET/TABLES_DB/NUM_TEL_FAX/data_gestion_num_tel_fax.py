# data_gestion_num_tel_fax.py
# Wolff Jérémy - 2020.05.19 - INFO1A
# Permet de gérer (CRUD) les données de la table "t_num_tel_fax"
# --------------------------------------------------------------------------------------------------------------------
#

from flask import flash

from PROJET.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from PROJET.DATABASE.erreurs import *


class GestionNumTelFax():
    def __init__(self):
        try:
            # DEBUG : Pour afficher un message dans la console.
            print("dans le try de gestions num_tel_fax")
            # La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans GestionNumTelFax: Erreur, il faut connecter une base de donnée!", "danger")
            # DEBUG : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionNumTelFax {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionNumTelFax ")

    def num_tel_fax_afficher_data(self, valeur_order_by, id_num_tel_sel):
        try:
            print("valeur_order_by ", valeur_order_by, type(valeur_order_by))

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Afficher soit la liste des numéros dans l'ordre inverse ou simplement les numéros sélectionnés
                # par l'action edit
                if valeur_order_by == "ASC" and id_num_tel_sel == 0:
                    strsql_num_tel_fax_afficher = """SELECT id_num_tel, NumTel, NumFax FROM t_num_tel_fax 
                    ORDER BY id_num_tel ASC"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_num_tel_fax_afficher)
                elif valeur_order_by == "ASC":
                    valeur_id_num_tel_selected_dictionnaire = {"value_id_num_tel_selected": id_num_tel_sel}
                    strsql_num_tel_fax_afficher = """SELECT id_num_tel, NumTel, NumFax FROM t_num_tel_fax 
                    WHERE id_num_tel = %(value_id_num_tel_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_num_tel_fax_afficher, valeur_id_num_tel_selected_dictionnaire)
                else:
                    strsql_num_tel_fax_afficher = """SELECT id_num_tel, NumTel, NumFax FROM t_num_tel_fax 
                    ORDER BY id_num_tel DESC"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_num_tel_fax_afficher)

                # Récupère les données de la requête.
                data_num_tel_fax = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_num_tel_fax ", data_num_tel_fax, " Type : ", type(data_num_tel_fax))
                # Retourne les données du "SELECT"
                return data_num_tel_fax
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise  MaBdErreurPyMySl(f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def add_num_tel_fax_data(self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            strsql_insert_num_tel_fax = """INSERT INTO t_num_tel_fax (id_num_tel, NumTel, NumFax) 
            VALUES (NULL,%(value_NumTel)s, %(value_NumFax)s)"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_num_tel_fax, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")


    def edit_num_tel_fax_data(self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # Commande MySql pour afficher le numéro sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_num_tel = "SELECT id_num_tel, NumTel, NumFax FROM t_num_tel_fax " \
                                 "WHERE id_num_tel = %(value_id_num_tel)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_num_tel, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_num_tel_fax_data Data Gestions NumTelFax numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions NumTelFax numéro de l'erreur : {erreur}", "danger")
            # On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_num_tel_fax_data d'un numéro Data Gestions NumTelFax {erreur}")

    def update_num_tel_fax_data(self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleNumTelFaxHTML" du form HTML "num_tel_fax_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_update_NumTel = "UPDATE t_num_tel_fax SET NumTel = %(value_NumTel)s " \
                                    "WHERE id_num_tel = %(value_id_num_tel)s"
            str_sql_update_NumFax = "UPDATE t_num_tel_fax SET NumFax = %(value_NumFax)s " \
                                    "WHERE id_num_tel = %(value_id_num_tel)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_NumTel, valeur_update_dictionnaire)
                    mc_cur.execute(str_sql_update_NumFax, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_num_tel_fax_data Data Gestions NumTelFax numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions NumTelFax numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_num_tel_fax_data d\'un numéro Data Gestions NumTelFax {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "danger")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash('Doublon! Introduire une valeur différente!')
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_num_tel_fax_data Data Gestions NumTelFax numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_num_tel_fax_data d'une aderesse DataGestionsNumTelFax {erreur}")

    def delete_select_num_tel_fax_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour la MODIFICATION de la valeur saisie dans le champ "nameEditIntituleNumTelFaxHTML" du form HTML "num_tel_fax_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"

            # Commande MySql pour afficher le numéro sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_num_tel = "SELECT id_num_tel, NumTel, NumFax " \
                                        "FROM t_num_tel_fax WHERE id_num_tel = %(value_id_num_tel)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_num_tel, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_select_num_tel_fax_data Gestions NumTelFax numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_num_tel_fax_data numéro de l'erreur : {erreur}", "danger")
            raise Exception("Raise exception... Problème delete_select_num_tel_fax_data d\'un numéro Data Gestions NumTelFax {erreur}")


    def delete_num_tel_fax_data(self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "num_tel_fax_edit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            str_sql_delete_num_tel_fax = "DELETE FROM t_num_tel_fax WHERE id_num_tel = %(value_id_num_tel)s"

            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_num_tel_fax, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...",data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG : Pour afficher un message dans la console.
            print(f"Problème delete_num_tel_fax_data Data Gestions NumTelFax numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions NumTelFax numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un numéro qui est associé à une autre valeur dans une table intermédiaire
                # il y a une contrainte sur des FK de tables intermédiaires
                # flash(f"Flash. Impossible d'effacer! Ce numéro est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}", "danger")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Ce numéro est associé à des valeurs dans une (des) table(s) intermédiaire(s)! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")