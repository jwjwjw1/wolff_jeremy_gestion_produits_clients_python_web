# routes_gestion_num_tel_fax.py
# Wolff Jérémy - 2020.05.19 - INFO1A
# Gestions des "routes" FLASK pour les numéros de téléphone et fax
# --------------------------------------------------------------------------------------------------------------------
#

from flask import render_template, flash, redirect, url_for, request
from PROJET import app
from PROJET.TABLES_DB.NUM_TEL_FAX.data_gestion_num_tel_fax import GestionNumTelFax
from PROJET.DATABASE.erreurs import *

# Importe le module ("re") pour utiliser les expressions régulières (regex)
import re


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_fax_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# ---------------------------------------------------------------------------------------------------
@app.route("/num_tel_fax_afficher//<string:order_by>/<int:id_num_tel_sel>", methods=['GET', 'POST'])
def num_tel_fax_afficher(order_by, id_num_tel_sel):
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_num_tel_fax = GestionNumTelFax()
            # Récupére les données grâce à une requête MySql définie dans la classe GestionNumTelFax()
            # Fichier data_gestion_pdv.py
            data_num_tel_fax = obj_actions_num_tel_fax.num_tel_fax_afficher_data(order_by, id_num_tel_sel)
            # DEBUG : Pour afficher un message dans la console.
            print(" data num_tel_fax", data_num_tel_fax, "type ", type(data_num_tel_fax))

            # Différencier les messages si la table est vide.
            if not data_num_tel_fax and id_num_tel_sel == 0:
                flash("""La table "t_num_tel_fax" est vide!""", "warning")
            elif not data_num_tel_fax and id_num_tel_sel > 0:
                # Si l'utilisateur change l'id_num_tel dans l'URL et que le numéro n'existe pas,
                flash(f"Le numéro demandé n'existe pas!", "warning")
            else:
                # Dans tous les autres cas, c'est que la table "t_num_tel_fax" est vide.
                # La ligne ci-après permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données des numéros de téléphone/fax affichées!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # Dérivation de "Exception" par le "@app.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoi de la page "HTML" au serveur.
    return render_template("num_tel_fax/num_tel_fax_afficher.html", data=data_num_tel_fax)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_fax_add ,
# ---------------------------------------------------------------------------------------------------
@app.route("/num_tel_fax_add", methods=['GET', 'POST'])
def num_tel_fax_add():
    # Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_num_tel_fax = GestionNumTelFax()
            # Récupère le contenu du champ dans le formulaire HTML "produits_add.html"
            NumTel = request.form['NumTel_html']
            NumFax = request.form['NumFax_html']

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("[0-9]*['\- ]*[0-9]*['\- ]*[0-9]",
                                NumTel):
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.
                return render_template("num_tel_fax/num_tel_fax_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_NumTel": NumTel,
                                                  "value_NumFax": NumFax}
                obj_actions_num_tel_fax.add_num_tel_fax_data(valeurs_insertion_dictionnaire)

                # Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées!", "success")
                print(f"Données insérées!")
                # On va interpréter la "route" 'num_tel_fax_afficher', car l'utilisateur
                # doit voir la nouvelle adressse qu'il vient d'insérer.
                return redirect(url_for('num_tel_fax_afficher', order_by='DESC', id_num_tel_sel=0))

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # Dérivation de "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}", "danger")
            raise MonErreur(f"Autre erreur")

        # Attention à l'ordre des excepts ; très important de respecter l'ordre.
        except Exception as erreur:
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # Envoi de la page "HTML" au serveur.
    return render_template("num_tel_fax/num_tel_fax_add.html")


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_fax_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une num_tel_fax par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/num_tel_fax_edit', methods=['POST', 'GET'])
def num_tel_fax_edit():
    # Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "num_tel_fax_afficher.html"
    if request.method == 'GET':
        try:
            id_num_tel_edit = request.values['id_num_tel_edit_html']

            # Affichage dans la console de la valeur éditée
            print(id_num_tel_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_id_num_tel": id_num_tel_edit}

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_num_tel_fax = GestionNumTelFax()

            # La commande MySql est envoyée à la BD
            data_id_num_tel = obj_actions_num_tel_fax.edit_num_tel_fax_data(valeur_select_dictionnaire)
            print("dataIdnum_tel ", data_id_num_tel, "type ", type(data_id_num_tel))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer un numéro de téléphone et/ou de fax!", "success")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la base de données! : %s", erreur)
            # On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("num_tel_fax/num_tel_fax_edit.html", data=data_id_num_tel)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_fax_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'une num_tel_fax par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/num_tel_fax_update', methods=['POST', 'GET'])
def num_tel_fax_update():
    # DEBUG : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))

    if request.method == 'POST':
        try:
            # DEBUG : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            id_num_tel_edit = request.values['id_num_tel_edit_html']

            # Récupère les contenus des champs de num_tel_fax dans le formulaire HTML "num_tel_fax_edit.html"
            NumTel = request.values['name_edit_NumTel_html']
            NumFax = request.values['name_edit_NumFax_html']

            valeur_edit_list = [{'id_num_tel': id_num_tel_edit,
                                 'NumTel': NumTel,
                                 'NumFax': NumFax}]

            # On ne doit pas accepter des valeurs vides,
            # des valeurs avec des caractères qui ne sont pas des lettres ou des chiffres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots.
            if not re.match("[0-9]*['\- ]*[0-9]*['\- ]*[0-9]",
                            NumTel):
                # En cas d'erreur:
                # Affiche un message d'erreur à l'utilisateur en cas de non respect de la regex
                flash(f"Une entrée est incorrecte ! Les caractères spéciaux, les espaces à double, "
                      f"les doubles apostrophe, les doubles trait d'union, ne sont pas acceptés", "Danger")
                # Réaffichage du formulaire "pdv_add.html" à cause des erreurs relatives à la regex.

                # Affichage de la liste pour éditer des num_tel_fax
                valeur_edit_list = [{'id_num_tel': id_num_tel_edit,
                                     'NumTel': NumTel,
                                     'NumFax': NumFax}]

                # DEBUG :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "num_tel_fax_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('num_tel_fax/num_tel_fax_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_id_num_tel": id_num_tel_edit,
                                              "value_NumTel": NumTel,
                                              "value_NumFax": NumFax}

                # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_num_tel_fax = GestionNumTelFax()

                # La commande MySql est envoyée à la base de données
                data_id_num_tel = obj_actions_num_tel_fax.update_num_tel_fax_data(valeur_update_dictionnaire)
                # DEBUG :
                print("dataIdnum_tel ", data_id_num_tel, "type ", type(data_id_num_tel))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Numéro de téléphone et/ou de fax modifié!", "success")
                # Affichage des num_tel_fax
                return redirect(url_for('num_tel_fax_afficher', order_by="ASC", id_num_tel_sel=id_num_tel_edit))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"Problème num_tel_fax update{erreur.args[0]}", "danger")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans les champs d'édition d'une num_tel_fax, on renvoie le formulaire "EDIT"
            return render_template('num_tel_fax/num_tel_fax_edit.html', data=valeur_edit_list)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_fax_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change les valeurs d'une num_tel_fax par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@app.route('/num_tel_fax_select_delete', methods=['POST', 'GET'])
def num_tel_fax_select_delete():

    if request.method == 'GET':
        try:

            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_num_tel_fax = GestionNumTelFax()
            # Récupérer la valeur de "idnum_telDeleteHTML" du formulaire html "num_tel_fax_delete.html"
            id_num_tel_delete = request.args.get('id_num_tel_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_num_tel": id_num_tel_delete}


            # La commande MySql est envoyée à la base de données
            data_id_num_tel = obj_actions_num_tel_fax.delete_select_num_tel_fax_data(valeur_delete_dictionnaire)
            flash(f"Effacer un numéo de téléphone et/ou de fax!", "warning")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communication d'une erreur
            # DEBUG : Pour afficher un message dans la console.
            print(f"Erreur num_tel_fax_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur num_tel_fax_delete {erreur.args[0], erreur.args[1]}", "danger")

    # Envoie la page "HTML" au serveur.
    return render_template('num_tel_fax/num_tel_fax_delete.html', data = data_id_num_tel)


# ---------------------------------------------------------------------------------------------------
# Définition d'une "route" /num_tel_faxUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier une num_tel_fax, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@app.route('/num_tel_fax_delete', methods=['POST', 'GET'])
def num_tel_fax_delete():

    # Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_num_tel_fax = GestionNumTelFax()
            # Récupérer la valeur de "id_num_tel" du formulaire html "num_tel_faxAfficher.html"
            id_num_tel_delete = request.form['id_num_tel_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_id_num_tel": id_num_tel_delete}

            data_num_tel_fax = obj_actions_num_tel_fax.delete_num_tel_fax_data(valeur_delete_dictionnaire)
            # Affichage de la liste des num_tel_fax
            # Envoi de la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les num_tel_fax
            return redirect(url_for('num_tel_fax_afficher', order_by="ASC", id_num_tel_sel=0))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # Traitement spécifique de l'erreur MySql 1451
            # L'erreur 1451 signifie qu'on veut effacer un numéro de téléphone/fax qui est associé dans une table intermédiaire
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('Impossible d\'effacer! Cette valeur est associée à une autre valeur par une table intermédiaire!', "warning")
                # DEBUG : Pour afficher un message dans la console.
                print(f"Impossible d'effacer! Cette addresse est associé à d'autres valeurs par une table intermédiaire! : {erreur}")
                # Affichage de la liste des num_tel_fax
                return redirect(url_for('num_tel_fax_afficher', order_by="ASC", id_num_tel_sel=0))
            else:
                # Communication d'une erreur survenue (autre que 1062)
                # DEBUG : Pour afficher un message dans la console.
                print(f"Erreur num_tel_fax_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur num_tel_fax_delete {erreur.args[0], erreur.args[1]}", "danger")


            # Envoi de la page "HTML" au serveur.
    return render_template('num_tel_fax/num_tel_fax_afficher.html', data=data_num_tel_fax)