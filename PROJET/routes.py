# routes.py
# Wolff Jérémy - 2020.05.01 - INFO1A
# Diverses routes. Eventuels ajouts au fur et à mesure du développement de l'application
# ---------------------------------------------------------------------------------------
from flask import render_template
from PROJET import app

# Retourne la page "home", menu principal de l'application
@app.route('/')
def home_page():
    return render_template("home.html")

# Retourne la page "equipealcina", page présentant les collaborateurs (cf. onglet "L'équipe Alcina")
@app.route('/equipealcina')
def equipealcina():
    return render_template("equipealcina.html")
