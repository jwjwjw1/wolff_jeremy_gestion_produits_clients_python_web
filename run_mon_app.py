# run_mon_app.py
# Wolff Jérémy - 2020.04.30 - INFO1A
# Fichier d'exécution de l'application
# --------------------------------------------------------------------------------------------------------------------

# Importation de la classe Flask
from flask import flash, render_template
from PROJET import app

# Définition d'une page d'erreur 404 personnelle
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# Grâce à la méthode "flash" cela permet de "raise" (remonter)
# les erreurs "try...except" dans la page "home.html"
@app.errorhandler(Exception)
def om_104_exception_handler(error):
    flash(error, "danger")
    return render_template("home.html")

# Retourne une page d'erreur 500
@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500


@app.errorhandler(ConnectionRefusedError)
def conn_internal_error(error):
    return render_template('500.html'), 500


if __name__ == "__main__":
    # Script principal "__main__"
    # Activation du mode "debug"
    # L'adresse IP du serveur mis en place par Flask peut être changée (ici 127.0.0.1)
    # Pour ce fichier on impose le numéro du port (ici 1234)
    print("app.url_map ____> ", app.url_map)
    app.run(debug=True,
            host="127.0.0.1",
            port="1234")
