<span style="color: #fb4141">**Validation du module ICT-104: rendu final du projet**</span>

**Marche à suivre/description d’installation du projet**

Cher Monsieur Maccaud, voici la marche à suivre/description d’installation de mon projet, demandé pour la validation du module ICT-104 sur la plateforme Moodle.

Ce fichier présente, respectivement :

1. Le protocole à suivre pour transférer (cloner) mon travail sur votre ordinateur ; (voir PDF*)

2. Un tutoriel concis rapidement conçu par mes propres soins pour préparer l’EDI PyCharm au lancement correct de mon projet ; (voir PDF*)

3. Très très brève description du répertoire « Divers_essais-tests-docs » de mon projet ; (voir PDF*)

4. Mes remerciements et souhaits au vénérable et légendaire, Olivier Maccaud de Bex ! (voir PDF*)

   *voir PROJET/Divers_essais-tests-docs/wolff_jeremy_marche_a_suivre_projet_info1a_2020.pdf pour avoir le fichier complet en PDF.

   

---

**1. Transfert (clonage) de mon travail sur votre ordinateur**

Pour cloner mon projet sur votre machine, il vous suffit de suivre ce protocole :

1. Ouvrir le terminal de commandes de Git sur le dossier sur lequel vous souhaitez cloner mon travail (clic droit sur le dossier, puis clic gauche sur *Git Bash Here*),

2. Dactylographier ou copier-coller (avec clic droit à Paste) cette ligne de code suivante :

<span style="color: #fb4141">git clone https://gitlab.com/jwjwjw1/wolff_jeremy_gestion_produits_clients_python_web.git</span>

**Veuillez svp vous référer au fichier "wolff_jeremy_marche_a_suivre_projet_info1a_2020.pdf" pour la marche à suivre COMPLETE!**



---

**ANCIENS EXERCICES**

---

**Exercice 3**

Ce qui a été demandé pour l'exercice 3 (donnée sur Moodle) a été fait!

Pour le rendu final, quelques heures de travail me premettront de pouvoir remplir les objectifs (de mon père) de l'application.

Je vais encore, par ailleurs, réajuster des commentaires et rectifier quelques noms de fichiers.

A bientôt, *vénérable Olivier Maccaud de Bex*!

Cher Monsieur Maccaud, pour tester mon programme, comme d'habitude:
* commande dans votre terminal: git clone https://gitlab.com/jwjwjw1/wolff_jeremy_gestion_produits_clients_python_web.git

Merci à vous et un excellent week-end prolongé!

Jérémy Wolff

-------------------------------------------

Exercice 2

Toutes les actions CRUD sont réalisables sur mes tables "t_points_de_vente" et "t_adresse".

Des commentaires de codage ont été ajoutés ou mis à jour.

J'ai créé une feuille de style dans lequel j'y ai écrit quelques lignes de CSS pour un rendu déjà un peu plus joli.

Cher Monsieur Maccaud, pour tester mon programme, comme d'habitude:
* commande dans votre terminal: git clone https://gitlab.com/jwjwjw1/wolff_jeremy_gestion_produits_clients_python_web.git

Merci à vous et un excellent week-end!

-------------------------------------------

Exercice 1 

Les tables de ma base de données (wolff_jeremy_gestion_produits_clients_104_2020) 
concernées par les tests réussis des 4 méthodes de l’acronyme CRUD (Create, Read, Update et Delete) 
pour l’exercice 1 sont les suivantes : 


*  t_titre_pers_ref 

*  t_adresse 

*  t_entr_avoir_adresse 

C’est dans ces dernières que les tests d’insertion, de lectures, de modifications 
et de suppressions de données, par l’intermédiaire de l’EDI PyCharm, ont été appliqués.

Pour ce faire, il a simplement été nécessaire de restructurer mes dossiers et 
d’éventuellement réajuster les chemins (exemple : from DATABASE import connect_db) 
où aller chercher le fichier de connexion à la base de données à importer (connect_db.py) dans les fichiers du CRUD. 

J’ai créé un dossier « TABLES_DB » dans lequel j’y ai créé d’autres dossiers aux 
noms de mes tables de ma base de données. Dans chacun de ces derniers dossiers, 
j’y ai ajouté vos fichiers que j’ai modifié par rapport aux champs relatifs et 
correspondants respectivement à ma table en question de ma base de données. 

Mis à part de la lecture, de la logique, quelques vérifications de bon fonctionnement 
des tests et quelques commandes git à effectuer, il n’y a pas vraiment eu grand-chose à faire pour cet exercice. 

    Pour être convaincu que mes tests aient bien aboutis, à la suite de l’exécution 
    de mes fichiers Python dans PyCharm, je suis allé vérifier dans ma base de données 
    si les données à insérer, modifier ou supprimer ont été mises à jour. Cela a été le cas !  

Pour tester, c’est exactement le même système que vous (nous devions effectivement modifier vos requêtes). 
Vous pouvez simplement, à la suite de la commande dans votre terminal, 
git clone https://gitlab.com/jwjwjw1/wolff_jeremy_gestion_produits_clients_python_web.git , 
faire des tests dans les fichiers sous le dossier « TABLES_DB » en y insérer des valeurs de test 
et en exécutant le fichier Python souhaité .

---------------

    Veuillez trouver la "marche à suivre" complète dans le fichier "wolff_jeremy_EXERCICE-1_info1a_2020.pdf" 
    sous le dossier "Divers_essais-tests-docs"